require 'openssl'
module YandexPenelope
  class Client
    def repeatCardPayment(payment)
      response = self.connection.post('/webservice/mws/api/repeatCardPayment', {
          clientOrderId: payment.id.to_s,
          invoiceId: payment.data['invoiceId'].to_i,
          amount: format('%.2f', payment.price),
          orderNumber: payment.id.to_s
      }.to_param)
      if response.success?
        YandexPenelope::Response.new(payment, response.body['repeatCardPaymentResponse'].symbolize_keys)
      else
        YandexPenelope::Response.new(payment, {
                                         status: '1',
                                         error: '-1',
                                         techMessage: "Bad response from Yandex.Penelope (status: #{response.status}",
                                         processedDT: Time.current.to_s,
                                         clientOrderId: payment.id.to_s
                                     })
      end
    end
    def connection
      @connection ||= begin
        p12 = OpenSSL::PKCS12.new File.read(settings.pkcs12), settings.password
        Faraday.new(settings.url, ssl: {
            verify: false,
            client_cert: p12.certificate,
            client_key: p12.key
        }) do |builder|
          builder.response :xml, content_type: /\bxml$/
          builder.adapter  :net_http_persistent
        end
      end
    end
    def settings
      Settings.yandex.penelope
    end
  end
end