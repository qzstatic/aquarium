require 'csv'
namespace :segments do
  desc "Разделение пользователей по сегментам"
  task find: :environment do

    other_paymethod_slugs = ['other', 'apple', 'google'] # Методы оплаты не учитываемые в статистике
    other_paymethod_ids = Paymethod.where('slug IN (?)', other_paymethod_slugs).map(&:id)
    period = '1year'

    year_payment_kit_ids =  Period.find_by_duration(period).payment_kits.active.map(&:id)

    price = 769

    # Активные подписчики
    active_subscribers =  User.joins(:access_rights).where('? BETWEEN access_rights.start_date AND access_rights.end_date', Time.now).
        joins('INNER JOIN "payments" ON "payments"."id" = "access_rights"."payment_id"')



    # Активные подпискичи, имещюие заказы с методами оплаты, не использующимися в статистике
    subscribers_with_other_paymethod = active_subscribers.where('payments.paymethod_id IN (?)', other_paymethod_ids)
    generate_report('Другое_AppStore_GooglePlay', subscribers_with_other_paymethod)

    #Активные подписчики имеющие активную годовую подписку, заисключением пользователей, имеющих подписки оплаченные, с помощью методов, не учитываемых в статистике
    year_subscribers = active_subscribers.where('payments.payment_kit_id IN (?) OR payments.renewal_id IN (?)', year_payment_kit_ids, year_payment_kit_ids).
        where('payments.paymethod_id NOT IN (?)', other_paymethod_ids)
    #link_mailing(466, year_subscribers)
    generate_report('Годовые подписчики', year_subscribers)

    # Переделать надо
    #Активные подписчики, имеющие активную подписку, стоимость продления которой составляет 769 рублей, при этом они НЕ имеют годовую подписку и НЕ имеют подписку оплачиваемую с помощьтю методов, не учитываемых в статистике
    priced_payment_kits = PaymentKit.where(price: price).active.map(&:id)
    price_subscribers = active_subscribers.where('payments.payment_kit_id or payments.renewal_id IN (?) (?)', priced_payment_kits, priced_payment_kits).
        where(autopay: true).
        where('payments.paymethod_id  NOT IN (?)', other_paymethod_ids)
   # link_mailing(467, price_subscribers)
    generate_report('769', price_subscribers)


    #Оставшиеся
    other_users =  active_subscribers - price_subscribers - year_subscribers - subscribers_with_other_paymethod
  #  link_mailing(465, other_users)
    generate_report('Оставшиеся', other_users)
  end

  def generate_report(name, users)

    CSV.open("/tmp/#{name}.csv", "wb") do |csv|
      csv << User.attribute_names
      users.each do |user|
        csv << user.attributes.values
      end
    end
  end

  def link_mailing(mailing_id, users)
    puts "Link #{users.count} to #{mailing_id}"
    mailing = Mailing.find(mailing_id)
    mailing.contacts = users.map(&:contact).compact
    mailing.contacts_status = :contacts_linking
    mailing.save
  end
end