namespace :adriver do
  desc 'Upload pressrelease banners to Adriver.'
  task upload: :environment do
    AdriverUploader.process
  end
end
