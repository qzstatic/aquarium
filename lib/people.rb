module People
  PEOPLE_CHUNK_SIZE = 1000

  class << self
    def snipe_client
      @snipe_client ||= SnipeClient.new
    end

    def category_ids
      unless @category_ids
        categories_tree = People::snipe_client.get('v1/categories/tree').body
        categories = categories_by_slugs(categories_tree, 'kinds', 'people') + categories_by_slugs(categories_tree, 'domains', 'vedomosti.ru')
        @category_ids = categories.map(&:id)
      end
      @category_ids
    end

    def synonyms
      unless @synonyms
        @synonyms, i = [], 0
        while (people_chunk = get_people_chunk(PEOPLE_CHUNK_SIZE * i)).size != 0 do
          @synonyms += people_chunk.map { |person| person.synonyms.to_s.split(/,\s/) }
          i += 1
        end
        @synonyms.flatten!.uniq!
      end
      @synonyms
    end

    private

    def categories_by_slugs(categories, *categories_chains)
      search_category = categories_chains.delete_at(0)
      found_categories = [categories.find { |cat| cat.slug == search_category }]
      found_categories += categories_chains.empty? ? [] : categories_by_slugs(found_categories[0].children, *categories_chains)
      found_categories
    end

    def get_people_chunk(offset)
      snipe_client.get('v1/documents/categorized', category_ids: category_ids, limit: PEOPLE_CHUNK_SIZE, offset: offset).body
    end
  end

end