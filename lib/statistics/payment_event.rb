module Statistics
  class PaymentEvent < Base

    attr_reader :event_params

    def initialize(options)
      @event_params = options.slice(:event, :payment_id, :status, :sum, :product, :namespace, :subproduct, :period, :paymethod,
                                    :access_token, :msisdn, :user_id, :session_id,:document_url
      )
    end

    def send!
      response = rigel_client.get('/payments', event_params)
      response.success?
    end
  end
end
