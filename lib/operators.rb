module Operators

  class << self

    def mts
      @mts ||= Operators::MTS.new
    end

    def tele2
      @tele2 ||= Operators::Tele2.new
    end

    def megafon
      @megafon ||= Operators::Megafon.new
    end

    def beeline
      @beeline ||= Operators::Beeline.new
    end

    def internal_error(code)
      internal_errors["code_#{code}"]
    end

    private

    def internal_errors
      @internal_errors ||= YAML.load_file(Rails.root.join('lib', 'operators', 'internal_errors.yml'))
    end
  end
end
