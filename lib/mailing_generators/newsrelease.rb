require Rails.root.join('app', 'helpers', 'application_helper')

module MailingGenerators
  class Newsrelease
    class << self
      include ::Rails.application.routes.url_helpers
      
      def generate(base_template: 'newsrelease', documents_limit: 1000)
        use_case = Common::NewsreleaseShow.new(documents_limit: documents_limit, **newsrelease_date)
        use_case.call

        @newsrelease = use_case.newsrelease

        @pages = use_case.pages

        @press_releases = Roompen.categorized(%w(kinds press_releases), limit: 4, full: true)
        
        action_view = ActionView::Base.new(Rails.configuration.paths["app/views"])
        action_view.class_eval do
          include Rails.application.routes.url_helpers
          include ApplicationHelper
          
          def default_url_options
            { host: Settings.hosts.shark }
          end
          
          def protect_against_forgery?
            false
          end
        end
        
        action_view.render(
          partial: "mailing_generators/newsrelease/#{base_template}",
          format: :html,
          locals: { :@newsrelease => @newsrelease, :@pages => @pages, :@press_releases => @press_releases }
        )
      end
    
    private
      def newsrelease_date
        {
          year:  Date.today.year,
          month: Date.today.month,
          day:   Date.today.day
        }
      end
    end
  end
end
