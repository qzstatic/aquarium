require 'net/ftp'

class SLXUploader
  include Procto.call

  ENCODING = 'windows-1251'
  
  def call
    return if orders.count == 0
    tmpfile_orders = Tempfile.new('orders', :encoding => ENCODING)
    tmpfile_users = Tempfile.new('users', :encoding => ENCODING)
    begin
      tmpfile_orders.write(orders_content)
      tmpfile_orders.close
      tmpfile_users.write(users_content)
      tmpfile_users.close
      Net::FTP.open(Settings.slx.ftp.host) do |ftp|
        ftp.passive = true
        ftp.login(Settings.slx.ftp.user, Settings.slx.ftp.password)
        ftp.put(tmpfile_orders.path, remote_orders_filename)
        ftp.put(tmpfile_users.path, remote_users_filename) if users.count > 0
      end
      mark_as_synced
    ensure
      tmpfile_orders.unlink
      tmpfile_users.unlink
    end
  end
  
private
  
  def remote_orders_filename
    ERB.new(Settings.slx.ftp.orders_filename).result
  end
  
  def remote_users_filename
    ERB.new(Settings.slx.ftp.users_filename).result
  end
  
  def orders_content
    orders.map do |o|
      order_slx_fields(o).join("\t")
    end.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end
  
  def users_content
    users.map do |u|
      user_slx_fields(u).join("\t")
    end.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end

  def payment_methods
    Order.payment_methods.values_at(:by_card, :by_yandex, :by_unknown)
  end

  def products
    Price.products.values_at(:online, :paper, :profi)
  end
  
  def orders
    @orders ||= Order.where(payment_method: payment_methods).unsynced.order('id DESC').joins(:price).where("prices.product in (?)", products)
  end
  
  def users
    @users = orders.map(&:user).compact
  end
  
  def mark_as_synced
    cnt = orders.each &:synced!
    @orders = nil
    cnt
  end
  
  def order_slx_fields(order)
    res = []
    user = order.user
    price = order.price
    data = order.data.symbolize_keys
    prev_order = data[:previous_order_id] && Order.find_by_id(data[:previous_order_id])
    res << (price && price.slx_product || ' ')
    res << order.created_at.strftime('%F %T')
    res << (order.start_date && order.start_date.strftime('%F %T') || ' ')
    res << (order.end_date && order.end_date.strftime('%F %T') || ' ')
    res << (price && months_for_price(price) || 0)
    res << (price && price.value || 0)
    res << (price && price.student ? 'Y' : 'N')
    res << order.id
    res << (user && user.id || 0)
    res << (order.paid? ? 'Y' : 'N')
    res << order.payment_method
    res << (user && user.phone || ' ')
    res << (user && user.email || ' ')
    res << (data[:auto_renewing] ? 'Y' : 'N')
    res << (user && user.subscribe_base_id || 0)
    res << (prev_order && prev_order.subscribe_base_id || 0)
    res << (prev_order && (prev_order.data['old_order_id'] || prev_order.id) || 0)
    res
  end
  
  def user_slx_fields(user)
    res = []
    res << ''
    res << user.last_name
    res << user.first_name
    res << user.phone
    res << user.email
    res << (user.orders.map {|o| o.price && o.price.student}.include?(true) ? 'Y' : 'N') # льготный/нельготный
    res << user.id
    res << user.subscribe_base_id
    res
  end

  def months_for_price(price)
    price.period_seconds.div(30.days)
  end
end
