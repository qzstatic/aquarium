module Indexer
  class Entity < Indexer::Base
    ELASTIC_DOCUMENTS_PER_REQUEST = 1000

    class << self

      def category_id_by_slug(slug = index_name)
        @category ||= snipe_client.get('v1/categories/kinds').body.find { |category| category.slug == slug }
        @category.id
      end

      def snipe_client
        @snipe_client ||= SnipeClient.new
      end

      def index_changed
        count = super
        regroup!
        count
      end

      def regroup!
        @entities = collect_elastic_entities
        @alphabet = @entities.map { |entity| entity[filtering_attribute] }.uniq
        @transform = nil
        @alphabet.each { |first_letter| regroup_by!(first_letter) }
      end

      def regroup_by!(attribute)
        entities_chunk = @entities.select { |entity| entity[filtering_attribute] == attribute }
        split_to_groups!(entities_chunk)
      end

      def split_to_groups!(entities)
        return if entities.blank?
        index_single_group(cut_group(entities))
        split_to_groups!(entities)
      end

      def cut_group(entities)
        current_group = entities.slice!(0, entities_per_group)
        while !entities.blank? && current_group.last[group_separator_attribute] == entities[0][group_separator_attribute]
          current_group.push(entities.delete_at(0))
        end
        current_group
      end

      def index_single_group(group_of_entities)
        group_name = "#{group_of_entities.first[group_separator_attribute]} — #{group_of_entities.last[group_separator_attribute]}"
        index_documents(group_of_entities.map { |document| document.merge(group_attribute => group_name) })
      end

      def collect_elastic_entities
        result = elastic.search(index: index_name, scroll: '2m', body: search_query)
        entities = result['hits']['hits']
        loop do
          result = elastic.scroll(scroll_id: result['_scroll_id'], scroll: '2m')
          entities += result['hits']['hits']
          break if result['hits']['hits'].length < ELASTIC_DOCUMENTS_PER_REQUEST
        end
        entities.map { |entity| entity['_source'] }
      end

      def search_query
        {
            query: {
                bool: {
                    must: [
                        { term: { published: true } },
                        { term: { disable_on_main_page: false } }
                    ]
                }
            },
            sort: sort_attributes,
            size: ELASTIC_DOCUMENTS_PER_REQUEST
        }
      end

      def filtering_attribute
        @filtering_attribute ||= 'first_letter'
      end

      def group_attribute
        @group_attribute ||= 'group'
      end

      def sort_attributes
        [
            { first_letter: :asc }
        ]
      end

      def entities_per_group
        120
      end

      def group_separator_attribute
        throw 'You must implement `group_separator_attribute` method'
      end
    end
  end
end
