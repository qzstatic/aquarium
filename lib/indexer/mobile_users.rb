module Indexer
  class MobileUsers < Indexer::Base
    load_settings_from_file

    sources({
      one:   "#{Settings.hosts.aquarium}/ibis/mobile_users/:id", 
      all:   "#{Settings.hosts.aquarium}/ibis/indexer/mobile_users/page/:page", 
      fresh: "#{Settings.hosts.aquarium}/ibis/indexer/mobile_users/fresh/:from"
    })

    transform do |document|
      document
    end
  end
end
