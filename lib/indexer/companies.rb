module Indexer
  class Companies < Indexer::Entity
    CATEGORY_SLUG = 'companies'
    COMPANIES_PER_GROUP = 60

    load_settings_from_file

    sources({
                one: "#{Settings.hosts.snipe}/v1/documents/:id/preview",
                all: "#{Settings.hosts.snipe}/v1/search/documents/index/:page?category_ids[]=#{category_id_by_slug}",
                fresh: "#{Settings.hosts.snipe}/v1/search/documents/fresh/:from/"
            })

    transform do |document|
      document['industry'] = document['links']['industry'][0]['bound_document']['title'] rescue nil
      next unless valid?(document)

      attributes = %w(id synonyms industry published_at published url title disable_on_main_page commercial)
      document.keep_if { |k, _| attributes.include?(k) }
      document['first_letter'] = document['title'][/[а-яё\w]/i].mb_chars.upcase.to_s
      document
    end

    class << self

      def index_changed
        switch_filtering_to(:title)
        count = super
        sleep 10
        switch_filtering_to(:industry)
        regroup!
        count
      end

      def valid?(document)
        document['categories']['kinds'].try(:[], 'slug').to_s == CATEGORY_SLUG && !document['industry'].blank?
      end

      def group_separator_attribute
        'title'
      end

      def sort_attributes
        [
            { title: :asc }
        ]
      end

      def entities_per_group
        COMPANIES_PER_GROUP
      end

      def switch_filtering_to(attribute)
        @filtering_attribute, @group_attribute = case attribute
                                                 when :title
                                                   %w(first_letter group_by_title)
                                                 when :industry
                                                   %w(industry group_by_industry)
                                                 else
                                                   throw 'Not implemented'
                                                 end
      end
    end
  end
end
