module Indexer
  class Users < Indexer::Base
    load_settings_from_file

    sources({
      one:   "#{Settings.hosts.aquarium}/ibis/users/:id", 
      all:   "#{Settings.hosts.aquarium}/ibis/indexer/users/page/:page", 
      fresh: "#{Settings.hosts.aquarium}/ibis/indexer/users/fresh/:from"
    })

    transform do |document|
      document.delete('orders')
      document.delete('accounts')
      document.delete('avatar')

      document
    end
  end
end
