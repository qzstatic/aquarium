module Indexer
  class Base
    class << self

      # Индексирует определенный документ по его id
      def index_by_id(id)
        index_by_ids([id])
      end

      # Индексирует набор документов по ids
      def index_by_ids(ids)
        count = 0

        if ids.any?
          ids.each_slice(Settings.elasticsearch.per_group) do |ids_group|
            loaded_documents = load_documents(ids_group)
            count += index_documents(loaded_documents)
          end
        end

        count
      end

      # Индексирует документы начиная с указанной даты, если дата не указана — за предыдущий день
      def index_from(from = 1.day.ago)
        url = "#{@source_fresh.sub(':from', from.to_i.to_s)}"
        ids = get(url)

        index_by_ids(ids)
      end

      # Индексирует измененные документы, дата последней индексации сохраняется в Редисе в
      # ключе workers:indexer:#{index_name}:from. Если дата не указана — за предыдущий день.
      def index_changed
        from = (value = redis.get(key_for_start)) && Time.parse(value) || 1.day.ago

        now = Time.now
        count = index_from(from)
        redis.set(key_for_start, now)

        count
      end

      # Пересоздает весь индекс с нуля и запоминает время начала обновления
      def regenerate
        drop_index
        create_index

        generate
      end

      # Индексирует все документы без уничтожения индекса и устанавливает время начала обновления
      def generate(start_page: 1)

        indexed_documents = 0
        page              = start_page
        now               = Time.now

        begin
          logger.info "Begin indexing documents page #{page}"
          documents_ids = get("#{@source_all.sub(':page', page.to_s)}")
          indexed_documents += index_by_ids(documents_ids) if documents_ids.present?
          page += 1
        end while nil == documents_ids || documents_ids.present?

        # Устанавливаем время, с которого в следующий раз нужно начинать обновление
        redis.set(key_for_start, now)

        indexed_documents
      end

      # Обновляет настройки индекса
      def update_settings
        if @settings['mappings']
          @settings['mappings'].each_pair do |type, mapping|
            elastic.indices.put_mapping index: index_name, type: type, body: mapping
          end
        end
      end

      # Удаляет индекс
      def drop_index
        elastic.indices.delete(index: index_name) if elastic.indices.exists(index: index_name)
      end

      # Создает индекс
      def create_index
        elastic.indices.create index: index_name, body: @settings
      end

      def faraday
        @faraday ||= Faraday.new do |c|
          c.use SnipeAuth
          c.response :json, content_type: /\bjson$/
          c.adapter :em_synchrony
        end
      end

    private

      # Физически загружает набор документов делая параллельные запросы к API
      def load_documents(document_ids)
        loaded_documents = []
        responses        = []

        faraday.in_parallel do
          document_ids.each do |document_id|
            responses << faraday.get("#{@source_one.sub(':id', document_id.to_s)}")
          end
        end

        responses.each do |resp|
          if 200 == resp.status
            loaded_documents << resp.body
            logger.info "index [#{document_type}] from [#{resp.env.url}]"
          else
            logger.info "error get [#{document_type}]"
            logger.info resp.env.inspect
            raise 'error get document'
          end
        end

        loaded_documents
      end

      # Делает GET запрос
      def get(url)
        resp = faraday.get(url)

        if 200 == resp.status
          resp.body
        else
          logger.info "error get [#{document_type}] from [#{url}], status: #{resp.status}"
          raise 'error get document'

          nil
        end
      end

      # Трансформирует документы и добавляет в индекс
      def index_documents(documents)
        if documents.any?
          if @transform.kind_of?(Proc)
            documents.map! do |document|
              @transform.call(document)
            end.compact!
          end

          documents.map! { |document| { index: { _id:  document['id'], data: document } } }
          elastic.bulk(index: index_name, type: document_type, body: documents) unless documents.blank?

          documents.count
        else
          0
        end
      end

      # Загружает из файла с #{index_name}.yml настройки для индекса
      def load_settings_from_file
        settings_file = Rails.root.join('lib/indexer', "#{index_name}.yml")

        if File.exist?(settings_file)
          @settings = YAML.load_file(settings_file)
        else
          raise "Settings file for #{index_name} index not found!"
        end
      end

      # Добавляет источники
      def sources(all: nil, one: nil, fresh: nil)
        @source_all   = all
        @source_one   = one
        @source_fresh = fresh
      end

      def transform(&block)
        @transform = block
      end

      def redis
        @redis ||= Aquarium::Redis.connection
      end

      def index_name
        # @index_name ||= name.split('::')[1].downcase

        unless @index_name.present?
          @index_name = name.split('::')[1].downcase
          @index_name = 'documents_new' if 'documents' == @index_name
        end

        @index_name
      end

      def document_type
        @document_type ||= 'documents_new' == index_name ? 'document' : index_name.singularize
      end

      def elastic
        @elastic ||= Elasticsearch::Client.new({
          host: Settings.elasticsearch.host,
          port: Settings.elasticsearch.port,
          adapter: :net_http_persistent,
          logger: logger
        })
      end

      def logger
        @logger ||= Logger.new("#{Rails.root.join('log')}/indexer.log")
      end

      def key_for_start
        "workers:indexer:#{index_name}:from"
      end
    end
  end
end
