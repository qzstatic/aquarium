class AdriverUploader
  def initialize
    adriver_client.authorize!
  end

  def process
    Settings.adriver.ads_data.map do |ad|
      documents(ad.company_id).map do |document|
        create_banner(document, ad.id, ad.placement_id)
      end
    end
  end
  
  class << self
    def process
      new.process
    end
  end
  
private
  def documents(company_id)
    Roompen.bound_documents(company_id, section: 'company', full: true, limit: 10)
  end
  
  def create_banner(document, ad_id, ad_placement_id)
    unless PressreleaseBanner.exists?(document_id: document.id)
      renderer = ERB.new(template('create_banner.xml.erb'))
      data = renderer.result(binding)
      
      response = adriver_client.post('/banners', data)

      Rails.logger.ap '=== BEGIN CREATE BANNER ==='
      
      if response.success?
        upload_banner(response.body.entry.content.banner.id, document)
        Rails.logger.ap "Success! document_id: #{document.id}, url: #{document.url}"
      else
        Rails.logger.ap "Failed! document_id: #{document.id}, url: #{document.url}"
        Rails.logger.ap response.body
      end
      
      Rails.logger.ap '=== END CREATE BANNER ==='
    end
  end
  
  def upload_banner(banner_id, document)
    PressreleaseBanner.find_or_create_by(document_id: document.id, banner_id: banner_id)
    xml_data_url = "http://www.#{document.url}.xml"
    renderer = ERB.new(template('upload_banner.xml.erb'))
    data = renderer.result(binding)
    
    Rails.logger.ap '=== BEGIN UPLOAD BANNER ==='
    
    response = adriver_client.put("/banners/#{banner_id}/upload_form", data)
    if response.success?
      Rails.logger.ap "Success! banner_id: #{banner_id}, document_id: #{document.id}, url: #{document.url}"
    else
      Rails.logger.ap "Failed! banner_id: #{banner_id}, document_id: #{document.id}, url: #{document.url}"
      Rails.logger.ap response.body
    end
    
    Rails.logger.ap '=== END UPLOAD BANNER ==='
  end
  
  def adriver_client
    @adriver_client ||= AdriverClient.new
  end
  
  def template(template_name)
    Rails.root.join('app', 'views', 'adriver_uploader', template_name).read
  end
end
