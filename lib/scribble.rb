module Scribble

  class << self
    def configure
      @config = Scribble::Config.new
      yield(@config) if block_given?
      user.authenticate
    end

    def config
      @config ||= Scribble::Config.new
    end

    def event
      @event ||= Scribble::Event.new
    end

    def user
      @user ||= Scribble::User.new
    end
  end

  class Unauthorized < Exception; end
end