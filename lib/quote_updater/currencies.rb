require 'faraday_middleware/response_middleware'

module QuoteUpdater
  class Currencies < Base
    def call
      ActiveRecord::Base.transaction do
        source_content.each do |curr|
          ticker = Ticker.find_or_initialize_by(quote_source: source, slug: curr.ticker)
          ticker.update(
            title:      curr.name,
            price_time: Time.parse(curr.date),
            value:      price_translate(curr.last),
            prev_value: price_translate(curr.prev),
            data:       { lot: curr.lot }            
          )
        end
      end
    end
    
  private
    def connection_feature(conn)
      conn.response :cb_response_decoder
    end
    
    class ResponseDecoder < FaradayMiddleware::ResponseMiddleware
      define_parser do |body|
        body = body.force_encoding('cp1251').encode!('utf-8', invalid: :replace, undef: :replace, replace: '?')
        rows = body.split('^')
        columns_names = rows.shift.split('|')
        rows.select { |r| r!='eos' }.map do |r|
          values = r.split('|')
          res = {}
          r.split('|').each_with_index { |v, i| res[columns_names[i]] = v }
          res
        end
      end
    end
    
    Faraday::Response.register_middleware cb_response_decoder: ResponseDecoder
  end
end
