module QuoteUpdater
  class Base
    attr_reader :source
    
    def initialize(source)
      @source = source
    end
    
    def config
      @config ||= Settings.quotes[source.slug]
    end
    
    def source_url
      ERB.new(config.source).result
    end
    
    def connection
      @connection ||= Faraday.new do |conn|
        conn.response :mashify
        connection_feature(conn)
        conn.basic_auth(config.user, config.password) if config.user && config.password
        conn.adapter Faraday.default_adapter
      end
    end
    
    def connection_feature(conn)
      # override in child class if needed
    end
    
    def source_content
      @source_content ||= connection.get(source_url).body
    end
    
    def price_translate(price)
      price.gsub(',', '.').to_f
    end
  end
end
