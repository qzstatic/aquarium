module QuoteUpdater
  class MicexBase < Base

  private
    def connection_feature(conn)
      conn.response :xml
    end
    
    def source_content
      super.document.data
    end

    def marketdata
      @marketdata ||= source_content.find { |d| d.id=='marketdata' }
    end

    def securities
      @securities ||= source_content.find { |d| d.id=='securities' }
    end
  end
end
