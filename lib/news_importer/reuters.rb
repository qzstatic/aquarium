module NewsImporter
  class Reuters < Base
    def call
      source_content.select { |n| !source.import_news.find_by_url(n.link) }.each do |n|
        body = connection.get(n.link).body.force_encoding('utf-8')
        body = ActionView::Base.full_sanitizer.sanitize(body).strip
        source.import_news.create(
          title:     n.title,
          url:       n.link,
          body:      body,
          news_date: Time.parse(n.pubDate),
          data:      n.slice(*%i(category credit description status group))
        )
      end
    end
    
  private
    def source_content
      super.rss.channel.item ? Array(super.rss.channel.item).reverse : []
    rescue NoMethodError => e
      error_log(e.message)
      []
    end
    
    def connection_feature(conn)
      conn.response :xml,  :content_type => /\bxml$/
    end
  end
end
