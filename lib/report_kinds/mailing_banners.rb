module ReportKinds
  class MailingBanners < Base
    def initialize
    end
    
    def call
      response = clicks_count
      {
        total_clicks: response['hits']['total'],
        clicks_by_banner: response['aggregations']['banner_clicks']['buckets']
      }
    end
  
  private
    def clicks_count
      elastic.search(body: query)
    end
  
    def query
      {
        query: {
          bool: {
            must: [
              { term:  { event: 'mail_banner_click' } }
            ]
          }
        },
        aggs: {
          banner_clicks: { terms: { field: 'tbanner_id', size:  0 } }
        },
        size: 0
      }
    end
  end
end
