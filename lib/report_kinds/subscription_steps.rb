module ReportKinds
  class SubscriptionSteps < Base
    attr_reader :url
    
    def call
      response = elastic.search(body: query(@report.start_date, @report.end_date))
      result = response['aggregations']['steps']['buckets']
      
      data = result.map! {|step| step['ids']['buckets'] }
      
      data[0] = subtraction_users(data[0], data[1])
      data[0] = subtraction_users(data[0], data[2])
      data[1] = subtraction_users(data[1], data[2])
      
      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(ID Фамилия Имя Email Телефон Компания Статус Автоплатёж Дата\ регистрации Число\ заходов)
        
        data.each_with_index do |step, index|
          csv << []
          csv << ["step#{index.succ} (шаг #{index.succ})"]
          csv << []
          
          step.each do |item|
            # development
            # csv << [
            #   item['key'],
            #   item['doc_count']
            # ]
            if user = User.find(item['key'])
              csv << [
                user.id,
                user.last_name,
                user.first_name,
                user.email,
                user.phone,
                user.company,
                user.status,
                user.autopay,
                user.created_at,
                item['doc_count']
              ]
            end
          end if step.present?
        end
        
        csv.close
      end
      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end
    
    def query(start_date, end_date)
      {
        query: {
          bool: {
            must: [
                {
                  range: {
                    time: {
                      lt: end_date.strftime('%FT%T%:z'),
                      gt: start_date.strftime('%FT%T%:z')
                    }
                  }
                },
                { term: { domain: 'buy.vedomosti.ru' } },
                { term: { event:  'subscription' } }
            ]
          }
        },
        aggs: {
          steps: {
            terms: { field: 'tsubscription' },
            aggs: {
              ids: {
                terms: {
                  field: 'tid',
                  size: 0
                }
              }
            }
          }
        },
        size: 0
      }
    end
    
    def subtraction_users(array1, array2)
      if array1 && array2
        array1.delete_if {|item| array2.map{|i| i['key']}.include?(item['key']) }
      end
    end
  end
end
