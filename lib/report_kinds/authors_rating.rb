require Rails.root.join('app', 'helpers', 'application_helper')

module ReportKinds
  class AuthorsRating < Base
    attr_reader :url
    
    def call
      response = elastic.search(index: 'visits', body: query(@report.start_date, @report.end_date, documents_ids_by_period))
      result = result_view(response)

      @filepath = @filepath.to_s.sub('csv', 'html')
      @filename = @filename.to_s.sub('csv', 'html')

      File.write(@filepath, html_view(result))
      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end
    
    def html_view(result)
      action_view = ActionView::Base.new(Rails.configuration.paths["app/views"])
      action_view.class_eval do
        include Rails.application.routes.url_helpers
        include ApplicationHelper
      end
      
      action_view.render(
        template: 'reports/authors_rating',
        format: :html,
        locals: { :@result => result, :@report => @report }
      )
    end
    
    def query(start_date, end_date, ids)
      {
        query: {
          bool: {
            must: [
              {
                range: {
                    time: {
                      lt: end_date.strftime('%FT%T%:z'),
                      gt: start_date.strftime('%FT%T%:z')
                    }
                }
              },
              { term:  { event: 'hit' } },
              { terms: { tdoc_id: ids } }
            ]
          }
        },
        aggs: {
            authors: {
                terms: {
                    field: 'tags',
                    include: 'author:[0-9]+',
                    size: 0
                },
                aggs: {
                    total: { filter: { term: { event: 'hit' } } },
                    total_from_yandex:    { filter: { regexp: { referer: '(.*?)yandex.ru'    } } },
                    total_from_google:    { filter: { regexp: { referer: '(.*?)google.com'   } } },
                    total_from_vedomosti: { filter: { regexp: { referer: '(.*?)vedomosti.ru' } } },
                    articles: {
                        terms: { field: 'path', size: 300 },
                        aggs: {
                            from_yandex:    { filter: { regexp: { referer: '(.*?)yandex.ru'    } } },
                            from_google:    { filter: { regexp: { referer: '(.*?)google.com'   } } },
                            from_vedomosti: { filter: { regexp: { referer: '(.*?)vedomosti.ru' } } }
                        }
                    }
                }
            }
        },
        size: 0
      }
    end
    
  private
    def result_view(elasic_data)
      if elasic_data['aggregations']['authors']['buckets'].present?
        elasic_data['aggregations']['authors']['buckets'].each do |author|
          begin
            if document = Roompen.document_by_id(author['key'].split(':')[1])
              author[:document] = document
            end
          rescue Roompen::NotFound => e
            Rails.logger.warn("Can't find author: #{e}")
          end
        end
      end
      
      elasic_data
    end
    
    def documents_ids_by_period
      response = snipe_client.get("/v1/documents/by_period?start_date=#{@report.start_date.strftime('%Y-%m-%d')}&end_date=#{@report.end_date.strftime('%Y-%m-%d')}")
      response.body.map(&:id) if response.success?
    end
    
    def snipe_client
      @snipe_client ||= SnipeClient.new
    end
  end
end
