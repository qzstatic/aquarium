module ReportKinds
  class MailingClicks < Base
    attr_reader :mailing_id
    
    def initialize(mailing_id)
      @mailing_id = mailing_id
    end
    
    def call
      response = clicks_count
      {
        total_clicks: response['hits']['total'],
        clicks_by_path: response['aggregations']['links_clicks']['buckets'],
        clicks_by_order: response['aggregations']['order_clicks']['buckets']
      }
    end
  
  private
    def clicks_count
      elastic.search(body: query('mail_click', mailing_id))
    end
  
    def query(event, mailing_id)
      {
        query: {
          bool: {
            must: [
              { term:  { tags:  "mailing_id:#{mailing_id}" } },
              { term:  { event: event } }
            ]
          }
        },
        aggs: {
          links_clicks: { terms: { field: 'path', size:  0 }  },
          order_clicks: { terms: { field: 'torder', size:  0 } }
        },
        size: 0
      }
    end
    
  end
end
