# Payment by Google

class PaymentProcess
  class Google < self
    class << self
      def verify(subscription_id, token, client_ip)
        receipt = ::Google::Verifier.verify(Settings.google.package_name, subscription_id, token)

        unless receipt.success?
          PaymentProcess.logger.error { "#{self.name} subscription_id: #{subscription_id}, token: #{token}, client_ip: #{client_ip}, error: #{receipt.error.message}" }
          return nil
        end
        PaymentProcess.logger.info { "#{self.name} subscription_id: #{subscription_id}, token: #{token}, client_ip: #{client_ip}, receipt: #{receipt}" }
        # Найдем заказ:
        order = Order.by_google.find_by_transaction_id(receipt.transaction_id)
        unless order
          order = new_order(
            receipt:         receipt,
            subscription_id: subscription_id,
            token:           token,
            transaction_id:  receipt.transaction_id,
            client_ip:       client_ip
          )
          price_params = {
            product: :online,
            student: false,
            period:  period_by_subscription_id(subscription_id)
          }
          Common::SetPriceForOrder.new(order, price_params).call
          order.save!
          order.payment.snapshot_order('payments_google_verify')
        end
        order
      rescue Exception => e
        PaymentProcess.logger.error { "#{self.name} error: #{e}, subscription_id: #{subscription_id}, token: #{token}" }
        nil
      end
      
      def new_order(data)
        params = {
          transaction_id: data[:transaction_id],
          start_date:     Time.at(data[:receipt].startTimeMillis.to_f / 1000),
          end_date:       Time.at(data[:receipt].expiryTimeMillis.to_f / 1000),
          data: {
            auto_renewing:   data[:receipt].autoRenewing,
            receipt:         data[:receipt].to_hash,
            subscription_id: data[:subscription_id],
            token:           data[:token]
          }
        }
        order = Order.by_google.paid.new(params)
        order.client_ip = data[:client_ip]
        order
      end
      
      def period_by_subscription_id(subscription_id)
        product = Settings.mobile_subscriptions.google.map(&:subscriptions).flatten.find { |p| p.product_id == subscription_id }
        product && product.period.to_sym
      end

      def create_snapshot(order, event)
        Snapshot.create!(
          subject: order,
          event:   event,
          data:    order.attributes
        )
      end
    end

    def can_renewal?
      data = order.data.symbolize_keys
      order.paid? && data[:subscription_id] && data[:token] && data[:auto_renewing] && !data[:next_order_id] && !data[:renewal_failed]
    end

    def renewal
      if can_renewal?
        data = order.data.symbolize_keys
        neworder = self.class.verify(data[:subscription_id], data[:token], order.client_ip)
        if neworder && neworder!=order
          order.data[:next_order_id] = neworder.id
          order.data_will_change!
          order.save
          snapshot_order('payments_google_renewal')

          neworder.data[:previous_order_id] = order.id
          neworder.data_will_change!
          neworder.price = order.price
          neworder.autopayment = true
          neworder.save
          neworder.payment.snapshot_order('payments_google_renewal')
        else
          # пролонгация не удалась
          order.data[:renewal_failed] = true
          order.data_will_change!
          order.save
          snapshot_order('payments_google_renewal_failed')
        end
      end
    end
  end
end
