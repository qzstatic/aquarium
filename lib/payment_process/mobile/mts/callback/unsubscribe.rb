class PaymentProcess
  module Mobile
    module Mts
      class Callback
        class Unsubscribe < Base

          def call
            node.each do |unsubscribe_child|
              if unsubscribe_child.key?('subscriberData')
                unsubscribe_child['subscriberData'].each { |subscriber_raw_data| parse_subscriber_data!(subscriber_raw_data) }
              else
                @errors << "Undefined node #{unsubscribe_child}"
                @status = false
              end
            end
            @status
          end

          private

          def parse_subscriber_data!(raw_data)
            if (data = read_raw_data(raw_data))
              data['SMS_SUBSCRIPTION_SUBSCRIPTIONS'].each do |notify_node|
                parse_notify_node!(notify_node)
              end
            else
              @errors << "Error parsing notify data: #{raw_data}"
              @status = false
            end
          end

          def parse_notify_node!(notify_node)
            reset_cache! # Сбрасываем значения в памяти, т.к. за коллбэк могут прийти отписки нескольких абонентов
            notify_node.each do |key, value|
              value = value.first
              case key
                when 'SUBSCRIPTION_ID'
                  @subscription_id                        = value
                when 'CONTENT_ID'
                  attributes[:data]['content_id']         = value
                when 'MSISDN'
                  @msisdn = value
                when 'SUBSCRIPTION_DATE'
                  attributes[:data]['unsubscribe_source'] = value
                when 'APPROVED'
                  attributes[:data]['approved']           = value
                when 'RETURN_URL'
                  attributes[:data]['return_url']         = value
                when 'TARIFFICATION_DATE'
                  attributes[:data]['tariffication_date'] = value
                when 'UNSUBSCRIBE_SOURCE'
                  attributes[:data]['unsubscribe_source'] = value
                when 'DELETED'
                  attributes[:data]['deleted']            = value.to_s == 'true'
                when 'OPTPARAMS'
                  attributes[:data]['optparams']          = value
                else
                  @error = "Unknown key #{key}"
              end
            end
            attributes[:end_date] = Time.now
            save_to_database! && fire_event!(:stop)
          end

          def payment
            unless @payment
              @attributes.deep_merge!(data: { subscription_id: redis_key })
              @payment ||= mobile_user.payments.find_by_subscription_id(redis_key)
            end
            @payment
          end

          def mobile_user
            @mobile_user ||= MobileUser.find_by_msisdn(msisdn)
          end
        end
      end
    end
  end
end
