class PaymentProcess
  module Mobile
    module Mts
      class Callback
        class Base < ::PaymentProcess::Mobile::Base

          attr_reader :attributes, :subscription_id, :operator, :msisdn, :errors, :node

          def initialize(node)
            @node         = node
            @access_token = nil
            @operator     = 'mts'
            @errors       = []
            reset_cache!
          end

          def read_raw_data(raw_data)
            begin
              XmlSimple.xml_in(CGI::unescape(raw_data))
            rescue REXML::ParseException, ArgumentError
              @error = "Error parsing MTS notification. Raw data: #{soap_body}"
              return false
            end
          end

          def parse_notify_node(*)
            throw 'Implement parse_notify_node method in your class'
          end

          def reset_cache!
            @subscription_id = nil
            @msisdn          = nil
            @attributes      = { data: { 'operator' => 'mts' } }
            @payment         = nil
          end
        end
      end
    end
  end
end
