class PaymentProcess
  module Mobile
    module Mts
      class Callback
        attr_reader :soap_body, :xml, :status, :errors, :handler

        alias_method :success?, :status

        def initialize(soap_body)
          @soap_body = soap_body
          @status    = true
          @errors    = []
        end

        def verify!
          return false unless parse_body!
          xml['Body'].each do |element|
            element.each do |key, value|
              case key
              when 'TarifficationNotify'
                @handler = ::PaymentProcess::Mobile::Mts::Callback::Tariffication.new(value)
              when 'Unsubscribe'
                @handler = ::PaymentProcess::Mobile::Mts::Callback::Unsubscribe.new(value)
              else
                @errors << "Undefined node name #{key} in Body"
                return false
              end
            end
          end
          if handler.call
            return true
          else
            @errors = handler.errors
            false
          end
        rescue Exception => e
          Rails.logger.error e.backtrace
          @errors = [e]
          return false
        end

        private

        def parse_body!
          @xml = begin
            XmlSimple.xml_in(soap_body.gsub(/\\n?/, ''))
          rescue REXML::ParseException, ArgumentError
            @error = "Error parsing MTS notification. Raw data: #{soap_body}"
            return false
          end
        end
      end
    end
  end
end
