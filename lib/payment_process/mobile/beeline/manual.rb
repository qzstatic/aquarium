class PaymentProcess
  module Mobile
    module Beeline
      class Manual < ::PaymentProcess::Mobile::Base

        attr_reader :subscription_id, :data, :msisdn, :operator, :attributes, :error

        def initialize(subscription_id, access_token)
          @operator        = :beeline
          @subscription_id = subscription_id
          @access_token    = access_token
        end

        def verify!
          if (response = Operators.send(operator).verify_subscription(subscription_id, access_token)).success?
            if (@msisdn = response.headers['x-msisdn']).blank?
              @error = 'MSISDN undefined'
              false
            else
              @attributes = { status: :paid, end_date: subscribe_end_date, data: {
                  msisdn: msisdn
              } }
              @attributes.deep_merge(data: redis_payment) unless redis_payment.blank?
              @subscription_id = active_subscription_id if active_subscription_id
              ::Payments::Mobile::UpdatePayment.new(payment, attributes).call
            end
          else
            redis_client.hset(redis_key, :error_code, response.status) unless redis_payment.blank?
            false
          end
        end

        private

        def mobile_user
          @mobile_user ||= MobileUser.where(operator: operator).find_by_msisdn(msisdn)
        end

        def active_subscription_id
          mobile_user && mobile_user.active_payment && mobile_user.active_payment.operator_subscription_id
        end

        def subscribe_end_date
          (Time.now + 30.days).end_of_day
        end
      end
    end
  end
end
