class PaymentProcess
  module Mobile
    module Tele2
      class Callback < ::PaymentProcess::Mobile::Base
        attr_reader :body, :errors, :subscription_id, :operator, :msisdn, :attributes

        def initialize(body)
          @body = body
          @operator = 'tele2'
          @errors = []
          @msisdn       = nil
          @access_token = nil
          @attributes   = { data: { 'operator' => 'tele2' } }
        end

        def verify!
          @msisdn = body['source_addr']
          commands = convert_short_message(body['short_message']).split('+')
          attributes[:data] = attributes[:data].merge!(body)
          if commands.count > 1
            case commands[1]
              when 'Activate'
                # create_subscription(*commands)
              when 'Stop'
                stop_subscription(*commands)
              when 'PayDay'
                prolong_subscription(*commands)
              when 'CreditPayment'
                credit_subscription(*commands)
              when 'ChangeMSISDN'
                change_msisdn(*commands)
              when 'ServiceStop'
                block_subscription(*commands)
            end
          else
            @errors =  ['Что-то пошло не так']
            redis_client.hset(redis_key, :error_code, body['error'])
            false
          end
        end

        def create_subscription(time = nil, command = nil, cause = nil, expire_date = nil, cost = nil, service_id = nil)
          #Схема Timestamp+Activate+Cause+ExpireDate+Cost+SubscriptionId
          @subscription_id = active_subscription_id || service_id
          @subscription_id ||= @body['destination_addr'].match(/\w+#(?<subscription_id>\d+)/)['subscription_id']
          attributes[:status] = :paid
          attributes[:start_date] = Time.parse(time)
          attributes[:end_date] = subscribe_end_date
          save_to_database!
        end

        def stop_subscription(time = nil, command = nil, cause = nil, result = nil, cost = nil, service_id = nil)
          # Схема  Timestamp+Stop+Cause+Result+Cost+SubscriptionId
          if success?(result)
            @subscription_id = active_subscription_id
            attributes[:end_date] = unsubscribe_end_date
            save_to_database! && fire_event!(:stop)
          end
        end

        def prolong_subscription(time = nil, command = nil, result = nil, expire_date = nil, cost = nil, service_id = nil)
          # Схема Timestamp+PayDay+Result+ExpireDate+Cost+SubscriptionId
          if success?(result)
            @subscription_id = active_subscription_id || service_id
            attributes[:status] = :paid
            attributes[:end_date] = subscribe_end_date
            save_to_database! && fire_event!(:prolong)
          else
            if (@subscription_id = active_subscription_id)
              attributes[:end_date] = unsubscribe_end_date
              save_to_database! && fire_event!(:stop)
            end
          end
        end

        def credit_subscription(time = nil, command = nil, creditline_id = nil, status = nil, result = nil)
          # Схема TimeStamp+CreditPayment+CreditlineId+CreditStatus+ChargeResult
          @subscription_id = active_subscription_id || creditline_id
          attributes[:end_date] = subscribe_end_date
          save_to_database! && fire_event!(:prolong)
        end

        def change_msisdn(time = nil, command = nil, new_msisdn = nil)
          # Схема Timestamp+ChangeMSISDN+MSISDN2Timestamp+ChangeMSISDN+MSISDN2
          if mobile_user
            mobile_user.msisdn = new_msisdn
            mobile_user.save!
          end
        end

        def block_subscription(time = nil, command = nil, cause = nil, service_id = nil)
          # Схема Timestamp+ServiceStop+Cause+SubscriptionId
          if mobile_user
            if (@subscription_id = active_subscription_id)
              attributes[:end_date] = Time.now
              save_to_database! && fire_event!(:stop)
            else
              @errors << 'Active subscription ID not found'
              false
            end
          end
        end

        def mobile_user
          @mobile_user ||= MobileUser.find_by_msisdn(msisdn)
        end

        def success?(result)
          if result.eql?('0') && mobile_user
            true
          else
            @errors << ["MobileUser #{msisdn} not found"]
            false
          end
        end

        def active_subscription_id
          mobile_user && mobile_user.active_payment && mobile_user.active_payment.try(:operator_subscription_id)
        end
      end
    end
  end
end
