class PaymentProcess
  module Mobile
    class Base < ::Payments::Mobile::Base

      def subscribe_end_date
        (Time.now + 10.days).end_of_day
      end

      def unsubscribe_end_date
        Time.now
      end

      def convert_short_message(message)
        message.delete!("\0")
        message
      end

      def payment
        unless @payment
          attributes.deep_merge!(data: { subscription_id: redis_key })
          @payment ||= Payment.find_by_subscription_id(redis_key)
          create_payment! if !@payment || @payment.paid? && @payment.inactive?
        end
        @payment
      end

      def access_token
        @access_token = redis_payment['access_token'] if @access_token.blank?
        @access_token
      end

      private

      def save_to_database!
        if payment
          @status = ::Payments::Mobile::UpdatePayment.new(payment, attributes).call
        else
          @errors << ["Payment #{subscription_id} not found"]
          @status = false
        end
      end

      def create_payment!
        @errors = [] if !@errors
        return unless subscription_id
        use_case = ::Payments::Mobile::CreatePayment.new(
            {
                subscription_id:  subscription_id,
                msisdn:           msisdn,
                access_token:     access_token,
                operator:         operator,
                start_date:       Time.now,
                payment_kit_path: [Settings.paywall_mobile.product, operator, Settings.paywall_mobile.subproduct, Settings.paywall_mobile.period].join('.'),
                paymethod_slug:   Settings.paywall_mobile.paymethod_slug
            }.merge(attributes)
        )
        if use_case.call
          @payment = use_case.payment
        else
          @errors += use_case.errors
          false
        end
      end

      def mobile_user
        @mobile_user = MobileUser.find_by_msisdn(msisdn)
      end

      def fire_event!(type, options = {})
        return true unless type
        Resque.enqueue(Workers::Statistics::FirePaymentEvent, type, {
                                                                payment_id:   payment.id,
                                                                status:       payment.status,
                                                                sum:         Settings.paywall_mobile.price,
                                                                paymethod:    Settings.paywall_mobile.paymethod_slug,
                                                                product:      Settings.paywall_mobile.product,
                                                                namespace:    operator,
                                                                subproduct:   Settings.paywall_mobile.subproduct,
                                                                period:       Settings.paywall_mobile.period,
                                                                operator:     operator,
                                                                msisdn:       msisdn,
                                                                access_token: access_token
                                                            }.merge(options))
      end
    end
  end
end
