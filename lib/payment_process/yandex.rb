# Payment by Yandex.Money
require 'builder'

class PaymentProcess
  class Yandex < self
    def start
      res = return_form(payment_url, payment_form_attributes)
      PaymentProcess.logger.info { "#{self.class.name} order_id: #{order.id}, start response: #{res}" }
      res
    end
    
    def check(parameters, client_ip)
      PaymentProcess.logger.info { "#{self.class.name} order_id: #{order.id}, check request: #{parameters}, client_ip: #{client_ip}" }
      return nil unless valid_ip?(client_ip)
      
      order.data['payment_check_parameters'] = parameters
      order.data['payment_check_ip'] = client_ip
      order.transaction_id = parameters['invoiceId']
      
      res = save_order_and_response(response_attributes(parameters))
      PaymentProcess.logger.info { "#{self.class.name} order_id: #{order.id}, check response: #{res}, client_ip: #{client_ip}" }
      res
    end
    
    def aviso(parameters, client_ip)
      PaymentProcess.logger.info { "#{self.class.name} order_id: #{order.id}, aviso request: #{parameters}, client_ip: #{client_ip}" }
      return nil unless valid_ip?(client_ip)
      
      order.data['payment_aviso_parameters'] = parameters
      order.data['payment_aviso_ip'] = client_ip
      
      attributes = response_attributes(parameters)
      if attributes[:code] == 0
        order.payment_status = :paid
        # открываем доступ
        order.grant_access
        order.send_user_mail
        # InnerMailer.new_subscription(order).deliver
      end
      res = save_order_and_response(attributes)
      PaymentProcess.logger.info { "#{self.class.name} order_id: #{order.id}, aviso response: #{res}, client_ip: #{client_ip}" }
      res
    end
    
  private
    def payment_url
      settings.url
    end
    
    def payment_form_attributes
      {
        shopId:         order.price.paper? ? settings.paper.shopid : settings.online.shopid,
        scid:           order.price.paper? ? settings.paper.scid : settings.online.scid,
        sum:            price,
        customerNumber: order.uuid,
        orderNumber:    order.uuid,
        shopSuccessURL: settings.client_success_url,
        shopFailURL:    settings.client_bad_url
      }
    end
    
    def settings
      Settings.yandex
    end
    
    def response_attributes(parameters)
      attributes = {
        performedDatetime: Time.now.strftime('%FT%T.%L%:z'),
        code:              0,
        shopId:            parameters['shopId'],
        invoiceId:         parameters['invoiceId'],
        orderSumAmount:    price
      }
      code, message = check_parameters(parameters)
      code > 0 ? attributes.merge(code: code, message: message) : attributes
    end
    
    def xmlresponse(attributes)
      x = Builder::XmlMarkup.new(indent: 1)
      x.instruct!
      x.checkOrderResponse(nil, attributes)
      x.target!
    end
    
    def save_order_and_response(attributes)
      unless order.save
        attributes = {
          code:    200,
          message: 'Попробуйте оплатить позже'
        }
      end
      snapshot_order('payments_yandex')
      
      return_body xmlresponse(attributes)
    end
    
    def valid_ip?(client_ip)
      valid = settings.ips.include?(client_ip)
      PaymentProcess.logger.error { "#{self.class.name} order_id: #{order.id}, ip not valid, client_ip: #{client_ip}" } unless valid
      valid
    end
    
    def check_parameters(parameters)
      p = %w(action orderSumAmount orderSumCurrencyPaycash orderSumBankPaycash shopId invoiceId customerNumber).map { |k| parameters[k] } << settings.shoppassword
      md5 = Digest::MD5.hexdigest(p.join(';')).upcase
      md5 == parameters['md5'] ? 0 : [1, "Ошибка авторизации"]
    end
  end
end
