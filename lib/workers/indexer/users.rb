require 'resque-lock-timeout'

module Workers
  module Indexer
    class Users
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::Users.index_changed
        logger.info "index #{count} changed users"
      end
    end
  end
end
