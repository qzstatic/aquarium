module Workers
  class SendSmsJob
    extend Workers::Logger

    @queue = :mailer

    def self.perform(msisdn, operator, message)
      logger.info "Begin sending SMS to #{msisdn}"
      sender = SmsSender.new(msisdn, operator, message)
      if sender.send!
        logger.info "Successfully send SMS to #{msisdn}"
      else
        logger.info "Unsuccessfully send SMS to #{msisdn}"
      end
    end
  end
end
