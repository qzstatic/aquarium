module Workers
  module Renewal
    class Apple
      extend Workers::Logger
      
      @queue = :payments
      
      class << self
        def perform(order_id)
          logger.info "Renewal apple order: #{order_id}"
          Order.by_apple.find(order_id).payment.renewal
        end
      end
    end
  end
end
