module Workers
  module Renewal
    class Google
      extend Workers::Logger
      
      @queue = :payments
      
      class << self
        def perform(order_id)
          logger.info "Renewal google order: #{order_id}"
          Order.by_google.find(order_id).payment.renewal
        end
      end
    end
  end
end
