module Workers
  class SLXUploadJob
    extend Workers::Logger
    
    @queue = :slx_upload
    
    class << self
      def perform
        logger.info "SLX Uploader started"
        cnt = SLXPaymentsUploader.call
        logger.info "SLX Uploader uploaded #{cnt} orders"
      end
    end
  end
end
