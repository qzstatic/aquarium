module Workers
  class SparkCompaniesSync
    extend Workers::Logger

    #TODO: Set correct queue name
    @queue = :news_import

    class << self
      def perform
        logger.info "Spark companies processor started."
        if Spark::CompaniesProcessor.new.process
          logger.info "Spark companies processor finished successfully!"
        else
          logger.info "Spark companies processor finished unsuccessfully!"
        end
      end
    end
  end
end