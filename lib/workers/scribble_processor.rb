module Workers
  class ScribbleProcessor
    extend Workers::Logger

    @queue = :news_import

    class << self
      def perform
        logger.info "Scribble broadcastings processor started."
        if ScribbleBroadcasts.new.process
          logger.info "Scribble broadcastings processor finished successfully!"
        else
          logger.info "Scribble broadcastings processor finished unsuccessfully!"
        end
      end
    end
  end
end