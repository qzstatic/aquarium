module Workers
  module Statistics
    class FirePaymentEvent
      extend Workers::Logger

      @queue = :payments

      def self.perform(event, options = {})
        if ::Statistics::PaymentEvent.new({ event: event }.merge(options.symbolize_keys)).send!
          logger.info 'Event has been sent successfully'
        else
          logger.info 'Event has been sent unsuccessfully'
        end
      end
    end
  end
end
