module Workers
  module PressReleases
    class FindCompaniesFeeds
      
      @queue = :press_releases
      
      class << self
        def perform
          ::PressReleases::Company.with_feeds.each do |c|
            Resque.enqueue(Workers::PressReleases::Importer, c.id)
          end
        end
      end
    end
  end
end
