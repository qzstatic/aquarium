module Workers
  class EpubJob
    extend Workers::Logger
    
    @queue = :flasher
    
    def self.perform(date_string = nil)
      logger.info "Start generating the epub"

      generator = Epub::IssueGenerator.new(date_string)
      epub_url = generator.generate

      if !epub_url.nil?
        logger.info "Epub generated, newsrelease date: '#{generator.newsrelease_date.to_s}', epub: '#{epub_url}'"
      else
        logger.info "Epub not generated, no newsrelease for date: '#{generator.newsrelease_date.to_s}'"
      end
    end
  end
end
