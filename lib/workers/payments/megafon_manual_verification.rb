module Workers
  module Payments
    class MegafonManualVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(subscription_id ,access_token)
        logger.info 'Start verifying Megafon subscription status manually'
        use_case = ::PaymentProcess::Mobile::Megafon::Manual.new(subscription_id, access_token)
        if use_case.verify!
          logger.info 'Successfully finished verifying Megafon subscription manually'
          true
        else
          logger.error use_case.error
          logger.info 'Unsuccessfully finished verifying Megafon subscription manually'
          false
        end
      end
    end
  end
end
