module Workers
  module Payments
    class GoogleVerify
      extend Workers::Logger

      @queue = :payments

      class << self
        def perform(subscription_id, token, session_id, client_ip, previous_payment_id=nil)
          logger.info "Verifing google subscription; session_id: #{session_id}, client_ip: #{client_ip}, subscription_id: #{subscription_id}, token: #{token}"

          receipt = Google::Verifier.verify(Settings.google.package_name, subscription_id, token)

          unless receipt.success?
            logger.error "Verify google subscription fail; subscription_id: #{subscription_id}, token: #{token}, client_ip: #{client_ip}, error: #{receipt.error.message}"
            return
          end
          logger.info "Verify google subscription success; subscription_id: #{subscription_id}, token: #{token}, client_ip: #{client_ip}, receipt: #{receipt}"

          # Найдем платеж:
          payment = Payment.bymethod('google').find_by_transaction_id(receipt.transaction_id)
          unless payment
            payment = new_payment_by_receipt(receipt, subscription_id, client_ip)
            payment.save

            if previous_payment_id && previous_payment = Payment.bymethod('google').find(previous_payment_id)
              payment.data[:previous_payment_id] = previous_payment.id
              payment.payment_kit_id = previous_payment.renewal_id
              payment.autopayment = true
              payment.save

              previous_payment.data[:next_payment_id] = payment.id
              previous_payment.data_will_change!
              previous_payment.save
              previous_payment.snapshots.create(
                event: 'payments_google_verify_renewal',
                data:  previous_payment.attributes
              )

              # копируем доступы из прошлого заказа
              previous_payment.access_rights.each do |ar|
                Common::GrantAccess.new(
                  payment:    payment,
                  reader:     ar.reader,
                  start_date: payment.start_date,
                  end_date:   payment.end_date
                ).call
              end
            end

            payment.snapshots.create(
              event: 'payments_google_verify',
              data:  payment.attributes
            )
          end

          if payment && session_id
            # привязываем текущую сессию к подпискам.
            session = Session.find(session_id)
            unless payment.access_rights.find { |ar| ar.reader == session }
              Common::GrantAccess.new(
                payment:    payment,
                reader:     session,
                start_date: payment.start_date,
                end_date:   payment.end_date
              ).call
            end

            if session.user
              # попробуем привязать подписку к пользователю
              ::Payments::LinkSubscription.new(session, session.user).call
            end
          end
        end

        def namespace
          @namespace ||= Namespace.joins(:product).where('products.slug=? and namespaces.slug=?', Settings.google.product, Settings.google.namespace).first
        end

        def new_payment_by_receipt(receipt, subscription_id, client_ip)
          start_date = Time.at(receipt.startTimeMillis.to_f / 1000)
          end_date   = Time.at(receipt.expiryTimeMillis.to_f / 1000)
          payment_kit = namespace.payment_kits.joins(:period).order("abs(extract('epoch' from periods.duration) - #{end_date - start_date})").first
          params = {
            payment_kit_id: payment_kit.id,
            price:          payment_kit.price,
            renewal_id:     payment_kit.renewal_id,
            paymethod:      Paymethod.find_by_slug('google'),
            transaction_id: receipt.transaction_id,
            start_date:     start_date,
            end_date:       end_date,
            data: {
              auto_renewing:   receipt.autoRenewing,
              receipt:         receipt.to_hash,
              subscription_id: subscription_id,
              token:           receipt.token,
              client_ip:       client_ip,
            }
          }

          Payment.paid.new(params)
        end
      end
    end
  end
end