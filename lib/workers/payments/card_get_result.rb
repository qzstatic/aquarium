module Workers
  module Payments
    class CardGetResult
      extend Workers::Logger

      @queue = :payments

      def self.perform(payment_id, client_ip = nil)
        logger.info "get result for payment #{payment_id}"
        payment  = Payment.notpaid.bymethod(:card).find(payment_id)
        use_case = ::Payments::Card::GetResult.new(payment)
        if use_case.call
          payment.status = :paid
          payment.grant_access
          payment.send_user_mail
          if payment.renewal_id && !payment.user.autopay
            user         = payment.user
            user.autopay = true
            user.save!
            Snapshot.create!(
                subject: user,
                event:   'payment_card_final',
                data:    user.attributes
            )
          end
        else
          payment.status = :canceled
        end

        payment.fire_event!

        payment.data['payment_result_response'] = use_case.response.to_hash if use_case.response
        payment.data_will_change!
        payment.save!
        Snapshot.create!(
            subject: payment,
            event:   'payment_card_final',
            data:    payment.attributes
        )
      end
    end
  end
end
