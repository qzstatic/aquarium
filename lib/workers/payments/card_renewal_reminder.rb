module Workers
  module Payments
    class CardRenewalReminder
      extend Workers::Logger

      @queue = :mailer

      def self.perform
        logger.info 'Find paid payments for send reminder'
        billing_date = 2.days.from_now
        Payment.bymethod(:card).paid.joins(:payment_kit).where("(payments.end_date - payment_kits.autopay_timeout)::timestamp::date = ?", billing_date.strftime('%Y-%m-%d')).each do |payment|
          if payment.can_renewal? && !payment.have_next_subscription? && payment.data.symbolize_keys[:biller_client_id].present?
            Resque.enqueue(Workers::PaymentMailer::ReminderEmail, payment.id, billing_date)
          end
        end
      end
    end
  end
end
