module Workers
  module Payments
    class BeelineManualVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(subscription_id, access_token)
        logger.info 'Start verifying Beeline subscription status manually'
        use_case = ::PaymentProcess::Mobile::Beeline::Manual.new(subscription_id, access_token)
        if use_case.verify!
          logger.info 'Successfully finished verifying Beeline subscription manually'
          true
        else
          logger.error use_case.error
          logger.info 'Unsuccessfully finished verifying Beeline subscription manually'
          false
        end
      end
    end
  end
end
