module Workers
  module Payments
    class MtsManualVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(subscription_id, access_token)
        logger.info 'Start verifying MTS subscription status manually'
        use_case = ::PaymentProcess::Mobile::Mts::Manual.new(subscription_id, access_token)
        if use_case.verify!
          logger.info 'Successfully finished verifying MTS subscription manually'
          true
        else
          logger.error use_case.error
          logger.info 'Unsuccessfully finished verifying MTS subscription manually'
          false
        end
      end
    end
  end
end
