module Workers
  module SubscriberMailer
    class ConfirmationEmailJob
      extend Workers::Logger
      
      @queue = :mailer
      
      class << self
        def perform(token_id)
          token = UserToken.find(token_id)
          contact = Contact.find(token.token_data['contact_id'])
          mail_type = ::Mailing::MailType.find(token.token_data['type_ids'].first)
          ::SubscriberMailer.confirmation_email(token, mail_type, contact.unsubscribe_token(mail_type.slug)).deliver
          logger.info "Contact confirmation email: token=#{token.token} contact=#{contact.id} email=#{contact.email} type_ids=#{token.token_data['type_ids']}"
        end
      end
    end
  end
end
