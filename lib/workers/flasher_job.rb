module Workers
  class FlasherJob
    extend Workers::Logger
    
    @queue = :flasher
    
    class << self
      def perform(document_id = nil)
        if document_id.nil?
          logger.info "Connect to Snipe..."
          snipe_client = SnipeClient.new
          
          logger.info "Getting newspaper from Snipe"
          newspapers = snipe_client.get("/v1/sandboxes/#{Settings.snipe.newspaper_sandbox_id}/documents").body
          
          document_id = newspapers.first[:id]
        end
          
        logger.info "Start coverting PDF to JPG/SWF, document #{document_id}"
        flash_processor = FlashProcessor.new(document_id)
        flash_processor.process
        logger.info "JPG/SWF files was generated"
      end
    end
  end
end
