module Workers
  class ServerMailerJob
    extend Workers::Logger
    
    @queue = :mailer
    
    class << self
      def perform
        result = ServerErrorsQuery.result
        
        if result.count > 5
          if ErrorsMailer.server_errors_email(result).deliver
            logger.info "Errors mail sent :)"
          else
            logger.info "Errors mail failed :("
          end
        end
      end
    end
  end
end
