module Workers
  class CleanImportedNews
    extend Workers::Logger
    
    @queue = :news_import
    
    def self.perform
      cnt = ImportNews.delete_all([ "news_date < ?", 1.month.ago ])
      logger.info "Removed #{cnt} old imported news"
    end
  end
end
