module Workers
  class NewsImporter
    extend Workers::Logger
    
    @queue = :news_import
    
    class << self
      def perform(slug)
        ImportSource.find_by_slug(slug).import
        logger.info "News from '#{slug}' imported"
      end
    end
  end
end
