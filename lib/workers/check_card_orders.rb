module Workers
  class CheckCardOrders
    extend Workers::Logger
    
    @queue = :payments
    
    class << self
      def perform
        cnt = Order.by_card.notpaid.where(created_at: 1.day.until..15.minute.until).where('transaction_id is not null').each do |order|
          order.payment.do_async('final')
        end.count
        logger.info("Checked #{cnt} unpaid card orders") if cnt>0
      end
    end
  end
end
