module Workers
  module Comments
    class UpdatePopular
      extend Workers::Logger
    
      @queue = :comments
    
      class << self
        def perform(widget_id, xid)
          logger.info "Update popualr comment: widget_id: #{widget_id}, xid: #{xid}"
          use_case = Shark::Comments::UpdatePopular.new(widget_id, xid)
          if use_case.call
            logger.info "Success update popualr comment: widget_id: #{widget_id}, xid: #{xid}"
          else
            logger.error "Failed update popualr comment: widget_id: #{widget_id}, xid: #{xid}"
          end
        end
      end
    end
  end
end
