module Workers
  class ZipperJob
    extend Workers::Logger
    
    @queue = :zipper
    
    class << self
      # Example:
      # date = {year: 2015, month: 2, day: 20}
      # image_type = [:mobile_high, :mobile_low]
      #
      def perform(date, image_versions = Newspaper::IMAGE_VERSIONS)
        image_versions.each do |image_version|
          use_case = Shark::NewsreleaseZip.new(date, image_version)
          use_case.call
          logger.info "ZIP-file file is generated and available at #{use_case.url}"
        end
      end
    end
  end
end
