module DultonMedia
  class Client < Base

    def upload_video(title, url)
      post('/media/records.json', {
                                    record: {
                                        name: title
                                    },
                                    source: {
                                        type: 'http',
                                        parameters: {
                                            url: url
                                        }
                                    }
                                })
    end
  end
end