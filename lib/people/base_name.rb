module People
  class BaseName

    def initialize(name)
      @name = name[:lex]
      @gr = name[:gr].split(',')
    end

    def gender
      case
        when @gr.include?('m')
          :male
        when @gr.include?('f')
          :female
        when @gr.include?('mf')
          :androgynous
        else
          :unknown
      end
    end

    def male?
      @gr.include?('m') || @gr.include?('mf')
    end

    def female?
      @gr.include?('f') || @gr.include?('mf')
    end

    def to_s
      @name.mb_chars.capitalize.to_s
    end
  end
end