class SnipeClient
  extend Forwardable
  
  def_delegators :connection, *%i(get post put patch delete)
  
  # Default authentication endpoint.
  AUTH_ENDPOINT = '/v1/access_token'.freeze
  
  attr_reader :access_token, :editor_id
  
  def initialize(access_token = nil)
    if access_token.nil?
      authenticate!
    else
      @access_token = access_token
    end
  end
  
private
  def connection
    Faraday.new(url: Settings.hosts.snipe) do |builder|
      builder.request  :authentication, access_token
      builder.request  :json
      builder.response :mashify
      builder.response :oj, content_type: 'application/json'
      builder.response :unauthorized_handler
      builder.adapter  :net_http_persistent
    end
  end
  
  def credentials_present?
    Settings.snipe.login.present? && Settings.snipe.password.present?
  end
  
  def credentials
    {
      login:    Settings.snipe.login,
      password: Settings.snipe.password
    }
  end
  
  def authenticate!
    raise ArgumentError, 'No editor credentials in Settings' unless credentials_present?
    
    response = connection.post(AUTH_ENDPOINT, editor: credentials)
    
    if response.success?
      @access_token = response.body[:access_token]
      @editor_id = response.body[:editor_id]
    else
      raise ArgumentError, 'Invalid credentials'
    end
  end
  
  def authenticated?
    access_token.present?
  end
  
  class SnipeAuth < Faraday::Middleware
    HEADER = 'X-Access-Token'.freeze
    
    attr_reader :access_token
    
    def initialize(app, access_token)
      @access_token = access_token
      super(app)
    end
    
    def call(env)
      env.request_headers[HEADER] = access_token unless access_token.nil?
      @app.call(env)
    end
  end
  
  Faraday::Request.register_middleware(authentication: SnipeAuth)
  
  class Unauthorized < StandardError; end
  
  class UnauthorizedHandler < Faraday::Response::Middleware
    def on_complete(response)
      raise Unauthorized if response.status == 401
    end
  end
  
  Faraday::Response.register_middleware(unauthorized_handler: UnauthorizedHandler)
end
