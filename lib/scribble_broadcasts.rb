class ScribbleBroadcasts
  POSTS_PER_PAGE = 99 #Максимальное количество постов, возвращаемых за один запрос

  def process
    documents = Roompen.categorized(%w(parts online), from: Date.today - 1.week)
    result = true
    documents.each do |document|
      next unless opened_scribble_broacasting?(document)
      scribble_document = Scribble::Document.new(document)
      options = { Max: POSTS_PER_PAGE, Order: :asc }
      options.merge!(since_option(scribble_document.scribble_check_time)) if scribble_document.scribble_check_time
      begin
        scribble_broadcasting = Scribble::Broadcasting.new(scribble_document)
        loop do
          scribble_broadcasting.store_next_scribble_posts_chunk(options)
          options.merge!(since_option(scribble_broadcasting.current_last_update_time))
          break if scribble_broadcasting.last_page?(POSTS_PER_PAGE)
        end
        result = scribble_broadcasting.process_and_upload
      rescue Exception
        result &&= false
      end
    end
    result
  end

  private

  def opened_scribble_broacasting?(document)
    document.respond_to?(:scribble_id) && !document.scribble_id.blank? && document.respond_to?(:finished) && !document.finished
  end

  def since_option(time)
    { Since: time.utc.to_s.chomp(' UTC') }
  end
end