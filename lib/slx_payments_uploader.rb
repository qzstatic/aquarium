require 'net/ftp'

class SLXPaymentsUploader
  include Procto.call

  ENCODING = 'windows-1251'
  
  def call
    return if payments.count == 0
    tmpfile_payments = Tempfile.new('payments', :encoding => ENCODING)
    tmpfile_users = Tempfile.new('users', :encoding => ENCODING)
    begin
      tmpfile_payments.write(payments_content)
      tmpfile_payments.close
      tmpfile_users.write(users_content)
      tmpfile_users.close
      Net::FTP.open(Settings.slx.ftp.host) do |ftp|
        ftp.passive = true
        ftp.login(Settings.slx.ftp.user, Settings.slx.ftp.password)
        ftp.put(tmpfile_payments.path, remote_payments_filename)
        ftp.put(tmpfile_users.path, remote_users_filename) if users.count > 0
      end
      mark_as_synced
    ensure
      tmpfile_payments.unlink
      tmpfile_users.unlink
    end
  end
  
  def remote_payments_filename
    ERB.new(Settings.slx.ftp.payments_filename).result
  end
  
  def remote_users_filename
    ERB.new(Settings.slx.ftp.users_filename).result
  end
  
  def payments_content
    payments.map do |o|
      payment_slx_fields(o).join("\t")
    end.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end
  
  def users_content
    users.map do |u|
      user_slx_fields(u).join("\t")
    end.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end

  def payments
    @payments ||= Payment.joins(:payment_kit).where('payment_kits.syncable = true').unsynced.order(id: :desc)
  end
  
  def users
    @users = payments.map(&:user).compact
  end
  
  def mark_as_synced
    cnt = payments.each &:synced!
    @payments = nil
    cnt
  end
  
  def payment_slx_fields(payment)
    res = []
    user = payment.user
    data = payment.data.symbolize_keys
    prev_payment = data[:previous_payment_id] && Payment.find_by_id(data[:previous_payment_id])
    res << payment.subproduct.slx_slug
    res << payment.created_at.strftime('%F %T')
    res << (payment.start_date && payment.start_date.strftime('%F %T') || ' ')
    res << (payment.end_date && payment.end_date.strftime('%F %T') || ' ')
    res << payment.period.duration.div(1.month)
    res << payment.price
    res << 'N'
    res << payment.id
    res << (user && user.id || 0)
    res << (payment.paid? ? 'Y' : 'N')
    res << payment.paymethod.slug
    res << (user && user.phone || ' ')
    res << (user && user.email || ' ')
    res << (payment.can_renewal? ? 'Y' : 'N')
    res << (user && user.subscribe_base_id || 0)
    res << (prev_payment && prev_payment.subscribe_base_id || 0)
    res << (prev_payment && (prev_payment.data['old_order_id'] || prev_payment.id) || 0)
    res
  end
  
  def user_slx_fields(user)
    res = []
    res << ''
    res << user.last_name
    res << user.first_name
    res << user.phone
    res << user.email
    res << 'N' # льготный/нельготный
    res << user.id
    res << user.subscribe_base_id
    res
  end
end
