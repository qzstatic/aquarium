require 'zip'

class Unzipper
  attr_reader :output_dir, :input_file
  
  # Initialize with the directory to unzip and the location of the input archive.
  #
  def initialize(output_dir, input_file)
    @output_dir = output_dir
    @input_file = input_file
  end
  
  # Unzip file to directory.
  #
  def unzip
    Zip::File.open(input_file) do |zip_file|
      zip_file.each do |entry|
        puts "Extracting #{entry.name}"
        entry.extract("#{output_dir}/#{entry.name}")
      end
    end
  end
end
