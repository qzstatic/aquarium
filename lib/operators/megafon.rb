require 'csv'
module Operators
  class Megafon < Base

    attr_reader :connection

    private :connection

    def initialize
      super
      @connection = Faraday.new(url: Settings.megafon.api.host) do |builder|
        builder.response :mashify
        builder.adapter  :net_http_persistent
        builder.use :gelf_logger, 'megafon'
        builder.options[:open_timeout] = 2
        builder.options[:timeout] = 5
      end
    end

    def create_subscription(msisdn = nil, access_token = nil)
      params = {status: 'new'}
      if msisdn && !msisdn.empty?
        params.merge!(msisdn: msisdn, serviceid: Settings.megafon.wifi_service_id)
      else
        params.merge!(serviceid: Settings.megafon.service_id)
      end
      subscription_action(access_token, params, generate_guid)
    end

    def destroy_subscription(service = :wap, msisdn=nil, subscription_id=nil, access_token=nil)
      params = {
          status: 'exit',
          serviceid: service.eql?(:wap) ? Settings.megafon.service_id : Settings.megafon.wifi_service_id,
          msisdn: msisdn
      }
      subscription_action(access_token, params, subscription_id)
    end

    def subscription_action(access_token = nil, params = {}, subscription_id = nil)

      return_url = Addressable::URI.parse(Settings.megafon.return_url)
      return_url.query_values = (return_url.query_values || {}).merge({subscription_id: subscription_id})
      params.merge!(
          {
              returnurl: return_url.to_s,
              transactid: subscription_id
          }
      )
      response = get("#{Settings.megafon.api.base}tmpurl/", params, access_token)
      if !response.body.eql?(200)
        logger.error(response.body)
      end
      [subscription_id, redirect_url(response)]
    end

    def redirect_url(response)
      if response.status.eql?(200)
        if Settings.megafon.test_number
          "#{response.body}?testMSISDN=#{Settings.megafon.test_number}"
        else
          response.body
        end
      end
    end

    def verify_subscription!(subscription_id, access_token = nil)
      params = {
          transactid: subscription_id
      }
      get("#{Settings.megafon.api.base}subinfo/", params, access_token)
    end

    def get_service_events(service = :wap, date_from = 1.day.ago, date_to = 1.from_now)
      params = {
          date_from: date_from.strftime('%Y-%m-%d'),
          date_to: date_to.strftime('%Y-%m-%d'),
          serviceid: service.eql?(:wap) ? Settings.megafon.service_id : Settings.megafon.wifi_service_id
      }
      response = get("#{Settings.megafon.api.base}checkevents", params)
      @csv = nil
      if response.status.eql?(200)
        CSV.parse(response.body, {:col_sep => ';'})
      else
        logger.error(response.body)
        nil
      end
    end

    private

    %i(get post put patch delete).each do |verb|
      define_method(verb) do |path, params = {}, access_token = ''|
        begin
          connection.send(verb, path, params) do |req|
            req.headers['Gelf-Access-Token'] = access_token if access_token && !access_token.empty?
          end
        rescue Net::HTTP::Persistent::Error => e
          raise unless e.message =~ /too many connection resets/
          retry
        end
      end
    end
  end
end
