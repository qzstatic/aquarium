module Operators
  class Beeline < ::Operators::Base

    attr_reader :connection
    private :connection

    def initialize
      super
      @connection = Faraday.new(url: settings.api.host) do |builder|
        builder.response :mashify
        builder.adapter  :net_http_persistent
        builder.authorization :Basic, Base64.encode64("#{settings.login}:#{settings.password}")
        builder.use :gelf_logger, operator_name
        builder.options[:open_timeout] = 2
        builder.options[:timeout] = 5
      end
      @const_params = {
          contentURL: settings.content_url,
          forwardURL: settings.forward_url,
          chargeLevel: settings.charge_level,
          flagSubscribe: 'True'
      }
    end

    def create_subscription(msisdn = nil, access_token = nil)
      params = @const_params
      params.merge!(msisdn_cp: msisdn) if msisdn
      response = get(settings.api.service.to_s, params, access_token)
      [get_service_id(response), get_redirect(response)]
    end

    def get_redirect(response)
      response.headers['Location']  if response.status.eql?(302)
    end

    def get_service_id(response)
      $1 if response.status == 302 && response.headers['Location'] =~ /serviceId=(\d+)/
    end

    def verify_subscription(service_id, access_token = nil)
      response = get(settings.api.service.to_s, {serviceId: service_id}, access_token)
      i = 0
      while !response.success? && i < settings.attempts do
        response = get(settings.api.service.to_s, {serviceId: service_id}, access_token)
        i += 1
      end
      response
    end

    def settings
      Settings.beeline
    end

    def operator_name
      self.class.to_s.demodulize.downcase
    end

    private

    %i(get post put patch delete).each do |verb|
      define_method(verb) do |path, params = {}, access_token = nil|
        begin
          connection.send(verb, path, params) do |req|
            req.headers['Gelf-Access-Token'] = access_token if access_token && !access_token.empty?
          end
        rescue Net::HTTP::Persistent::Error => e
          raise unless e.message =~ /too many connection resets/
          retry
        end
      end
    end
  end
end
