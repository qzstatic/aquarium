module Operators
  class Base
    attr_reader :client

    class << self
      def system_subscription_id(operator, subscription_id)
        "#{operator}:#{subscription_id}" if subscription_id
      end
    end

    def initialize
      @errors = YAML.load_file(Rails.root.join('lib', 'operators', "#{self.class.to_s.demodulize.downcase}.yml"))
    end

    def create_subscription(*args)
      throw 'Implement this method in your class'
    end

    def generate_guid
      SecureRandom.uuid
    end

    def logger
      Rails.logger
    end

    def error(kind, code)
      @errors[kind.to_s]["code_#{code}"]
    end

    def internal_error(kind: , code: )
      if kind
        error = error(kind, code) || { 'internal_error_code' => 0 }
        { error: ::Operators.internal_error(error['internal_error_code']), code: error['internal_error_code'] }
      else
        { error: ::Operators.internal_error(code), code: code }
      end
    end

    private

    def store_in_database
      ActiveRecord::Base.transaction do
        Order.create
      end
    end
  end
end
