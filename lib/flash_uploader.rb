class FlashUploader
  attr_accessor :document_id, :archive_filename, :upload_data, :snipe_client, :attachment_data
  
  ARCHIVE_FILENAME = '/Users/foxweb/work/flash/Archive.zip'.freeze
  
  def initialize(document_id, archive_filename = ARCHIVE_FILENAME)
    @document_id = document_id
    @archive_filename = Pathname.new(archive_filename)
  end
  
  def process
    upload
    create_attachment
    create_box
    
    "#{Settings.hosts.agami}#{upload_data[:versions][:original][:url]}"
  end

private
  def upload(filename = nil)
    @upload_data = AgamiUploader.upload(archive_filename.to_s).body
  end
  
  def create_attachment
    @attachment_data = snipe_client.post('/v1/attachments', { attributes: upload_data }).body
  end
  
  def create_box
    data = {
      attributes: {
        type: 'newsrelease_archive',
        parent_id: root_box[:id],
        position: 1,
        enabled: true,
        pdf_id: attachment_data[:id]
      }
    }
    
    @box = snipe_client.post('/v1/boxes', data).body
  end
  
  def document
    snipe_client.get("/v1/documents/#{document_id}").body
  end
  
  def document_boxes
    snipe_client.get("/v1/documents/#{document_id}/boxes").body
  end
  
  def root_box
    snipe_client.get("/v1/documents/#{document_id}/root_box").body
  end
  
  def editor
    snipe_client.get('/v1/session').body
  end
  
  def snipe_client
    @snipe_client ||= SnipeClient.new
  end
end
