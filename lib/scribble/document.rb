class Scribble::Document
  attr_reader :document, :root_box, :settings

  def initialize(document)
    @document = document
    @settings = snipe_client.get("/v1/documents/#{document.id}").body.settings
    @root_box = @snipe_client.get("/v1/documents/#{document.id}/boxes").body.first
  end

  def scribble_check_time
    @scribble_check_time ||= Time.parse(@document.scribble_check_time) rescue nil
  end

  def id
    @document.id
  end

  def scribble_id
    @document.scribble_id
  end

  private

  def snipe_client
    @snipe_client ||= SnipeClient.new
  end
end