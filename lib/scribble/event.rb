class Scribble::Event
  def initialize
    @request = Scribble::Request.new('event')
  end

  def info(id, options = {})
    @request.get(id, options)
  end

  def page(id, page, options = {})
    @request.get("#{id}/page/#{page}", options)
  end

  def posts(id, options = {})
    @request.get("#{id}/posts", options)
  end

  def all(id, options = {})
    @request.get("#{id}/all", options)
  end
end