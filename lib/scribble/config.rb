class Scribble::Config
  attr_accessor :host, :username, :password, :token

  def initialize
    @host, @username, @password, @token = nil, nil, nil, nil
  end
end