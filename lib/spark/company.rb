module Spark
  class Company
    include ERB::Util

    attr_reader :common_info, :coowners_egrul, :coowners_fcsm, :coowners_rosstat, :finance_info

    def initialize(spark_company_doc, spark_coowners_doc)
      @company_doc      = get_report_from_spark_response(spark_company_doc)
      @coowners_doc     = get_report_from_spark_response(spark_coowners_doc)
      @common_info      = {}
      @coowners_egrul   = {}
      @coowners_fcsm    = {}
      @coowners_rosstat = {}
      @finance_info     = {
          periods: [],
          data: {
              'Внеоборотные активы' => [],
              'Оборотные активы' => [],
              'Активы  всего' => [],
              'Капитал и резервы' => [],
              'Долгосрочные обязательства' => [],
              'Краткосрочные обязательства' => [],
              'Выручка от продажи (за минусом НДС, акцизов ...)' => [],
              'Валовая прибыль' => [],
              'Прибыль (убыток) до налогообложения' => [],
              'Чистая прибыль (убыток)' => []
          }
      }
    end

    def process_and_upload(snipe_client, company_id)
      @snipe_client = snipe_client
      begin
        return true unless @company_doc || @coowners_doc
        process_common_info
        process_finance_info
        process_coowners
        html_tables = []
        html_tables.push(generate_html(:common))           unless @common_info.empty?
        html_tables.push(generate_html(:coowners_egrul))   unless @coowners_egrul.except(:count, :actual_date).empty?
        html_tables.push(generate_html(:coowners_fcsm))    unless @coowners_fcsm.except(:count, :actual_date).empty?
        html_tables.push(generate_html(:coowners_rosstat)) unless @coowners_rosstat.except(:count, :actual_date).empty?
        html_tables.push(generate_html(:finance))          unless @finance_info[:periods].empty?
        html_tables.unshift(generate_html(:logo))          unless html_tables.empty?
        init_embed_boxes!(company_id)
        update_document_boxes!(html_tables, company_id) unless @embed_boxes.map(&:body) == html_tables
      rescue Exception => e
        Resque.logger.error("Error processing company ID: #{company_id}. Error: #{e.inspect}")
        return false
      end
      true
    end

    private

    def update_document_boxes!(html_tables, company_id)
      root_box = @snipe_client.get("/v1/documents/#{company_id}/root_box").body
      @embed_boxes.each { |box| @snipe_client.delete("/v1/boxes/#{box[:id]}") }
      html_tables.each_with_index do |html_table, index|
        @snipe_client.post('/v1/boxes', attributes: {
                                         type:      'inset_media',
                                         parent_id: root_box[:id],
                                         position:  @position_offset.to_i + 10 + 1 * (index + 1),
                                         body:      html_table
                                     }
        )
      end
      @snipe_client.patch("/v1/documents/#{company_id}/publish")
    end

    def get_report_from_spark_response(spark_response_doc)
      matches = spark_response_doc.to_s.match(/<xmlData>(?<xml_data>.*)<\/xmlData>/)
      if matches[:xml_data]
        doc = Nokogiri::XML(CGI.unescapeHTML(matches[:xml_data]))
        doc.encoding = 'utf-8'
        doc.xpath('//Report').each { |report_node| return report_node if report_node.at_xpath('EGRPOIncluded').try(:text).to_s == 'true' }
      end
      nil
    end

    def process_common_info
      @common_info[:short_name] = @company_doc.xpath('ShortNameRus').first.try(:text)
      leader_node = @company_doc.xpath('LeaderList/Leader').max_by { |leader| leader['ActualDate'] }
      @common_info[:leader] = {name: leader_node.attr('FIO'), position: leader_node.attr('Position')} if leader_node
    end

    def process_finance_info
      @company_doc.xpath('Finance/FinPeriod').each_with_index do |fin_period_node, index|
        @finance_info[:periods].push(fin_period_node.attr('PeriodName'))
        fin_period_node.xpath("StringList/String[@Name='#{@finance_info[:data].keys.join('\' or @Name=\'')}']").each do |finance_string_node|
          @finance_info[:data][finance_string_node.attr('Name')][index] = finance_string_node.attr('Value')
        end
      end
    end

    def process_coowners
      @coowners_rosstat = process_single_coowners_category(@coowners_doc.xpath('CoownersRosstat/CoownerRosstat'))
                              .merge(get_additional_coowners_info_for('Rosstat'))
      @coowners_egrul   = process_single_coowners_category(@coowners_doc.xpath('CoownersEGRUL/CoownerEGRUL'))
                              .merge(get_additional_coowners_info_for('EGRUL'))
      @coowners_fcsm    = process_single_coowners_category(@coowners_doc.xpath('CoownersFCSM/CoownerFCSM'))
                              .merge(get_additional_coowners_info_for('FCSM'))
    end

    def process_single_coowners_category(node_set)
      coowners = {}
      node_set.each do |coowner_node|
        current_coowner = node_children_to_hash(coowner_node)
        coowners[current_coowner[:name]] ||= current_coowner.except(:name)
        is_current_coowner_newer = begin
          Time.strptime(coowners[current_coowner[:name]][:actual_date].to_s, '%Y-%m-%d') < Time.strptime(current_coowner[:actual_date].to_s, '%Y-%m-%d')
        rescue Exception
          nil
        end
        coowners[current_coowner[:name]].merge!(current_coowner.except(:name)) if is_current_coowner_newer
      end
      coowners
    end

    def generate_html(template)
      ERB.new(File.read(Rails.root.join("app/views/spark/#{template}.erb"))).result(binding)
    end

    def node_children_to_hash(node)
      node.xpath('*').each_with_object({}) { |node_child, coowner_hash| coowner_hash.merge!(node_child.name.underscore.to_sym => node_child.text) }
    end

    def get_additional_coowners_info_for(type)
      {
          count: @company_doc.at_xpath("StructureInfo/CountCoowner#{type}").try(:text),
          actual_date: @coowners_doc.at_xpath("Coowners#{type}").try(:attr, 'ActualDate')
      }
    end

    def init_embed_boxes!(document_id)
      boxes = @snipe_client.get("/v1/documents/#{document_id}/boxes").body.first['children'] || []
      index = -1
      boxes.each_with_index do |box, i|
        if box.kind == 'h1'
          index = i - 1
          break
        end
      end
      main_tab_boxes = boxes[0..index]
      @embed_boxes = main_tab_boxes.select { |box| box.type == 'inset_media' }
      @position_offset = (main_tab_boxes - @embed_boxes).last.try(:position)
    end
  end
end
