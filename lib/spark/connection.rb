module Spark
  module Connection

    class << self
      def configure
        @config = Spark::Connection::Configuration.new
        yield(@config) if block_given?
      end

      def start
        configure unless @config
        @savon_client = Savon.client(wsdl: @config.url) { convert_request_keys_to :none }
        authenticate
        yield(@savon_client, @auth_cookies) if block_given? && @auth_cookies
        destroy_session
      end

      private

      def authenticate
        auth_result = @savon_client.call(:authmethod, message: {Login: @config.login, Password: @config.password})
        @auth_cookies = auth_result.http.cookies unless auth_result.to_s.match(/\<AuthmethodResult\>False\<\/AuthmethodResult\>/)
      end

      def destroy_session
        @savon_client.call(:end, cookies: @auth_cookies)
      end
    end

    class Configuration

      attr_accessor :url, :login, :password

      def initialize
        @url      = Settings.spark.server
        @login    = Settings.spark.login
        @password = Settings.spark.password
      end
    end
  end
end