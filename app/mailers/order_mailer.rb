class OrderMailer < ActionMailer::Base
  default from: Settings.mailer.from
  
  def order_online_email(user, order)
    @user  = user
    @order = order
    mail(to: @user.email, subject: 'Ваша подписка на «Ведомости» оплачена')
  end
  
  def order_profi_email(user, order)
    @user  = user
    @order = order
    mail(to: @user.email, subject: 'Ваша подписка на «Ведомости» оплачена')
  end
  
  def order_paper_email(user, order)
    @user  = user
    @order = order
    mail(to: @user.email, subject: 'Ваша подписка на «Ведомости» оплачена')
  end
  
  def order_courier_email(user)
    @user  = user
    mail(to: @user.email, subject: 'Ваш заказ на подписку принят')
  end

  def order_conf_translation_email(user, order)
    @user  = user
    @order = order
    mail(to: @user.email, subject: 'Оформлен заказ на сайте Vedomosti.ru')
  end
end
