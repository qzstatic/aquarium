class MailingMailer < ActionMailer::Base
  default from: Settings.mailer.from,
          content_transfer_encoding: 'quoted-printable'
  
  def test_email(mailing, email)
    @template = Pigeon::MailingTemplateRender.new(mailing).template_preview
    
    mail(to: email, subject: mailing.subject)
  end
end
