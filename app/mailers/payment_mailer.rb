class PaymentMailer < ActionMailer::Base
  default from: Settings.mailer.from
  
  def new_subscription(user, payment)
    @user  = user
    @payment = payment
    mail(to: @user.email, subject: 'Вы оформили подписку')
  end

  def new_autopayment(user, payment)
    @user = user
    @payment = payment
    mail(to: @user.email, subject: 'Ваша подписка продлена')
  end

  def subscription_reminder(payment, billing_date)
    @payment  = payment
    @price = @payment.renewal ? @payment.renewal.price : @payment.payment_kit.renewal ? @payment.payment_kit.renewal.price  : @payment.payment_kit.price
    @period = @payment.renewal ? @payment.renewal.period.title : @payment.payment_kit.renewal ? @payment.payment_kit.renewal.period.title : @payment.payment_kit.period.title
    @billing_date = billing_date
    mail(to: @payment.user.email, subject: 'Уведомление об автопродлении подписки')
  end
end
