# Query object for users search.
class UsersSearchQuery
  attr_reader :relation
  
  COLUMNS = %w(first_name last_name nickname company)
  
  def initialize(relation = User.all)
    @relation = relation
  end
  
  def search(search_string)
    relation.where(condition, query: "%#{search_string.strip}%")
  end
  
  def find_by_email(email)
    relation.find_by!(email: email.strip)
  end
  
private
  def condition
    COLUMNS.map do |column|
      "#{column} ILIKE :query"
    end.join(' OR ')
  end
end
