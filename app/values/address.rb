class Address < Entity
  attributes :region, :city, :zip, :street, :house, :housing, :building, :flat
  attribute :comment
end
