class Entity
  def initialize(attributes)
    @attributes = attributes.symbolize_keys.slice(*self.class.attributes)
  end
  
  def as_json(*)
    attributes
  end
  
  attr_reader :attributes
  private     :attributes
  
  class << self
    def attribute(attr_name)
      attributes << attr_name.to_sym
      
      define_method(attr_name) do
        attributes[attr_name]
      end
    end
    
    def attributes(*attr_names)
      attr_names.each do |attr_name|
        attribute(attr_name)
      end
      
      @attributes ||= Set.new
    end
  end
end
