module Ibis
  class PaymentsController < ApplicationController
    skip_before_action :editor_id, only: %i(subscribers_counts)

    def index
      payments = Payments::AllPayments.call(params[:page], params[:mobile])
      render json: payments, each_serializer: params[:search] ? PaymentSearchSerializer : PaymentListSerializer
    end

    def count
      render json: {
          payments: params[:mobile] ?  Payment.mobile.count : Payment.not_mobile.count,
          pages:  params[:mobile] ? Payment.mobile.page.total_pages : Payment.not_mobile.page.total_pages,
          payments_today: Payment.where("created_at > date_trunc('day', now())").count
      }
    end

    def show
      payment = Payment.find(params[:id])
      render json: payment, serializer: for_search? ? PaymentSearchSerializer : PaymentSerializer
    end

    def create
      use_case = Payments::CreatePayment.new(attributes, editor_id)

      if use_case.call
        render json: use_case.payment, serializer: PaymentSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdatePayment.new(params[:id], attributes, editor_id)

      if use_case.call
        render json: use_case.payment, serializer: PaymentSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def statuses
      respond_with Payment::STATUSES.map {|s| {slug:s[0], title:s[1]} }
    end

    def export
      e = PaymentsExport.new(
          Time.zone.parse(params.require(:start_date)),
          params[:end_date] ? Time.zone.parse(params[:end_date]) : Time.now
      )

      response.headers['Content-Length'] = e.zip.bytes.size.to_s
      send_data e.zip, filename: "payment_export_#{Time.now.strftime("%Y-%m-%d-%H-%M")}.zip"
    end

    def subscribers_counts
      use_case = Ibis::Payments::SubscribersCounts.new(params)

      if use_case.call
        render json: use_case.stats
      else
        render plain: "Ошибка: #{use_case.error}"
      end
    end

    private
    def for_search?
      params[:view] == 'search'
    end
  end
end
