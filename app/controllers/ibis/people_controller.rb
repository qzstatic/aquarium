module Ibis
  class PeopleController < ApplicationController
    before_action :validate_synonym_params, only: [:synonyms]
    # GET /people
    # GET /people.json
    def index
      start_date = Time.parse(params[:start_date]) rescue nil

      @people = Person.limit(200)
      @people.where!(processed: params[:processed].to_s =='true') if params[:processed]
      @people.where!('created_at > ?', start_date) if start_date

      render json: @people
    end

    # GET /people/synonyms
    def synonyms
      first_name, last_name = params[:name].split(/\s/)
      respond_with Petrovich(firstname: first_name, lastname: last_name, gender: params[:gender]), serializer: Ibis::SynonymsSerializer
    end

    # PATCH/PUT /people/1
    # PATCH/PUT /people/1.json
    def update
      @person = Person.find(params[:id])

      if @person.update(person_params)
        head :no_content
      else
        render json: @person.errors, status: :unprocessable_entity
      end
    end

    private

    def person_params
      params.require(:attributes).require(:person)
    end

    def validate_synonym_params
      render nothing: true unless params.require(:name) =~ /[А-ЯЁ][а-яё]+\s[А-ЯЁ][а-яё]+/ && gender_valid?
    end

    def gender_valid?
      params[:gender] ? %w(male female).include?(params[:gender]) : true
    end
  end
end
