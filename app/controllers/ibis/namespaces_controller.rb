module Ibis
  class NamespacesController < ApplicationController
    def index
      render json: Namespace.order(start_date: :desc), each_serializer: NamespaceSimpleSerializer
    end

    def show
      namespace = Namespace.find(params.require(:id))
      render json: namespace, serializer: NamespaceSerializer
    end

    def create
      use_case = Payments::CreateNamespace.new(attributes, editor_id)
      if use_case.call
        render json: use_case.namespace, serializer: NamespaceSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdateNamespace.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.namespace, serializer: NamespaceSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def payment_kits
      render json: PaymentKit.where(namespace_id: params[:namespace_id]).joins(:subproduct, :period).order('subproducts.position, periods.duration'), each_serializer: PaymentKitSimpleSerializer
    end

    def set_base
      use_case = Payments::SetBaseNamespace.new(params[:namespace_id], editor_id)
      if use_case.call
        render json: use_case.namespace, serializer: NamespaceSerializer
      else
        render json: {error: use_case.error}, status: :unprocessable_entity
      end
    end
    
    def unsyncable
      namespace_ids = PaymentKit.where(syncable: false).pluck(:namespace_id).uniq
      namespaces = Namespace.where(id: namespace_ids).order(updated_at: :desc)
      render json: namespaces, each_serializer: NamespaceSimpleSerializer
    end
    
    def unsyncable_payment_kits
      payment_kits = PaymentKit.where(namespace_id: params[:id], syncable: false).order(updated_at: :desc)
      render json: payment_kits, each_serializer: PaymentKitSimpleSerializer
    end
  end
end
