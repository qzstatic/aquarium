module Ibis
  class PaymethodsController < ApplicationController
    def index
      render json: Paymethod.order(:title), each_serializer: PaymethodSerializer
    end

    def show
      paymethod = Paymethod.find(params.require(:id))
      render json: paymethod, serializer: PaymethodSerializer
    end

    def create
      use_case = Payments::CreatePaymethod.new(attributes, editor_id)
      if use_case.call
        render json: use_case.paymethod, serializer: PaymethodSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdatePaymethod.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.paymethod, serializer: PaymethodSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end
  end
end
