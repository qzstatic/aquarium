module Ibis
  class DultonController < Ibis::ApplicationController

    #POST dulton/upload
    #Uploads video to dulton from given URL
    def upload
      res = dulton_client.upload_video(title, url)
      render json: { record: res.data.try(:record) }, status: res.status
    end

    private

    def dulton_client
      @dulton_client ||= DultonMedia::Client.new
    end

    def url
      params.require(:url)
    end

    def title
      params.require(:title)
    end
  end
end