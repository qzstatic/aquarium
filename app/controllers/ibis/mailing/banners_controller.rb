module Ibis
  class Mailing::BannersController < ApplicationController
    def index
      mailing_banners = ::Mailing::Banner.order(id: :desc).page(page)
      mailing_banners.where!('start_date >= :start_date AND end_date >= :start_date', start_date: params[:start_date]) if params[:start_date]
      mailing_banners.where!('start_date <= :end_date AND end_date <= :end_date', end_date: params[:end_date]) if params[:end_date]
      mailing_banners.where!('title ILIKE ?', "%#{params[:query]}%") unless params[:query].blank?
      render json: mailing_banners, each_serializer: Mailing::BannerSerializer, root: :banners, meta: meta_attributes(mailing_banners)
    end

    def count
      render json: { mailing_banners: ::Mailing::Banner.count, pages: ::Mailing::Banner.page.total_pages }
    end

    def show
      mailing_banner = ::Mailing::Banner.find(params[:id])
      render json: mailing_banner, serializer: Mailing::BannerSerializer
    end
    
    def create
      use_case = Mailing::CreateBanner.new(attributes, editor_id)
      
      if use_case.call
        render json: use_case.banner, serializer: Mailing::BannerSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def update
      use_case = Mailing::UpdateBanner.new(params[:id], attributes, editor_id)
      
      if use_case.call
        render json: use_case.banner, serializer: Mailing::BannerSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    private

    def page
      params[:page] || 1
    end
  end
end
