module Ibis
  class ImportNewsController < ApplicationController
    def show
      news = ::ImportNews.find(params.require(:id))
      render json: ImportNewsSerializer.new(news)
    rescue ActiveRecord::RecordNotFound => e
      render json: {error: 'Not found'}, status: :not_found
    end

    def index
      use_case = Ibis::ImportNews::Last.new(params)
      render json: use_case.call.map { |n| ImportNewsSerializer.new(n) }
    end
  end
end
