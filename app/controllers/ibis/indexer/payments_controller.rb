class Ibis::Indexer::PaymentsController < ApplicationController
  def fresh
    respond_with Payment.where('updated_at > ?', Time.at(params[:timestamp].to_i)).order(updated_at: :desc).limit(1000).pluck(:id)
  end
  
  def page
    respond_with Payment.order(updated_at: :desc).page(params[:page]).pluck(:id)
  end
end
