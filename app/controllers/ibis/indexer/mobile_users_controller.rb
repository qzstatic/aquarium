class Ibis::Indexer::MobileUsersController < ApplicationController
  # Lists ids of users filtered by updated_at after specified timestamp.
  #
  # `GET /ibis/indexer/users/fresh/:timestamp`
  #
  def fresh
    respond_with MobileUser.where('updated_at > ?', Time.at(params[:timestamp].to_i)).limit(1000).pluck(:id)
  end

  # Lists ids of documents by page and ordered by updated_at.
  #
  # `GET /ibis/indexer/users/page/:page`
  #
  def page
    respond_with MobileUser.order(updated_at: :desc).page(params[:page]).pluck(:id)
  end
end
