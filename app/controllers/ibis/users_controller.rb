module Ibis
  class UsersController < ApplicationController
    def index
      users = AllUsers.call(params[:page])
      render json: users
    end
    
    def count
      render json: { users: User.count, pages: User.page.total_pages }
    end
    
    def find_by_email
      user = UsersSearchQuery.new.find_by_email(params.require(:email))
      render json: user
    end
    
    def search
      users = UsersSearchQuery.new.search(params.require(:search_string))
      render json: users
    end
    
    def create
      use_case = CreateUser.new(attributes, editor_id)
      
      if use_case.call
        render json: UserSerializer.new(use_case.user).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def show
      user = User.find(params[:id])
      render json: user
    end
    
    def full
      user = FullUserSerializer.new(User.find(params[:id])).as_json
      render json: user
    end
    
    def update
      use_case = UpdateUser.new(params[:id], attributes, editor_id)
      
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def destroy
      DestroyUser.call(params[:id], editor_id)
      head :ok
    end
    
    def payment_conditions
      respond_with User::PAYMENT_CONDITIONS.map {|key, value| { slug: key, title: value } }
    end
  end
end
