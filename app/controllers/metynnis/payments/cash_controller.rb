module Metynnis
  module Payments
    class CashController < PaymentsController
      def create
        render(json: { status: 'error', errors: ['user not found'] }, status: :forbidden) and return unless user

        use_case = ::Payments::CreatePayment.new(payment_params.merge(paymethod: paymethod), user, request.ip)
        if use_case.call
          render json: { status: 'ok' }
        else
          render json: { status: 'error', errors: Array(use_case.errors) }, status: :unprocessable_entity
        end
      end

    private
      def paymethod
        @paymethod ||= Paymethod.find_by_slug('cash')
      end
    end
  end
end
