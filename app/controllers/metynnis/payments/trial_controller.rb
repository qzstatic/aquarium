module Metynnis
  module Payments
    class TrialController < PaymentsController
      def create
        render(json: { status: 'error', errors: ['user not found'] }, status: :forbidden) and return unless user

        use_case = ::Payments::CreateTrialPayment.new(payment_params, user, request.ip)
        if use_case.call
          pay_init_event
          render json: { status: 'ok' }
        else
          render json: { status: 'error', errors: Array(use_case.errors) }, status: :unprocessable_entity
        end
      end
    end
  end
end
