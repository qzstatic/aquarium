module Metynnis
  module Payments
    class CardController < PaymentsController
      def create
        render(json: { status: 'error', errors: ['user not found'] }, status: :forbidden) and return unless user

        use_case = ::Payments::CreatePayment.new(payment_params.merge(paymethod: paymethod, data: {auto_renewing: true}, document_url: return_url), user, request.ip)
        if use_case.call
          pay_init_event
          Resque.enqueue(Workers::Payments::CardGetTransactionId, use_case.payment.id)
          render json: { status: 'ok', wait: { id: use_case.payment.id } }
        else
          render json: { status: 'error', errors: Array(use_case.errors) }, status: :unprocessable_entity
        end
      end

      def check_transaction_id
        payment = Payment.find(params.require(:id))
        if !payment.notpaid?
          render json: { status: 'error', errors: [ 'Old payment' ] }, status: :unprocessable_entity
        elsif payment.transaction_id
          render json: { status: 'ok', post: Settings.card.client_host + Settings.card.client_url, attributes: {trans_id: payment.transaction_id} }
        else
          render json: { status: 'ok', wait: {id: payment.id} }
        end
      end

      def callback
        payment = Payment.notpaid.bymethod(:card).find_by_transaction_id(params.require(:trans_id))
        if payment
          Resque.enqueue(Workers::Payments::CardGetResult, payment.id, request.ip)
          render json: { status: 'ok', wait: { id: payment.id } }
        else
          render json: { status: 'error', errors: ['Payment not found'] }, status: :unprocessable_entity
        end
      end

      def check_result
        payment = Payment.find(params.require(:id))
        if payment.notpaid?
          render json: { status: 'ok', wait: {id: payment.id} }
        elsif payment.canceled?
          render json: { status: 'error', errors: [ 'Payment canceled' ] }, status: :unprocessable_entity
        elsif payment.paid?
          render json: { status: 'ok', redirect: {path: 'subscription_ok'} }
        end
      end

      private

      def paymethod
        @paymethod ||= Paymethod.find_by_slug('card')
      end

      def return_url
        params[:return_url]
      end
    end
  end
end
