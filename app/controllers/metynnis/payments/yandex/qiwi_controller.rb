module Metynnis
  module Payments
    module Yandex
      class QiwiController < YandexBaseController
        private
        def paymethod
          @paymethod ||= Paymethod.find_by_slug('qiwi')
        end
      end
    end
  end
end
