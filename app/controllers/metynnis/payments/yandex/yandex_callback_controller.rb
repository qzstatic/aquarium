module Metynnis
  module Payments
    module Yandex
      class YandexCallbackController < PaymentsController
        def cancel
          use_case = ::Payments::Yandex::CancelPayment.new(params[:attributes])
          use_case.call
          render xml: use_case.xml_response
        end
        def check
          use_case = ::Payments::Yandex::CheckPayment.new(params[:attributes])
          use_case.call
          render xml: use_case.xml_response
        end
        def payment
          use_case = ::Payments::Yandex::AvisoPayment.new(params[:attributes])
          use_case.call
          render xml: use_case.xml_response
        end
      end
    end
  end
end
