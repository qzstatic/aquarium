module Metynnis
  module Payments
    module Yandex
      class YandexBaseController < PaymentsController
        def create
          render(json: { status: 'error', errors: ['user not found'] }, status: :forbidden) and return unless user
          use_case = ::Payments::Yandex::CreatePayment.new(payment_params.merge(paymethod: paymethod), user, request.ip)
          if use_case.call
            render json: { attributes: use_case.form_attributes, url: use_case.form_url, result: paymethod.slug }
          else
            render json: { status: 'error', errors: Array(use_case.errors) }, status: :unprocessable_entity
          end
        end
      end
    end
  end
end
