module Metynnis
  module Payments
    module Yandex
      class YandexMoneyController < YandexBaseController
        private
        def paymethod
          @paymethod ||= Paymethod.find_by_slug('yandex_money')
        end
      end
    end
  end
end
