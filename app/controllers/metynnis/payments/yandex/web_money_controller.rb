module Metynnis
  module Payments
    module Yandex
      class WebMoneyController < YandexBaseController
        private
        def paymethod
          @paymethod ||= Paymethod.find_by_slug('web_money')
        end
      end
    end
  end
end
