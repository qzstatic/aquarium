class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::ImplicitRender
  
  respond_to :json
  
  rescue_from Roompen::NotFound, with: :not_found
  rescue_from Shark::UnknownToken, with: :unknown_token
  
private
  def attributes
    params.require(:attributes)
  end
  
  def not_found
    if Rails.env.production?
      raise ActionController::RoutingError.new('Not Found')
    else
      raise
    end
  end
  
  def unknown_token
    head :unauthorized
  end

  def meta_attributes(resource, extra_meta = {})
    {
        current_page: resource.current_page,
        next_page: resource.next_page,
        prev_page: resource.prev_page, # use resource.previous_page when using will_paginate
        total_pages: resource.total_pages,
        total_count: resource.total_count
    }.merge(extra_meta)
  end

  def check_redis
    render json: { error: 'Извините, сервис временно недоступен', status: 503 } unless redis_available?
  end

  def redis_available?
    Aquarium::Redis.connection.ping == 'PONG'
  end
end
