module Shark
  class CommentsController < ApplicationController
    def show_popular
      comment = PopularComment.find_by(widget_id: params[:widget_id], xid: params[:xid])
      if comment && comment.body.present?
        render json: comment.body.merge(updated_at: comment.updated_at)
      else
        head :not_found
      end
    end
    
    def update_popular
      if Resque.enqueue(Workers::Comments::UpdatePopular, params[:widget_id], params[:xid])
        head :accepted
      else
        head :unprocessable_entity
      end
    end
  end
end
