module Shark
  class ContactsController < ApplicationController
    def show
      contact = Contact.find(params[:id])
      
      if contact
        render json: contact
      else
        head :not_found
      end
    end
    
    def create
      use_case = Common::CreateContact.new(params.require(:contact), params.require(:type_slugs))
      
      if use_case.call
        render json: {
          contact:    ContactSerializer.new(use_case.contact),
          mail_types: use_case.mail_types.map {|t| Shark::Mailing::MailTypeSerializer.new(t) }
        }
      else
        render json: {
          errors:     use_case.errors,
          contact:    ContactSerializer.new(use_case.contact),
          mail_types: use_case.mail_types.map {|t| Shark::Mailing::MailTypeSerializer.new(t) }
        }, status: :unprocessable_entity
      end
    end

    def unsubscribe_info
      use_case = UnsubscribeConfirm.new(params.require(:unsubscribe_token))
      # don't use 'call' method here!
      if use_case.contact && use_case.mail_type
        render json: {
          contact:   ContactSerializer.new(use_case.contact),
          mail_type: Shark::Mailing::MailTypeSerializer.new(use_case.mail_type)
        }
      else
        head :not_found
      end
    end
    
    def subscribe_confirm
      use_case = SubscribeConfirm.new(params)
      
      if use_case.call
        render json: {
          contact:    ContactSerializer.new(use_case.contact),
          mail_types: use_case.mail_types.map { |t| Shark::Mailing::MailTypeSerializer.new(t) }
        }
      else
        head :not_found
      end
    end
    
    def unsubscribe_confirm
      use_case = UnsubscribeConfirm.new(params.require(:unsubscribe_token))
       
      if use_case.call
        render json: {
          contact:    ContactSerializer.new(use_case.contact),
          mail_type: Shark::Mailing::MailTypeSerializer.new(use_case.mail_type)
        }
      else
        head :not_found
      end
    end
  end
end
