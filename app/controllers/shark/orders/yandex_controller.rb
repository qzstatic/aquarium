module Shark
  module Orders
    class YandexController < OrdersController
      def create
        use_case = Shark::PaymentsOld::Yandex::CreateOrder.new(order_params, user, request.ip)
        payment_answer(use_case)
      end
      
      def check
        use_case = Shark::PaymentsOld::Yandex::Check.new(params, request.ip)
        payment_answer(use_case)
      end
      
      def aviso
        use_case = Shark::PaymentsOld::Yandex::Aviso.new(params, request.ip)
        payment_answer(use_case)
      end
    end
  end
end
