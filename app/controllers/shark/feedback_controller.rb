module Shark
  class FeedbackController < ApplicationController
    def create
      use_case = CreateFeedback.new(attributes)
      
      if use_case.call
        render json: use_case.feedback
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
