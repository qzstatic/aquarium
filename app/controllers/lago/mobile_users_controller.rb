module Lago
  class MobileUsersController < Lago::ApplicationController

    before_action :authenticate_mobile_user!

    def show
      render json: Lago::MobileUserSerializer.new(mobile_user)
    end

    def subscribe
      use_case = Lago::UpdateMobileUser.new(mobile_user, email: email, mailing_types: mailing_types)

      if use_case.call
        render json: { status: :ok, redirect: use_case.redirect }
      else
        render json: { status: :error, errors: use_case.errors }
      end
    end

    private

    def mailing_types
      attributes.require(:mailing_types)
    end

    def email
      attributes.require(:email)
    end

    def msisdn
      params.require(:msisdn)
    end
  end
end
