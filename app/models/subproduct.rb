# create_table :subproducts, force: true do |t|
#   t.integer  :product_id
#   t.string   :slug,                   null: false
#   t.string   :title,                  null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.text     :phone_text
#   t.integer  :position,   default: 0, null: false
# end
#
# add_index :subproducts, [:product_id], name: :index_subproducts_on_product_id, using: :btree

class Subproduct < ActiveRecord::Base
  belongs_to :product
  has_many :payment_kits
  has_many :snapshots, as: :subject

  validates :slug, :title, :product_id, :position, presence: true
  validates :slug, uniqueness: { scope: :product_id }

  def slx_slug
    slug == 'paper' ? 'offline' : slug
  end
end
