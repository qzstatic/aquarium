# create_table :periods, force: true do |t|
#   t.string   :slug,       null: false
#   t.string   :title,      null: false
#   t.interval :duration,   null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class Period < ActiveRecord::Base
  has_many :payment_kits
  has_many :snapshots, as: :subject

  validates :slug, :title, :duration, presence: true
  validates :slug, uniqueness: true

  def measure_unit
    duration.parts.first.first
  end

  def duration_number
    duration.parts.first.second
  end
end
