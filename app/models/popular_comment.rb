# create_table :popular_comments, force: true do |t|
#   t.integer  :widget_id,                         null: false
#   t.integer  :xid,        limit: 8,              null: false
#   t.json     :body,                 default: {}, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :popular_comments, [:widget_id, :xid], name: :index_popular_comments_on_widget_id_and_xid, unique: true, using: :btree

class PopularComment < ActiveRecord::Base
  validates_uniqueness_of :widget_id, scope: :xid
end
