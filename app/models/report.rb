# create_table :reports, force: true do |t|
#   t.datetime :start_date,                 null: false
#   t.datetime :end_date,                   null: false
#   t.boolean  :ready,      default: false, null: false
#   t.string   :url
#   t.string   :kind,                       null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class Report < ActiveRecord::Base
  has_many :snapshots, as: :subject
  
  paginates_per 50
end
