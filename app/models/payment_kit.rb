# create_table :payment_kits, force: true do |t|
#   t.integer  :product_id,                                               null: false
#   t.integer  :namespace_id,                                             null: false
#   t.integer  :subproduct_id,                                            null: false
#   t.integer  :period_id,                                                null: false
#   t.ltree    :slug_path,                                                null: false
#   t.decimal  :price,           precision: 10, scale: 2
#   t.decimal  :crossed_price,   precision: 10, scale: 2
#   t.integer  :upsell_id
#   t.integer  :renewal_id
#   t.integer  :alternate_ids,                            default: [],    null: false, array: true
#   t.integer  :paymethod_ids,                            default: [],    null: false, array: true
#   t.interval :autopay_timeout
#   t.boolean  :syncable,                                 default: false, null: false
#   t.hstore   :texts,                                    default: {},    null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.hstore   :user_conditions,                          default: {},    null: false
#   t.boolean  :city_choice,                              default: false, null: false
#   t.boolean  :disabled,                                 default: false, null: false
#   t.integer  :discount
# end
#
# add_index :payment_kits, [:disabled], name: :index_payment_kits_on_disabled, using: :btree
# add_index :payment_kits, [:namespace_id], name: :index_payment_kits_on_namespace_id, using: :btree
# add_index :payment_kits, [:period_id], name: :index_payment_kits_on_period_id, using: :btree
# add_index :payment_kits, [:product_id], name: :index_payment_kits_on_product_id, using: :btree
# add_index :payment_kits, [:renewal_id], name: :index_payment_kits_on_renewal_id, using: :btree
# add_index :payment_kits, [:slug_path], name: :index_payment_kits_on_slug_path, unique: true, using: :btree
# add_index :payment_kits, [:subproduct_id], name: :index_payment_kits_on_subproduct_id, using: :btree
# add_index :payment_kits, [:upsell_id], name: :index_payment_kits_on_upsell_id, using: :btree

class PaymentKit < ActiveRecord::Base
  belongs_to :product
  belongs_to :namespace
  belongs_to :period
  belongs_to :subproduct
  belongs_to :renewal, class_name: "PaymentKit"
  belongs_to :upsell, -> { active }, class_name: "PaymentKit"
  has_many :snapshots, as: :subject
  has_many :payments, dependent: :restrict_with_exception
  has_many :payments_renewal, class_name: 'Payment', foreign_key: :renewal_id, dependent: :restrict_with_exception

  before_destroy :check_associations

  before_validation do
    self.slug_path = generate_slug_path
  end

  validates :product_id, :namespace_id, :subproduct_id, :period_id, :slug_path, presence: true
  validates :slug_path, uniqueness: true

  scope :active, -> do
    joins(:namespace).where(
      "disabled = :disabled AND ((namespaces.end_date IS NULL AND namespaces.start_date < :now) OR (:now BETWEEN namespaces.start_date AND namespaces.end_date))",
      disabled: false, now: Time.now
    )
  end
  
  scope :active_by_product_namespace, ->(product_slug, namespace_slug) do
    active.where("payment_kits.slug_path ~ '#{product_slug}.#{namespace_slug}.*.*'")
  end
    
  def alternates
    PaymentKit.where(id: alternate_ids)
  end
  
  def paymethods
    Paymethod.where(id: paymethod_ids).order("CASE WHEN paymethods.slug='card' THEN 0 ELSE 1 END")
  end
  
  def suitable_for?(user = User.new)
    return false unless user
    user_conditions.map do |condition_name, condition_value|
      condition_bool = TypeConversion.to_bool(condition_value)
      if condition_value.present? && User.instance_methods.include?("is_#{condition_name}?".to_sym)
        !(TypeConversion.to_bool(condition_value) ^ user.send("is_#{condition_name}?".to_sym, self))
      else
        true
      end
    end.all?
  end
  
  def generate_slug_path
    [product.slug, namespace.slug, subproduct.slug, period.slug].join('.')
  end

  def associations
    self.class.where("upsell_id = :id OR renewal_id = :id OR :id = any(alternate_ids)", id: id)
  end

private
  def check_associations
    associations.empty?
  end
end
