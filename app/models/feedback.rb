# create_table :feedbacks, force: true do |t|
#   t.string   :access_token,                 null: false
#   t.integer  :user_id
#   t.string   :username,                     null: false
#   t.string   :company
#   t.string   :email,                        null: false
#   t.string   :phone
#   t.text     :message
#   t.integer  :price_id
#   t.boolean  :callback,     default: false, null: false
#   t.integer  :status,       default: 0,     null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :type,         default: 0,     null: false
#   t.json     :user_data,    default: {},    null: false
# end
#
# add_index :feedbacks, [:status], name: :index_feedbacks_on_status, using: :btree
# add_index :feedbacks, [:user_id], name: :index_feedbacks_on_user_id, using: :btree

class Feedback < ActiveRecord::Base
  self.inheritance_column = :sti_disabled
  
  enum type: {
    feedback:             0,
    subscription_request: 1,
    courier_payment:      2
  }
  
  enum status: {
    created:  0,
    sent:     1
  }
  
  belongs_to :user
  belongs_to :price
  
  validates :type, :access_token, :username, :email, presence: true
  validates :email, email: true
  
  after_create :enqueue
  after_create :send_user_mail
  
  def type_label
    I18n.t("feedback_type.#{type}")
  end
  
  def mail_subject
    "#{type}##{id} #{type_label}"
  end
  
  def enqueue
    Resque.enqueue(Workers::FeedbackSendJob, id)
  end
  
  def send_user_mail
    # TODO: тут лучше тоже добавить письмо в очередь
    if courier_payment?
      OrderMailer.order_courier_email(user).deliver
    end
  end
end
