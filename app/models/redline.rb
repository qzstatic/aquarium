# create_table :redlines, force: true do |t|
#   t.string   :label
#   t.string   :title,                  null: false
#   t.string   :url
#   t.integer  :status,     default: 0, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class Redline < ActiveRecord::Base
  enum status: {
    off: 0,
    on:  1
  }
  
  validates :title,     presence: true
  
  paginates_per 10
  
  class << self
    def active
      on.last
    end
  end
end
