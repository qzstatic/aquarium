class Document < Roompen::Document
  def domain
    categories.domains
  end
  
  def has_rubrics?
    categories.has_rubrics?
  end
  
  def rubrics
    has_rubrics? && categories.rubrics
  end
  
  def has_rubric?
    has_rubrics? && !rubrics.empty?
  end

  def rubric
    rubrics.first
  end
    
  def has_subrubric?
    has_rubric? && rubric.has_subrubrics?
  end
  
  def subrubric
    rubric.subrubrics
  end

  def has_story?
    categories.has_story?
  end

  def story
    has_story? && categories.story
  end

  def has_section?
    has_rubric? && has_subrubric? && subrubric.has_sections?
  end

  def section
    has_section? && subrubric.sections
  end

  def has_theme?
    categories.has_themes?
  end

  def theme
    has_theme? && categories.themes
  end

  def has_theme_cycle?
    categories.has_themes? && categories.themes.has_cycle_list?
  end

  def theme_cycle
    has_theme_cycle? && categories.themes.cycle_list
  end
  
  def part
    categories.parts
  end
  
  def gallery?
    categories.has_parts? && part.slug == 'galleries'
  end

  def gallery
    boxes.find { |box| box.type == 'gallery' }
  end

  def online?
    categories.has_parts? && part.slug == 'online'
  end

  def video?
    categories.has_parts? && part.slug == 'video'
  end

  def video
    boxes.find { |box| box.type == 'dulton_media' || box.type == 'inset_media'  }
  end
  
  def opinion?
    has_rubric? && rubric.slug == 'opinion'
  end
  
  def editorial?
    opinion? && has_subrubric? && subrubric.slug == 'editorial'
  end
  
  def authors
    has_authors? ? links[:authors] : []
  end
  
  def has_authors?
    has_links? && links.key?(:authors)
  end

  def has_newsrelease?
    links.has_key?(:newsrelease) && links[:newsrelease].present?
  end

  def newsrelease
    if has_newsrelease?
      @newsrelease ||= links[:newsrelease].first.bound_document
    end
    @newsrelease
  end

  def has_company?
    links.has_key?(:company) && links[:company].present?
  end

  def company
    if has_company?
      @company ||= links[:company].first.bound_document
    end
    @company
  end

  def last_published_at
    Time.at(super)
  end
  
  def title_equals_newspaper_title?
    newspaper_title.present? && title == newspaper_title
  end

  def pay_requirement_expired?
    has_pay_required_expire? && !pay_required_expire.blank? && (Time.parse(pay_required_expire) < Time.now)
  end

  def pay_required?
    @pay_required ||= has_pay_required? && pay_required && !pay_requirement_expired?
  end
end
