# create_table :users, force: true do |t|
#   t.string   :first_name
#   t.string   :last_name
#   t.string   :nickname,                              null: false
#   t.string   :email,                                 null: false
#   t.string   :phone
#   t.integer  :status,                default: 0,     null: false
#   t.json     :avatar,                default: {},    null: false
#   t.json     :address,               default: {},    null: false
#   t.string   :company
#   t.date     :birthday
#   t.integer  :company_speciality_id
#   t.integer  :appointment_id
#   t.integer  :income_id
#   t.integer  :auto_id
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.boolean  :autopay,               default: false, null: false
#   t.string   :subscribe_base_id
#   t.hstore   :accepted_promo,        default: {},    null: false
#   t.json     :data,                  default: {},    null: false
# end
#
# add_index :users, [:email], name: :index_users_on_email, unique: true, using: :btree
# add_index :users, [:first_name, :last_name, :nickname, :company], name: :index_users_on_credentials, using: :btree
# add_index :users, [:status], name: :index_users_on_status, using: :btree

require 'new_relic/agent/method_tracer'

class User < ActiveRecord::Base

  include ::NewRelic::Agent::MethodTracer

  validates :email, :nickname, presence: true
  validates :email, uniqueness: true, email: true

  belongs_to :company_speciality, -> { company_speciality }, class_name: 'Option'
  belongs_to :appointment,        -> { appointment }, class_name: 'Option'
  belongs_to :income,             -> { income }, class_name: 'Option'
  belongs_to :auto,               -> { auto }, class_name: 'Option'

  has_many :sessions, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :access_rights, as: :reader
  has_many :mobile_users, through: :sessions
  has_many :tokens, class_name: 'UserToken', dependent: :destroy
  has_many :orders, through: :access_rights
  has_many :payments, through: :access_rights
  has_many :feedbacks
  has_many :snapshots, as: :subject

  has_one :contact

  before_validation do |user|
    if user.nickname.blank?
      user.nickname = user.email.split('@').first if user.email.present?
    end
  end

  before_create do
    self.data[:guid] = SecureRandom.uuid
    self.data_will_change!
  end

  enum status: {
      registered: 0,
      confirmed:  1,
      blocked:    2,
      deleted:    4
  }

  PAYMENT_CONDITIONS = {
      # registered:                  'Зарегистрирован',
      # subscribed:                  'Имеет активные подписки',
      online_subscriber_now:       'Имеет подписку онлайн/профи',
      online_subscriber_last_year: 'Имеет онлайн/профи подписку за последний год',
      subscriber_of_namespace:     'Имеет подписку по этой акции'
  }.freeze

  scope :unconfirmed, -> { registered.where('created_at < ?', 12.hours.ago) }

  paginates_per 50

  def full_name
    [first_name, last_name].compact.join(' ')
  end

  def email=(new_email)
    if new_email.respond_to?(:downcase)
      super(new_email.downcase)
    else
      super
    end
  end

  def address
    Address.new(super)
  end

  def avatar
    Avatar.new(super)
  end

  def allowed_to_sign_in?
    %w(registered confirmed banned).include?(status)
  end

  def legal_entities
    LegalEntity.where('user_ids @> ARRAY[?]', id)
  end

  def access_rights_from_legal_entities
    join_sql = <<-SQL
      INNER JOIN legal_entities
        ON access_rights.reader_id = legal_entities.id
        AND access_rights.reader_type = 'LegalEntity'
    SQL

    AccessRight.joins(join_sql).where('legal_entities.user_ids @> ARRAY[?]', id)
  end

  def access_rights_from_sessions
    join_sql = <<-SQL
      INNER JOIN sessions
        ON access_rights.reader_id = sessions.id
        AND access_rights.reader_type = 'Session'
    SQL

    AccessRight.joins(join_sql).where('sessions.id IN (?)', sessions.pluck(:id))
  end
  
  def has_active_access_rights?
    access_rights.active.any? || access_rights_from_legal_entities.active.any? || access_rights_from_sessions.active.any?
  end

  def confirm_subscriber
    confirmed! if has_active_access_rights?
  end

  alias_method :confirm_subscribers, :confirm_subscriber

  def payment
    payments.paid.order(end_date: :desc).first
  end

  def last_access_right
    access_rights.where.not(end_date: nil).order(end_date: :desc).first
  end

  def last_unexpired_access_right
    access_rights.unexpired.order(end_date: :desc).first
  end

  def last_paper_order
    orders.paid.joins(:price).where(prices: {product: Price.products.values_at(:paper, :profi)}).order(end_date: :desc).first
  end

  def last_paper_payment
    payments.paid.joins(:product, :subproduct).where(products: {slug: 'subscription'}, subproducts: {slug: %w(paper profi)}).order(end_date: :desc).first
  end

  def dump_sessions_to_redis
    sessions.each(&:dump_to_redis)
  end

  def last_paid_order(*except_ids)
    orders.paid.where.not(id: except_ids).order(created_at: :desc).first
  end

  def send_confirmation_email(token_data = {})
    token = tokens.confirmation.generate(token_data)
    Resque.enqueue(Workers::UserMailer::ConfirmationEmailJob, token.id)
  end

  def destroy
    update(status: :deleted, email: destroyed_email)
  end

  # payments conditions

  #  зарегистрирован или подтверждён
  def is_registered?(payment_kit = nil)
    !id.nil? && (registered? || confirmed?)
  end

  # имеет активные подписки
  def is_subscribed?(payment_kit = nil)
    access_rights.active.any?
  end

  # имеет активную подписку онлайн или профи
  def is_online_subscriber_now?(payment_kit = nil)
    access_rights.active.any?
  end

  # была ли онлайн или профи подписка за последний год
  # срок окончания последней открытой подписки на онлайн или профи старше 1 года
  def is_online_subscriber_last_year?(payment_kit = nil)
    access_rights.where('end_date > ?', 1.year.ago).any?
  end

  # имеет подписку на данный продукт по данной акции (subproduct)
  def is_subscriber_of_namespace?(payment_kit = nil)
    payment_kit.present? && payments.active.joins(:payment_kit).where(payment_kits: {product_id: payment_kit.product_id, namespace_id: payment_kit.namespace_id, subproduct_id: payment_kit.subproduct_id}).any?
  end

  add_method_tracer :send_confirmation_email, 'Custom/User_send_confirmation_email'
  private
  def destroyed_email
    salt = Digest::MD5.hexdigest("#{id}:#{Time.now.to_f}")
    "deleted+#{salt}+#{email}"
  end

  class << self
    def find_by_access_token(token = nil)
      session = Session.find_by(access_token: token)
      session.user if session
    end
  end
end
