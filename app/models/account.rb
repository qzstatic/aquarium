# create_table :accounts, force: true do |t|
#   t.integer  :user_id,         null: false
#   t.string   :type,            null: false
#   t.string   :password_digest
#   t.string   :external_id
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :accounts, [:user_id], name: :index_accounts_on_user_id, using: :btree

class Account < ActiveRecord::Base
  TYPES = %w(password twitter facebook vkontakte).freeze
  
  belongs_to :user
  has_many :sessions, dependent: :destroy
  
  validates :user, :type, presence: true
  validates :type, inclusion: { in: TYPES }
  
  has_secure_password validations: false
  validates :password, presence: true, on: :create, if: :password?
  
  self.inheritance_column = :sti_disabled
  
  TYPES.each do |type|
    scope type, -> { where(type: type) }
    
    define_method("#{type}?") do
      self.type == type
    end
  end
  
  def localized_type
    I18n.t("account_type.#{type}")
  end
end
