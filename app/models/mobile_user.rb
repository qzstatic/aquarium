# create_table :mobile_users, force: true do |t|
#   t.integer  :status
#   t.string   :first_name
#   t.string   :last_name
#   t.integer  :msisdn,     limit: 8, null: false
#   t.string   :operator,             null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :mobile_users, [:msisdn], name: :index_mobile_users_on_msisdn, unique: true, using: :btree

class MobileUser < ActiveRecord::Base
  has_many :access_rights, as: :reader, dependent: :destroy
  has_many :sessions, dependent: :destroy
  has_one :contact

  has_many :payments, through: :access_rights
  has_many :snapshots, as: :subject

  validates :msisdn, presence: true, allow_nil: false

  paginates_per 50

  def email
    contact && contact.email
  end

  def payment
    payments.paid.last
  end

  def active_payment
    payments.paid.active.last
  end

  def has_active_access_rights?
    access_rights.active.any?
  end

  def dump_sessions_to_redis
    sessions.map(&:dump_to_redis)
  end

  def confirm_subscribers
    true
  end

  def redis_payment
    base_use_case = ::Payments::Mobile::Base.new
    base_use_case.redis_payment(payment.subscription_id) if payment && payment.subscription_id
  end

  def return_url
    (!redis_payment.empty?  && redis_payment.key?('return_url')) ? redis_payment['return_url'] : Settings.hosts.shark
  end
end
