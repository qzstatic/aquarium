# create_table :legal_entities, force: true do |t|
#   t.string   :name,                    null: false
#   t.inet     :ips,        default: [], null: false, array: true
#   t.integer  :user_ids,   default: [], null: false, array: true
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class LegalEntity < ActiveRecord::Base
  validates :name, presence: true
  
  has_many :access_rights, as: :reader
  has_many :snapshots, as: :subject
  has_many :payments, through: :access_rights
  
  def users
    User.where(id: user_ids)
  end
  
  def subscriber?
    access_rights.active.any?
  end
  
  def raw_ips
    if ips_before_type_cast.is_a? Array
      ips
    else
      ips_before_type_cast.gsub(/[{}]/, '').split(',')
    end
  end
  
  def dump_sessions_to_redis
    users.each(&:dump_sessions_to_redis)
    self.class.add_ips_to_redis(raw_ips) if subscriber?
  end
  
  def confirm_subscribers
    users.each do |user|
      user.confirm_subscriber
    end
  end
  
  class << self
    def add_ips_to_redis(new_ips)
      ips, masks = differentiate_ips(new_ips)
      
      Aquarium::Redis.connection.sadd('ips', ips) unless ips.nil?
      Aquarium::Redis.connection.sadd('ip_masks', masks) unless masks.nil?
    end
    
    def remove_ips_from_redis(old_ips)
      ips, masks = differentiate_ips(old_ips)
      
      Aquarium::Redis.connection.srem('ips', ips) unless ips.nil?
      Aquarium::Redis.connection.srem('ip_masks', masks) unless masks.nil?
    end
    
  private
    def differentiate_ips(ips)
      ips.map(&IP.method(:new)).group_by { |ip| ip.mask.zero? }.values_at(true, false)
    end
  end
end
