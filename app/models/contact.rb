# create_table :contacts, force: true do |t|
#   t.string   :email,                            null: false
#   t.integer  :user_id
#   t.integer  :contact_status,      default: 0,  null: false
#   t.integer  :subscription_status, default: 0,  null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :type_ids,            default: [], null: false, array: true
#   t.integer  :mobile_user_id
# end
#
# add_index :contacts, [:contact_status], name: :index_contacts_on_contact_status, using: :btree
# add_index :contacts, [:email], name: :index_contacts_on_email, unique: true, using: :btree
# add_index :contacts, [:mobile_user_id], name: :index_contacts_on_mobile_user_id, using: :btree
# add_index :contacts, [:subscription_status], name: :index_contacts_on_subscription_status, using: :btree
# add_index :contacts, [:type_ids], name: :index_contacts_on_type_ids, using: :gin
# add_index :contacts, [:user_id], name: :index_contacts_on_user_id, using: :btree

class Contact < ActiveRecord::Base
  enum contact_status: {
    registered:  0,
    confirmed:   1,
    deleted:     2
  }
  
  enum subscription_status: {
    off: 0,
    on:  1
  }
  
  belongs_to  :user
  belongs_to  :mobile_user
  validates   :email, email: true, presence: true, uniqueness: true
  before_save :match_with_user

  has_many    :mails, class_name: 'Mailing::Letter'
  has_many    :mailings, through: :mails
  has_many    :snapshots, as: :subject

  has_many    :access_rights, through: :user

  paginates_per 50

  def email=(new_email)
    if new_email.respond_to?(:downcase)
      super(new_email.downcase)
    else
      super
    end
  end
  
  def match_with_user
    self.user = User.find_by(email: email)
  end

  def secret_token(type_slug)
    Digest::SHA1.hexdigest([id, email, type_slug, Settings.contacts.salt].join)
  end

  def unsubscribe_hash(type_slug)
    {
      email:     email,
      type_slug: type_slug,
      secret:    secret_token(type_slug)
    }
  end
  
  def unsubscribe_token(type_slug)
    Base64.urlsafe_encode64(Oj.dump(unsubscribe_hash(type_slug)))
  end
  
  def mail_types
    @mail_types ||= if type_ids.blank?
      Mailing::MailType.none
    else
      Mailing::MailType.where(id: type_ids)
    end
  end
  
  class << self
    def find_by_unsubscribe_token(unsubscribe_token)
      hash = secret_hash(unsubscribe_token)
      find_by_email_type_secret(*hash.values_at(*%i(email type_slug secret)))
    end

    def find_by_email_type_secret(email, type_slug, secret)
      contact = find_by(email: email)
      contact if contact.secret_token(type_slug) == secret
    end

    def secret_hash(unsubscribe_token)
      Oj.load(Base64.urlsafe_decode64(unsubscribe_token))
    end
    
    def by_confirmed_and_type_id(type_id)
      confirmed.where('type_ids @> ARRAY[?]', type_id)
    end
  end
end
