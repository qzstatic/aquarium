# create_table :mailing_rules, force: true do |t|
#   t.string   :code
#   t.integer  :status,     default: 0, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :mailing_rules, [:code], name: :index_mailing_rules_on_code, unique: true, using: :btree

class Mailing::Rule < ActiveRecord::Base

  enum status: {
           allow: 0,
           block: 1
       }
end
