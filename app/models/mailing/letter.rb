# create_table :mailing_letters, force: true do |t|
#   t.integer  :contact_id,              null: false
#   t.integer  :mailing_id,              null: false
#   t.integer  :status,     default: 0,  null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.text     :error,      default: ""
#   t.text     :mail,       default: "", null: false
#   t.json     :data,       default: {}, null: false
# end
#
# add_index :mailing_letters, [:contact_id], name: :index_mailing_letters_on_contact_id, using: :btree
# add_index :mailing_letters, [:mailing_id], name: :index_mailing_letters_on_mailing_id, using: :btree

class Mailing::Letter < ActiveRecord::Base
  self.table_name = :mailing_letters
  belongs_to :contact
  belongs_to :mailing
  has_one    :type, through: :mailing
  
  enum status: {
      unknown:      0,
      undelivered:  1,
      error:        2
  }
end
