# create_table :cities, force: true do |t|
#   t.string   :title,      null: false
#   t.integer  :capability, null: false
#   t.integer  :position,   null: false
#   t.text     :contact
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class City < ActiveRecord::Base
  has_many :snapshots, as: :subject

  validates :title, :position, :capability, presence: true

  enum capability: {
    main:      0, # Оплату газеты можно делать вместе с онлайн
    affiliate: 1, # Оплата газеты нужно делать другим способом
    other:     2  # Газету доставить не можем
  }
end
