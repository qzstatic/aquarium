module Ibis
  class FullUserSerializer
    attr_reader :user
    
    def initialize(user)
      @user = user
    end
    
    def as_json
      {
        user: user,
        accounts: user.accounts.select(:id, :type, :external_id, :created_at),
        acceess_rights: user.access_rights.order(:id).map{ |ar| Ibis::AccessRightSerializer.new(ar).as_json }
      }
    end
  end
end
