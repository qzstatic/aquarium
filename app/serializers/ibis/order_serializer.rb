module Ibis
  class OrderSerializer < ActiveModel::Serializer
    attributes :id, :start_date, :end_date, :created_at, :updated_at, :payment_method
    attributes :payment_status, :synced, :autopayment, :account, :subscribe_base_id
    attributes :manager_status, :price_id

    has_one :price
    has_many :users
    
    def autopayment
      data[:autopayment]
    end

    def data
      object.data.symbolize_keys
    end
  end
end
