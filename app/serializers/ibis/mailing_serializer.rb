module Ibis
  class MailingSerializer < ActiveModel::Serializer
    attributes :id, :type_id, :subject, :template, :created_at, :status, :scheduled_at
    attributes :status, :contacts_status, :template_kit, :preheader

    def template
      Pigeon::MailingTemplateRender.new(object).template_preview
    end

    def template_kit
      object.data['template_kit']
    end

    def preheader
      object.data['preheader']
    end
  end
end
