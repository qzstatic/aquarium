module Ibis
  class FullOrderSerializer < OrderSerializer
    attributes :users, :price, :manager_comment

    def users
      object.users.map do |user|
        UserSerializer.new(user)
      end
    end

    def price
      PriceSerializer.new(object.price)
    end
  end
end

