module Ibis
  class SubproductSimpleSerializer < ActiveModel::Serializer
    attributes :id, :product_id, :slug, :title, :phone_text, :position
  end
end
