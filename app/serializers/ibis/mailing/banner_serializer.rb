module Ibis
  class Mailing::BannerSerializer < ActiveModel::Serializer
    attributes :id, :title, :body, :url, :type, :embed, :picture, :type_ids, :start_date, :end_date, :position
  end
end