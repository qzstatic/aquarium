module Ibis
  class Mailing::MailTypeSerializer < ActiveModel::Serializer
    attributes :id, :slug, :title
  end
end