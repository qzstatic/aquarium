module Ibis
  class Mailing::LetterSerializer < ActiveModel::Serializer
    attributes :id, :mailing, :updated_at, :created_at, :type, :data, :status
  end
end