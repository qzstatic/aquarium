module Ibis
  class PaymentSearchSerializer < PaymentListSerializer
    attributes :search_text

    def search_text
      data = PaymentSimpleSerializer.new(object).as_json
      deep_values(data).join(' ')
    end

  private
    def deep_values(h)
      case h
      when Array
        h.map { |v| deep_values(v) }.flatten.compact
      when Hash
        deep_values(h.values)
      when ActiveModel::Serializer
        deep_values(h.as_json)
      when TrueClass, FalseClass
        nil
      else
        h
      end
    end
  end
end
