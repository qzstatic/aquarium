module Ibis
  class PaymentSimpleSerializer < ActiveModel::Serializer
    attributes :id, :status, :price, :start_date, :end_date, :synced, :created_at
    attributes :autopayment, :account, :subscribe_base_id
    attributes :payment_kit, :payment_kit_id, :paymethod, :user, :status_title, :mobile_user
    attributes :manager_status

    def price
      object.price.to_f
    end

    def autopayment
      data[:autopayment]
    end

    def data
      object.data.symbolize_keys
    end

    def payment_kit
      PaymentKitSimpleSerializer.new(object.payment_kit)
    end

    def paymethod
      PaymethodSerializer.new(object.paymethod)
    end

    def user
      UserSerializer.new(object.user)
    end

    def mobile_user
      MobileUserSerializer.new(object.mobile_user)
    end

    def status_title
      object.status_title
    end
  end
end
