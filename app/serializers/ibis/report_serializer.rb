module Ibis
  class ReportSerializer < ActiveModel::Serializer
    attributes :kind, :start_date, :end_date, :url, :ready, :created_at
  end
end
