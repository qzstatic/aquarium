module Ibis
  class PaymentListSerializer < ActiveModel::Serializer
    attributes :id, :created_at, :updated_at, :user_name, :paymethod_title, :status_title
    attributes :product_title, :namespace_title, :subproduct_title, :period_title
    attributes :price, :start_date, :end_date, :synced, :autopayment, :manager_status

    def user_name
      if object.user
        [ object.user.first_name, object.user.last_name ].join(' ')
      elsif object.mobile_user
        [object.mobile_user.msisdn, object.mobile_user.email].join(' ')
      end
    end

    def paymethod_title
      object.paymethod.title
    end

    def price
      object.price.to_f
    end

    def product_title
      object.product.title
    end

    def namespace_title
      object.namespace.title
    end

    def subproduct_title
      object.subproduct.title
    end

    def period_title
      object.period.title
    end

    def autopayment
      object.data.symbolize_keys[:autopayment]
    end
  end
end
