module Ibis
  class NamespaceSerializer < NamespaceSimpleSerializer
    attributes :redirect_to

    def redirect_to
      NamespaceSimpleSerializer.new(object.redirect_to)
    end
  end
end
