module Ibis
  class ImportNewsSerializer
    attr_reader :import_news

    def initialize(import_news)
      @import_news = import_news
    end
    
    def as_json(option = {})
      import_news.slice(*%i(id title url news_date body data updated_at)).merge(import_source: import_source)
    end
    
    def import_source
      import_news.import_source.slice(*%i(title url))
    end
  end
end
