module Ibis
  class SynonymsSerializer < ActiveModel::Serializer
    attributes :synonyms

    def synonyms
      {
          nominative:    synonymize(object.to(:nominative)),
          genitive:      synonymize(object.to(:genitive)),
          dative:        synonymize(object.to(:dative)),
          accusative:    synonymize(object.to(:accusative)),
          instrumental:  synonymize(object.to(:instrumental)),
          prepositional: synonymize(object.to(:prepositional))
      }
    rescue Exception
      {}
    end

    private

    def synonymize(petrovich)
      "#{petrovich.firstname} #{petrovich.lastname}"
    end
  end
end
