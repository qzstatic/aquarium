module Shark
  class PaymentSerializer < ActiveModel::Serializer
    attributes :id, :status, :price, :start_date, :end_date, :created_at
    attributes :autopayment, :paymethod, :subproduct, :period

    def price
      object.price.to_f
    end

    def autopayment
      object.data[:autopayment]
    end

    def paymethod
      PaymethodSerializer.new(object.paymethod)
    end
    
    def subproduct
      object.subproduct
    end
    
    def period
      object.period
    end
  end
end
