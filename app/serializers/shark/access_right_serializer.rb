module Shark
  class AccessRightSerializer < ActiveModel::Serializer
    attributes :start_date, :end_date, :order, :paymethod
    attributes :payment_id, :subproduct_title, :subproduct_period, :price

    def order
      OrderSerializer.new(object.order)
    end

    def paymethod
      if object.payment.present?
        object.payment.paymethod.as_json(only: %i(title slug))
      else
        nil
      end
    end
    
    def payment_id
      object.payment.id if object.payment.present?
    end
    
    def subproduct_title
      object.payment.subproduct.title if object.payment.present?
    end
    
    def subproduct_period
      object.payment.period.title if object.payment.present?
    end
    
    def price
      object.payment.price.to_i if object.payment.present?
    end
  end
end
