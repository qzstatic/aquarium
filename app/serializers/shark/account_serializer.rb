module Shark
  class AccountSerializer < ActiveModel::Serializer
    attributes :id, :user_id, :type, :account_name, :external_id, :created_at
    
    def account_name
      I18n.t("account_type.#{object.type}")
    end
  end
end
