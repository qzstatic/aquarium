module Shark
  class OrderSerializer < ActiveModel::Serializer
    attributes :id, :payment_method, :payment_status, :autopayment

    has_one :price
    
    def autopayment
      object.data.symbolize_keys[:autopayment]
    end
  end
end
