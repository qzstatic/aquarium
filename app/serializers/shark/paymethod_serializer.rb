module Shark
  class PaymethodSerializer < ActiveModel::Serializer
    attributes :slug, :title
  end
end
