module Shark
  class SessionSerializer
    attr_reader :session
    
    def initialize(session)
      @session = session
    end
    
    def as_json
      session.slice(:access_token).merge(ip: ip)
    end
    
    def ip
      session.ip.to_s
    end
  end
end
