module Shark
  class MailTypeSerializer < ActiveModel::Serializer
    attributes :id, :title, :slug
  end
end
