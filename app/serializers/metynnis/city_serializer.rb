module Metynnis
  class CitySerializer < ActiveModel::Serializer
    attributes :title, :capability, :contact
  end
end
