module Metynnis
  class PaymethodSerializer < ActiveModel::Serializer
    attributes :slug, :title, :description
  end
end
