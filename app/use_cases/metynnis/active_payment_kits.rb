module Metynnis
  class ActivePaymentKits
    include Procto.call

    attr_reader :product_slug, :namespace_slug, :user, :payment_kits

    def initialize(product_slug, namespace_slug)
      @product_slug = product_slug
      @namespace_slug = namespace_slug
    end
    
    def call
      @payment_kits = PaymentKit.active_by_product_namespace(@product_slug, @namespace_slug).joins(:subproduct, :period).order('subproducts.position, periods.duration')
      subproducts = []
      @payment_kits.each do |p|
        if subproducts.empty? || subproducts.last[:slug]!=p.subproduct.slug
          subproducts << p.subproduct.slice(*%i(slug title phone_text)).merge(payment_kits: [])
        end
        subproducts[subproducts.count - 1][:payment_kits] << PaymentKitSerializer.new(p)
      end
      subproducts
    end
  end
end
