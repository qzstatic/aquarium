module Metynnis
  class SuitablePaymentKits
    include Procto.call

    attr_reader :product_slug, :namespace_slug, :user
    
    def initialize(product_slug, namespace_slug, user)
      @product_slug   = product_slug
      @namespace_slug = namespace_slug
      @user           = user
    end
    
    def call
      payment_kits_unfiltered.select do |payment_kit|
        payment_kit.suitable_for?(user)
      end
    end

    def payment_kits_unfiltered
      @payment_kits_unfiltered ||= PaymentKit.active_by_product_namespace(product_slug, namespace_slug).joins(:subproduct, :period).order('subproducts.position, periods.duration')
    end
  end
end
