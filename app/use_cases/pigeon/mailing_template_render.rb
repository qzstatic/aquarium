module Pigeon
  class MailingTemplateRender
    include StatisticHelper
    
    attr_reader :mailing

    def initialize(mailing)
      @mailing = mailing
    end

    def template
      mailing.template || ''
    end

    def template_with_statistics
      # пиксель открытия письма
      text = template.sub('</body>', %Q|<img src="#{statistic_mailing_view}" alt="" width="1" height="1"></body>|)

      # редиректы через статистику
      text.gsub(/<a href=("|')(.+?)\1/i) do
        "<a href=\"#{statistic_mailing_click($2)}\""
      end
    end

    def template_with_banners
      template_with_statistics.gsub(/<!--\s*banner:(\d+)\s*-->/) do |element|
        banner_with_statistics($1.to_i)
      end
    end

    def banners
      @banners ||= Hash[ mailing.banners_active.map { |b| [b.position, b] } ]
    end

    def banner_with_statistics(position)
      if tmpl = banner_view(position)
        # редиректы для ссылок
        tmpl.gsub!(/<a href=("|').+?\1/i) do |m|
          url = m.match(/<a href=("|')(.+?)\1/i)[2] # пришлось применять такой идиотский метод, потому что в переменной $2 ничего нет в отличии от кода в строке 21
          "<a href=\"#{statistic_banner_click(url, banners[position])}\""
        end

        # пиксель просмотра
        tmpl + %Q|<img src="#{statistic_banner_view(banners[position])}" alt="" width="1" height="1">|
      end
    end

    def banner_view(position)
      if banner = banners[position]
        ActiveTemplate.new.render("mailing_generators/#{mailing.type.slug}/banner", banner: banner, mailing: mailing)
      end
    end

    def template_preview
      # with banners, without statistics
      template.gsub(/<!--\s*banner:(\d+)\s*-->/) do
        banner_view($1.to_i)
      end
    end
  end
end