module Ibis
  class CreateOption
    include Procto.call
    
    attr_reader :option
    
    def initialize(params)
      @option = Option.new do |option|
        option.type  = params[:type]
        option.value = params[:value]
      end
    end
    
    def call
      Option.save_type_to_redis(option.type) if option.save
    end
    
    def errors
      option.errors
    end
  end
end
