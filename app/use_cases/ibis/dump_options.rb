module Ibis
  class DumpOptions
    include Procto.call
    
    def call
      Option::TYPES.each do |type|
        Option.save_type_to_redis(type)
      end
    end
  end
end
