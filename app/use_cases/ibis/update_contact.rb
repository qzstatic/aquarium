module Ibis
  class UpdateContact
    attr_reader :contact, :editor_id
        
    def initialize(contact_id, attributes, editor_id)
      @contact = Contact.find(contact_id)
      old_type_ids = contact.type_ids
      contact.assign_attributes(attributes)
      contact.type_ids_will_change! if old_type_ids!=contact.type_ids
      @editor_id = editor_id
    end
    
    def call
      if contact.save
        create_snapshot
      end
    end
    
    def errors
      contact.errors
    end
    
  private
    def create_snapshot
      contact.snapshots.create!(
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.contact_updated',
        data:      contact.attributes
      )
    end
  end
end
