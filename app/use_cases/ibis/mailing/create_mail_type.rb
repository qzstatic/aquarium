module Ibis
  class Mailing::CreateMailType
    attr_reader :mail_type, :editor_id
    
    def initialize(params, editor_id)
      @editor_id = editor_id
      @mail_type = Mailing::Type.new(params)
    end
    
    def call
      if mail_type.save
        create_snapshot
      end
    end
    
    def errors
      mail_type.errors
    end
  
  private
    def create_snapshot
      Snapshot.create!(
        subject:   mail_type,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mailing_type_created',
        data:      mail_type.attributes
      )
    end
  end
end
