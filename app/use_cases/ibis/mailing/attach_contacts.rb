module Ibis
  class Mailing::AttachContacts
    attr_reader :mailing, :conditions

    def initialize(mailing_id, conditions)
      @mailing = ::Mailing.find(mailing_id)
      @conditions = conditions
    end

    def call
      # change status
      mailing.contacts_linking!
      make_snapshot('contacts_linking')

      contacts = ::Mailings::ContactsSelector.new(mailing.type.slug)
      conditions.each do |c|
        if c[:type] == 'status' && c[:statuses].present?
          contacts = contacts.by_status(c[:statuses].map(&:to_sym))
        elsif c[:type] == 'mailing' && c[:mail_type_ids].present?
          contacts = contacts.by_mailing(Array(c[:mail_type_ids]))
        elsif c[:type] == 'payment_namespace' && c[:namespace_ids]
          contacts = contacts.by_payment_namespace(Array(c[:namespace_ids]))
        elsif c[:type] == 'register_date' && c[:start_date].present? && c[:end_date].present?
          contacts = contacts.by_register_date(Time.zone.parse(c[:start_date]), Time.zone.parse(c[:end_date]))
        elsif c[:type] == 'subscription_end' && c[:start_date].present? && c[:end_date].present?
          contacts = contacts.by_subscription_end(Time.zone.parse(c[:start_date]), Time.zone.parse(c[:end_date]))
        end
      end

      ::Mailing::Letter.transaction do
        ::Mailing::Letter.where(mailing_id: mailing.id).delete_all
        contacts.each do |contact_id|
          mailing.letters.create!(contact_id: contact_id)
        end
        mailing.contacts_ready!
        make_snapshot('contacts_linked')
      end
    end

  private
    def make_snapshot(event)
      mailing.snapshots.create!(
        event: event,
        data: mailing.attributes
      )
    end
  end
end
