module Ibis
  class UpdateUser
    include Procto.call
    
    attr_reader :user, :editor_id
    
    ALLOWED_ATTRIBUTES = %i(
      first_name
      last_name
      nickname
      email
      phone
      status
      avatar
      address
      company
      birthday
      company_speciality_id
      appointment_id
      income_id
      auto_id
      autopay
    )
    
    def initialize(user_id, params, editor_id)
      @user = User.find(user_id)
      user.assign_attributes(params.slice(*ALLOWED_ATTRIBUTES))
      @editor_id = editor_id
    end
    
    def call
      alert = user.autopay_changed?(from: true, to: false)
      if user.save
        create_snapshot
        send_alert if alert
        return true
      end
    end
    
    def errors
      user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   user,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.user_updated',
        data:      user.attributes
      )
    end

    # Важный метод!
    def send_alert
      InnerMailer.autopay_changed(user).deliver
    end
  end
end
