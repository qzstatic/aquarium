module Ibis
  module Reports
    class All
      include Procto.call
      
      attr_reader :page
      
      def initialize(page)
        @page = page
      end
      
      def call
        Report.order(id: :desc).page(page)
      end
    end
  end
end
