module Ibis
  class UpdateMobileUser
    include Procto.call
    
    attr_reader :mobile_user, :editor_id
    
    ALLOWED_ATTRIBUTES = %i(
      first_name
      last_name
      status
      operator
      msisdn
    )
    
    def initialize(mobile_user_id, params, editor_id)
      @mobile_user = ::MobileUser.find(mobile_user_id)
      mobile_user.assign_attributes(params.slice(*ALLOWED_ATTRIBUTES))
      @editor_id = editor_id
    end
    
    def call
      if mobile_user.save
        create_snapshot
        return true
      end
    end
    
    def errors
      mobile_user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   mobile_user,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mobile_user_updated',
        data:      mobile_user.attributes
      )
    end
  end
end
