module Ibis
  class DestroyUser
    include Procto.call
    
    attr_reader :user, :editor_id
    
    def initialize(user_id, editor_id)
      @user = User.find(user_id)
      @editor_id = editor_id
    end
    
    def call
      user.destroy && create_snapshot(user, 'ibis.user_destroyed')
      user.sessions.each do |session|
        session.update(user_id: nil, account_id: nil) && session.dump_to_redis && create_snapshot(session, 'shark.destroyed_user_unauthenticated')
      end
    end
    
  private
    def create_snapshot(subject, event)
      Snapshot.create!(
        subject:   subject,
        author_id: editor_id,
        service:   'ibis',
        event:     event,
        data:      subject.attributes
      )
    end
  end
end
