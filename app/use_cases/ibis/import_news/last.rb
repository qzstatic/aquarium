module Ibis
  module ImportNews
    class Last
      attr_reader :params
      
      def initialize(params)
        @params = params
      end
      
      def call
        limit = params[:limit] || 20
        offset = params[:offset] || 0
        order = params[:order]=='updated_at' ? :updated_at : :news_date
        from = params[:date_from] ? Time.parse(params[:date_from]) : Time.at(0)
        to = params[:date_to] ? Time.parse(params[:date_to]) : Time.now
        ::ImportNews.where(news_date: from..to).order(order => :desc).limit(limit).offset(offset)
      end
    end
  end
end
