module Ibis
  class DestroyOption
    include Procto.call
    
    attr_reader :option
    
    def initialize(option_id)
      @option = Option.find(option_id)
    end
    
    def call
      Option.save_type_to_redis(option.type) if option.destroy
    end
  end
end
