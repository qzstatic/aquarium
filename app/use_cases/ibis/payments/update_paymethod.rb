module Ibis
  module Payments
    class UpdatePaymethod
      attr_reader :paymethod, :editor_id
      
      def initialize(paymethod_id, params, editor_id)
        @paymethod = Paymethod.find(paymethod_id)
        paymethod.assign_attributes(params.slice(:slug, :title, :description))
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if paymethod.save
      end

      def errors
        paymethod.errors
      end

    private

      def create_snapshot
        paymethod.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.paymethod_updated',
          data:      paymethod.attributes
        )
      end
    end
  end
end
