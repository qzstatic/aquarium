module Ibis
  module Payments
    class SubscribersCounts

      attr_reader :timestamp, :stats, :error

      def initialize(params)
        current    = Time.now
        @timestamp = Time.new(
            params.fetch(:year, current.year),
            params.fetch(:month, current.month).to_i,
            params.fetch(:day, current.day).to_i,
            params.fetch(:hours, 23).to_i,
            params.fetch(:minutes, 59).to_i,
            params.fetch(:seconds, 59).to_i,
            '+03:00')
        @stats     = {}
        @mobile_period_slug = '1days'
      end

      def call
        begin
          Period.where.not(slug: @mobile_period_slug).each do |period|
            stats[period.title] = Payment.active_at(timestamp).joins(:payment_kit).where('payment_kits.period_id = ?', period.id).count
          end
          mobile_sub_period_id     = Period.find_by_slug(@mobile_period_slug).id
          stats['Всего больших']   = Payment.active_at(timestamp).joins(:payment_kit).where.not('payment_kits.period_id = ?', mobile_sub_period_id).count
          stats['Всего мобильных'] = Payment.active_at(timestamp).joins(:payment_kit).where('payment_kits.period_id = ?', mobile_sub_period_id).count
          true
        rescue Exception => e
          @error = e.backtrace.join("\n")
          false
        end
      end
    end
  end
end
