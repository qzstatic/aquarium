module Ibis
  module Payments
    class CreateProduct
      attr_reader :product, :editor_id
      
      def initialize(params, editor_id)
        @product   = Product.new(params.slice(:slug, :title))
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if product.save
      end

      def errors
        product.errors
      end

    private

      def create_snapshot
        product.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.product_created',
          data:      product.attributes
        )
      end
    end
  end
end
