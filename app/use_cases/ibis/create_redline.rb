module Ibis
  class CreateRedline
    include Procto.call
    
    attr_reader :redline, :editor_id
    
    def initialize(params, editor_id)
      @editor_id = editor_id
      @redline = Redline.new(params)
    end
    
    def call
      if redline.save
        create_snapshot
      end
    end
    
    def errors
      redline.errors
    end
  
  private
    def create_snapshot
      Snapshot.create!(
        subject:   redline,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.redline_created',
        data:      redline.attributes
      )
    end
  end
end
