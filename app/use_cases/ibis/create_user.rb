module Ibis
  class CreateUser
    include Procto.call
    
    attr_reader :user, :editor_id
    
    ALLOWED_ATTRIBUTES = %i(
      first_name
      last_name
      nickname
      email
      phone
      status
      avatar
      address
      company
      birthday
      company_speciality_id
      appointment_id
      income_id
      auto_id
    )
    
    def initialize(user_attributes, editor_id)
      @user = User.new(user_attributes.slice(*ALLOWED_ATTRIBUTES))
      @editor_id = editor_id
    end
    
    def call
      if user.save
        user.send_confirmation_email
        create_snapshot
      end
    end
    
    def errors
      user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   user,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.user_created',
        data:      user.attributes
      )
    end
  end
end
