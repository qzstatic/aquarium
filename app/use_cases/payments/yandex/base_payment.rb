require 'digest/md5'
module Payments
  module Yandex
    class BasePayment
      attr_accessor :errors, :params

      include Payments::Logger

      def initialize(params)
        @errors = []
        @params = params.symbolize_keys
      end

      def valid?(action)
        errors << :md5 and return if params[:md5] != self.checksum(params, action)
        errors << 'Не найден платеж в системе' and return unless payment
        if params[:rebillingOn] == 'true'
          errors << 'Подписка не может быть пролонгированна' and return unless payment.can_renewal_paymethod?
        end
        errors.empty?
      end

      def response
        hash = {}
        hash[:performedDatetime] = DateTime.current.rfc3339
        hash[:invoiceId] = params[:invoiceId]
        hash[:shopId] = Settings.yandex.shop_id
        if errors.include? :md5
          hash[:code] = 1
          hash[:message] = 'Значение параметра md5 не совпадает с результатом расчета хэш-функции'
        elsif errors.any?
          hash[:code] = 200
          hash[:message] = errors.join(', ')
        else
          hash[:code] = 0
        end

        logger.info "response for yandex: #{hash.inspect}"
        hash
      end

      def xml_response(tag_name)
        xml = Builder::XmlMarkup.new(indent: 1)
        xml.instruct!
        xml.tag!(tag_name,response)
        xml.target!
      end

      def checksum(hash, action)
        parts = []
        parts << action
        parts << hash[:orderSumAmount]
        parts << hash[:orderSumCurrencyPaycash]
        parts << hash[:orderSumBankPaycash]
        parts << Settings.yandex.shop_id
        parts << hash[:invoiceId]
        parts << hash[:customerNumber]
        parts << Settings.yandex.shop_password
        Digest::MD5.hexdigest(parts.join(';')).upcase
      end
      def payment
        @payment ||= ::Payment.notpaid.bymethod([:yandex_kassa, :yandex_money, :qiwi, :web_money]).find_by(id: params[:orderNumber])
      end
    end
  end
end
