module Payments
  module Yandex
    class CancelPayment < BasePayment
      def call
        if valid?('cancelOrder')
          payment.status = :canceled
          payment.save!
          Snapshot.create!(
              subject: payment,
              event:   'payment_yandex_cancel',
              data:    payment.attributes
          )
          logger.error { "Use case: #{self.class.name}, payment: #{payment.id}" }
        end
      end
      def xml_response
        super('cancelOrderResponse')
      end
    end
  end
end
