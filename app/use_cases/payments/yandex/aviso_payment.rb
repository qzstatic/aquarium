module Payments
  module Yandex
    class AvisoPayment < BasePayment
      def call
        if valid?('paymentAviso')
          payment.status = :paid
          payment.grant_access
          payment.save!
          Snapshot.create!(
              subject: payment,
              event:   'payment_yandex_aviso',
              data:    payment.attributes
          )
          logger.error { "Use case: #{self.class.name}, payment: #{payment.id}" }
        end
      end
      def xml_response
        super('paymentAvisoResponse')
      end
    end
  end
end
