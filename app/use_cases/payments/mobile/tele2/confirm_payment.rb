module Payments
  module Mobile
    module Tele2
      class ConfirmPayment < ::Payments::Mobile::Beeline::ConfirmPayment
        include Payments::Logger
      end
    end
  end
end
