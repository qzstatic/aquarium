module Payments
  module Mobile
    module Megafon
      class ProcessPayment

        def initialize(params)
          @body = params[:body]
        end

        def call
          Resque.enqueue(::Workers::Payments::MegafonCallbackVerification, @body)
        end
      end
    end
  end
end
