module Payments
  module Mobile
    module Megafon
      class ConfirmPayment < ::Payments::Mobile::ConfirmPayment
        include Payments::Logger

        def call
          super do
            redis_client.hset(redis_key, :error_code, status)
            @result = { redirect: wait_payment_url }
          end
        end

        def status
          attributes['operation_status']
        end

        def success?
          !status.blank? && status.eql?('ok') && !attributes['subscription_id'].blank? && !attributes['transactid'].blank?
        end

        def verify_payment!
          Resque.enqueue(::Workers::Payments::MegafonManualVerification, subscription_id, access_token)
        end
      end
    end
  end
end
