module Payments
  module Mobile
    module Mts
      class ProcessPayment

        def initialize(attributes)
          @body = attributes.require(:body)
        end

        def call
          Resque.enqueue(::Workers::Payments::MtsCallbackVerification, @body)
        end
      end
    end
  end
end
