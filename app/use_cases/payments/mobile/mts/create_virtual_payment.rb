module Payments
  module Mobile
    module Mts
      class CreateVirtualPayment < ::Payments::Mobile::CreateVirtualPayment

        def call
          super do
            if (@subscription_id = ::Operators.send(@operator).create_subscription(@msisdn, @access_token))
              @redirect = ::Operators.send(@operator).redirect_url_for(@subscription_id, template_id)
              save_to_redis!
              true
            else
              logger.error { "Use case: #{self.class.name}, errors: There is an error in operator's response" }
              redirect_to_wait_error!
            end
          end
        end

        private

        def template_id
          return @attributes[:template_id] unless @attributes[:template_id].blank?
          authorized? ? 3 : 1
        end
      end
    end
  end
end
