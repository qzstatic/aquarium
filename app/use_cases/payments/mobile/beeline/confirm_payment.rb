module Payments
  module Mobile
    module Beeline
      class ConfirmPayment < ::Payments::Mobile::ConfirmPayment
        include Payments::Logger

        def call
          super do
            create_fake_payment!(error_code: attributes[:resultCode])
            @result = { redirect: wait_payment_url }
          end
        end

        def success?
          attributes['serviceId']
        end

        def verify_payment!
          Resque.enqueue(manual_verification_worker, subscription_id, access_token)
        end

        def manual_verification_worker
          "::Workers::Payments::#{operator.to_s.capitalize}ManualVerification".constantize
        end
      end
    end
  end
end
