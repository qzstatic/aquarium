module Payments
  module Mobile
    class UpdatePayment

      attr_reader :payment

      def initialize(payment, attributes)
        @payment, @attributes = payment, attributes
      end

      def call
        ActiveRecord::Base.transaction do
          payment.assign_attributes(@attributes.symbolize_keys)
          if payment.save
            payment.grant_access
            create_snapshot
          end
        end
      end

      def create_snapshot
        payment.snapshots.create(
            event:   'payment_updated',
            data:    payment.attributes
        )
      end

      def errors
        payment.errors
      end
    end
  end
end
