module Payments
  module Logger
    def logger
      @logger ||= begin
        logger = ::Logger.new(Rails.root.join('log','payments.log'))
        logger.level = ::Logger::INFO
        logger
      end
    end
  end
end