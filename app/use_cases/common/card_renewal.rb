module Common
  class CardRenewal
    attr_reader :order, :access_right
    
    def initialize(order)
      @order = order
    end
    
    def call
      PaymentProcess.logger.info { "Use case: #{self.class.name}, original order: #{order.id}" }
      
      # проверяем, можно ли делать продление
      unless order.payment.can_renewal?
        PaymentProcess.logger.error { "Use case: #{self.class.name}, can't renewal: #{order.id}" }
        return
      end
      
      # создаем заказ с доступом
      params = {
        payment_method: Order.payment_methods[:by_card],
        price_id:       (order.price.renewal ? order.price.renewal.id : order.price.id),
        data: {
          biller_client_id:  order.data['biller_client_id'],
          auto_renewing:     true,
          autopayment:       true,
          previous_order_id: order.id
        }
      }
      neworder = Common::CreateOrder.new(params, order.user, order.client_ip).call
      
      order.data[:next_order_id] = neworder.id
      order.data_will_change!
      order.save!
      
      # делаем автоплатеж
      neworder.payment.renewal
    end
  end
end
