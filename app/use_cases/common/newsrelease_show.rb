module Common
  class NewsreleaseShow
    include Procto.call

    attr_reader :date, :newsrelease, :documents, :pages, :documents_limit

    def initialize(full: false, documents_limit: 1000, **params)
      @date = newsrelease_date(params)
      @newsrelease = if date.nil?
        last_newsrelease
      else
        Roompen.document(newspaper_url)
      end
      @documents = Roompen.bound_documents(@newsrelease.id, full: full)
      @documents_limit = documents_limit
    end

    def call
      fill_newspaper_pages(documents)
    end

  private
    def fill_newspaper_pages(documents)
      categories = Roompen.child_categories_of(%w(newspaper))
      hdocs = {}

      documents.reverse_each do |doc|
        if doc.categories.has_newspaper?
          hdocs[doc.categories.newspaper.slug] ||= []
          hdocs[doc.categories.newspaper.slug] << doc
        end
      end

      @pages = []
      documents_remain = documents_limit

      categories.each do |category|
        break if documents_remain<=0
        
        if hdocs.has_key?(category.slug)
          docs = hdocs[category.slug][0..documents_remain-1]
          documents_remain -= docs.size

          @pages << Roompen::List.new({
            title:     category.title,
            header:    category.title,
            slug:      category.slug,
            documents: docs
          })
        end
      end
    end

    def last_newsrelease
      newsreleases = Roompen.categorized(%w(kinds newsrelease), without: [%w(parts news)], limit: 1, full: true)
      if newsreleases.size == 1
        return newsreleases.first
      else
        raise ActionController::RoutingError.new('Not Found')
      end
    end

    def newsrelease_date(params)
      if [:year, :month, :day].all? { |key| params[key] }
        Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
      else
        nil
      end
    end
    
    def newspaper_url
      [Settings.domain, 'newspaper', date.strftime('%Y/%m/%d')].join('/')
    end
  end
end
