module Shark
  class UpdateMailing
    attr_reader :contact, :user, :type_ids
    
    def initialize(user, params)
      @user = user
      @contact = user.contact
      @type_ids = params[:type_ids] || []
    end
    
    def call
      unless contact
        use_case = Common::CreateContact.new({ email: user.email })
        use_case.call
        @contact = use_case.contact
      end

      if contact
        contact.user_id = user.id # if contact was not linked to user
        contact.type_ids = type_ids.map(&:to_i) + hidden_type_ids
        contact.contact_status = :confirmed
        contact.subscription_status = :on
        if contact.save
          contact.snapshots.create(
            event: 'contact_changed',
            data:  contact.attributes
          )
        end
      end
    end
    
    def errors
      contact.errors
    end
  
  private
    def hidden_type_ids
      @hidden_type_ids ||= contact.mail_types.select(&:hidden?).map(&:id)
    end
  end
end
