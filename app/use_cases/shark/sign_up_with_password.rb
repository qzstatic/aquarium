require 'new_relic/agent/method_tracer'

module Shark
  class SignUpWithPassword
    include ::NewRelic::Agent::MethodTracer

    attr_reader :user, :update_session, :mailing_type_ids
    
    PERMITTED_ATTRIBUTES = %i(
      first_name
      last_name
      nickname
      email
      phone
    )
    
    def initialize(params, ip)
      @user = User.new(params.slice(*PERMITTED_ATTRIBUTES))
      
      user.accounts.build do |account|
        account.type     = 'password'
        account.password = params[:password]
      end
      
      @update_session = UpdateSession.new(params.fetch(:access_token), ip)

      mailing_type_slugs = Array(params[:mailing_type_slugs]) + [ 'marketing' ]
      @mailing_type_ids = ::Mailing::MailType.where(slug: mailing_type_slugs).pluck(:id)
    end
    
    def save
      if user.save
        user.send_confirmation_email(token_data)
        update_session.call(
          user_id:    user.id,
          account_id: user.accounts.first.id
        )
        
        if contact
          contact.update(user_id: user.id)
          create_contact_snapshot('contact_updated')
        else
          user.create_contact(email: user.email)
          create_contact_snapshot('contact_created')
        end
        
        create_user_snapshot
        
        notify_newrelic
      end
    end
    
    def errors
      user.errors
    end

    add_method_tracer :save, 'Custom/Shark_SignUpWithPassword_save'
    
  private
    def create_user_snapshot
      Snapshot.create!(
        subject: user,
        author:  user,
        service: 'shark',
        event:   'shark.user_signed_up_with_password',
        data:    user.attributes
      )
    end
    
    def create_contact_snapshot(event)
      user.contact.snapshots.create!(
        event:   event,
        data:    contact.attributes
      )
    end
    
    def notify_newrelic
      ::NewRelic::Agent.increment_metric('Custom/PasswordSignUp')
    end

    def token_data
      if mailing_type_ids.present?
        { mailing_type_ids: mailing_type_ids }
      else
        {}
      end
    end
    
    def contact
      Contact.find_by(email: user.email)
    end
  end
end
