module Shark
  module Cache
    class PurgePath < Base

      BASE_PATH = '/purge/'

      attr_reader :path

      def initialize(path)
        @path = path
      end

      private
      def full_urls
        [Settings.hosts.shark, Settings.hosts.m_shark].map do |host|
          "#{host}" + BASE_PATH + path
        end
      end
    end
  end
end
