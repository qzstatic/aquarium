module Shark
  class CreateFeedback
    include Procto.call
    
    attr_reader :feedback
    
    def initialize(attributes)
      @feedback = Feedback.new(attributes)
    end
    
    def call
      feedback.save
    end
    
    def errors
      feedback.errors
    end
  end
end
