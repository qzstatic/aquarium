require 'digest/sha1'

module Shark
  module Comments
    class UpdatePopular
      attr_reader :widget_id, :xid, :comment, :document

      def initialize(widget_id, xid)
        @widget_id = widget_id
        @xid       = xid
      end
      
      def call
        @comment = PopularComment.find_or_create_by(widget_id: widget_id, xid: xid)
        
        @document = Roompen.document_by_id(xid)
        return false unless document
        
        resp = connection.post '/1.0/comments/list', body: body.to_json, signature: signature
        
        if resp.success? && resp.body[:result] == 'success' && resp.body[:data].present?
          comment.body = resp.body[:data].first
          comment.save
          purge_cache
        elsif !resp.body[:data].present?
          comment.destroy
          purge_cache
        end
      end
    
    private
      def body
        {
          widget_id: widget_id,
          link:      document.url,
          sort:      'popular',
          limit:     1,
          offset:    0,
          xid:       xid
        }
      end
      
      def signature
        Digest::SHA1.hexdigest(body.to_json + Settings.hypercomments.secret_key)
      end
      
      def connection
        Faraday.new(url: Settings.hypercomments.api_host) do |builder|
          builder.request  :url_encoded
          builder.response :oj, content_type: 'application/json'
          builder.adapter  Faraday.default_adapter
        end
      end

      def purge_cache
        Settings.shark.srv_ids.each do |srv_id|
          Faraday.get("#{Settings.hosts.shark}/purge/includes/long_cache/popular_comment/#{xid}") do |request|
            request.headers['Cookie'] = "srv_id=#{srv_id}"
          end
        end
      end
    end
  end
end
