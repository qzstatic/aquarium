module Shark
  class PaymentsOld
    module Card
      class Callback < PaymentsOld
        include ::Payments::Logger
        attr_reader :transaction_id, :client_ip
        
        def initialize(transaction_id, client_ip)
          @transaction_id = transaction_id
          @client_ip      = client_ip
          @status         = :ok
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {transaction_id: #{transaction_id}, client_ip: #{client_ip}}" }
          if order && order.payment
            order.client_ip = client_ip if client_ip
            order.data.delete('response_for_client')
            order.data.delete('status_for_client')
            
            if order.save
              Snapshot.create!(
                subject: order,
                service: 'shark',
                event:   'payments_card_callback',
                data:    order.attributes
              )
              order.payment.do_async(:final)
            else
              logger.error { "Use case: #{self.class.name}, error: #{order.errors}, transaction_id: #{transaction_id}" }
              error_response :unprocessable_entity, order.errors
            end
          else
            logger.error { "Use case: #{self.class.name}, error: Order not found, transaction_id: #{transaction_id}" }
            error_response :not_found, 'Order not found'
          end
        end
        
        def order
          @order ||= Order.by_card.find_by_transaction_id(transaction_id)
        end
      end
    end
  end
end
