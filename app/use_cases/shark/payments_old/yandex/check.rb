module Shark
  class PaymentsOld
    module Yandex
      class Check < PaymentsOld
        include ::Payments::Logger
        attr_reader :params, :uuid, :client_ip
        
        def initialize(params, client_ip)
          @params    = params
          @uuid      = params.require(:customerNumber)
          @client_ip = client_ip
          @status    = :ok
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {params: #{params}, client_ip: #{client_ip}}" }
          if !order
            logger.error { "Use case: #{self.class.name}, error: Order not found, uuid: #{uuid}" }
            error_response :not_found, 'Order not found'
          elsif order.payment && order.payment.respond_to?(:check)
            order.transaction_id = params.require(:invoiceId)
            if response = order.payment.check(params, client_ip)
              response
            else
              logger.error { "Use case: #{self.class.name}, error: Forbidden, uuid: #{uuid}" }
              error_response :forbidden, 'Forbidden'
            end
          else
            logger.error { "Use case: #{self.class.name}, error: Invalid order, uuid: #{uuid}" }
            error_response :unprocessable_entity, 'Invalid order'
          end
        end
        
        def order
          order ||= Order.by_yandex.find_by_uuid(uuid)
        end
      end
    end
  end
end
