module Shark
  class CheckPasswordRecoveryToken
    attr_reader :token
    
    def initialize(password_recovery_token)
      @token = UserToken.password_recovery.unused.find_by(token: password_recovery_token)
    end
    
    def correct?
      token.present?
    end
  end
end
