module Shark
  module Restrictions
    class Base

      NAMESPACE = 'restricted'

      private

      def redis_cli
        Aquarium::Redis.connection
      end

      def redis_key
        "#{NAMESPACE}:#{@document_url}"
      end
    end
  end
end
