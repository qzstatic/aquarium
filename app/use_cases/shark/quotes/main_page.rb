module Shark
  module Quotes
    class MainPage
      include Procto.call
      
      def call
        tickers.sort { |a, b| slugs.index(a.full_slug) <=> slugs.index(b.full_slug)}
      end
      
      def slugs
        Settings.quotes.main_page
      end
      
      def tickers
        Ticker.joins(:quote_source).where("concat_ws('.', quote_sources.slug, tickers.slug) in (?)", slugs).includes(:quote_source)
      end
    end
  end
end
