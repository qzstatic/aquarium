module Shark
  class SignOut
    include Procto.call
    
    attr_reader :update_session
    
    def initialize(params, ip)
      @update_session = UpdateSession.new(params.fetch(:access_token), ip)
    end
    
    def call
      update_session.call(
        user_id:    nil,
        account_id: nil
      )
    end
  end
end
