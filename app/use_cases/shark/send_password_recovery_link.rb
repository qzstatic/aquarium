module Shark
  class SendPasswordRecoveryLink
    include Procto.call
    
    attr_reader :user
    
    def initialize(email)
      @user = User.find_by!(email: email.downcase)
    end
    
    def call
      token = user.tokens.password_recovery.generate
      UserMailer.password_recovery_email(token).deliver
    end
  end
end
