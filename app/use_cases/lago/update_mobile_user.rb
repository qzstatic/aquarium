module Lago
  class UpdateMobileUser
    extend Forwardable

    attr_reader :mobile_user, :attributes, :use_case, :redirect

    def initialize(mobile_user, attributes)
      @attributes    = attributes
      @mobile_user   = mobile_user
      @mailing_types = @attributes.delete(:mailing_types)
      @redirect = Settings.hosts.shark
    end

    def call
      unless mobile_user || mobile_user.contact
        @errors = 'Мобильный пользователь не найден'
        return false
      end
      @use_case = Common::CreateContact.new(attributes)

      if @use_case.call
        @use_case.contact.update(mobile_user_id: mobile_user.id, type_ids: Mailing::MailType.where(slug: @mailing_types).map(&:id)) && create_contact_snapshot
        @redirect = mobile_user.return_url
        true
      else
        nil
      end
    end



    def create_contact_snapshot
      Snapshot.create!(
          subject: @use_case.contact,
          event:   'contacts_updated',
          data:    @use_case.contact.attributes
      )
    end

    def errors
      @errors ||= use_case.errors
    end
  end
end
