module Lago
  module Code
    class Base
      CODE_LIFETIME = 600.seconds

      attr_reader :code, :errors, :access_token, :redirect

      private

      def otp_token
        @otp_token ||= ROTP::Base32.random_base32
      end

      def redis_token
        redis_client.hgetall(redis_key)
      end

      def redis_client
        Aquarium::Redis.connection
      end

      def redis_key
        "otp:#{access_token}"
      end
    end
  end
end
