require 'resque/server'

Rails.application.routes.draw do
  mount Resque::Server.new, at: '/resque'

  # Load balancer
  get '/check_alive' => 'main#check_alive'

  namespace :pigeon, defaults: { format: :json } do
    resources :mailings, only: [:show] do
      post :notify, on: :collection
      post :update, on: :member
      resources :contacts, only: :index
    end
  end

  namespace :ibis, defaults: { format: :json } do
    resources :options, only: %i(index create update destroy) do
      collection do
        post :dump
        get  :by_type
        get  :types
      end
    end

    resources :contacts, except: %i(new edit) do
      collection do
        get :count
      end
      member do
        get :letters
        get '/letters/:letter_id/details' => 'contacts#letter_details'
      end
    end

    resources :users, only: %i(index create show update destroy) do
      member do
        get :full
      end

      collection do
        get :count
        get :find_by_email
        get :search
        get :payment_conditions
      end
    end

    resources :mobile_users, only: %i(index create show update destroy) do
      collection do
        get :count
        get :search
      end
    end

    resources :orders, only: %i(index show create update) do
      get :count, on: :collection
    end

    resources :mailings, only: %i(index show create update count) do
      get :count, on: :collection
      get :preview, on: :collection
      member do
        get :contacts_count
        post :deliver_test
        post :build_template
        post :attach_contacts
        post :ready
        post :not_ready
      end
    end

    resources :reports, only: %i(index create)

    get '/import_news/'    => 'import_news#index'
    get '/import_news/:id' => 'import_news#show'
    post '/generate_flash' => 'newspaper#generate_flash'
    post '/dulton/upload'  => 'dulton#upload'

    scope :indexer do
      scope :users do
        get '/page/(:page)'     => 'indexer/users#page',  page:      /[0-9]+/
        get '/fresh/:timestamp' => 'indexer/users#fresh', timestamp: /[0-9]+/
      end

      scope :mobile_users do
        get '/page/(:page)'     => 'indexer/mobile_users#page',  page:      /[0-9]+/
        get '/fresh/:timestamp' => 'indexer/mobile_users#fresh', timestamp: /[0-9]+/
      end

      scope :orders do
        get '/page/(:page)'     => 'indexer/orders#page',  page:      /[0-9]+/
        get '/fresh/:timestamp' => 'indexer/orders#fresh', timestamp: /[0-9]+/
      end

      scope :payments do
        get '/page/(:page)'     => 'indexer/payments#page',  page:      /[0-9]+/
        get '/fresh/:timestamp' => 'indexer/payments#fresh', timestamp: /[0-9]+/
      end

      scope :contacts do
        get '/page/(:page)'     => 'indexer/contacts#page',  page:      /[0-9]+/
        get '/fresh/:timestamp' => 'indexer/contacts#fresh', timestamp: /[0-9]+/
      end
    end

    resources :products, only: %i(index show create update) do
      get :namespaces
      get :active_namespaces
      get :subproducts
      get :active_payment_kits
    end

    resources :namespaces, only: %i(index show create update) do
      get :payment_kits
      patch :set_base

      member do
        get :unsyncable_payment_kits
      end

      collection do
        get :unsyncable
      end
    end

    resources :subproducts, only: %i(show create update) do
      get :payment_kits
    end

    resources :periods,      only: %i(index show create update)
    resources :paymethods,   only: %i(index show create update)

    resources :payment_kits, only: %i(index show create update destroy) do
      get :associations
    end

    resources :cities, only: %i(index show create update destroy)

    resources :payments, only: %i(index show create update) do
      collection do
        get :statuses
        get :count
        get :export
        get 'subscribers_counts(/:year)(/:month)(/:day)(/:hours)(/:minutes)(/:seconds)' => 'payments#subscribers_counts', year: /\d{4,4}/, month: /0?[1-9]|1[0-2]/, day: /0?[1-9]|[1-2][0-9]|3[0-1]/, hours: /\d{1,2}/, minutes: /\d{1,2}/, seconds: /\d{1,2}/
      end
    end

    resources :redlines, only: %i(index show create update) do
      collection do
        get :count
      end
    end

    resources :people, except: [:new, :edit, :create, :destroy] do
      collection do
        get :synonyms
      end
    end
    
    namespace :mailing do
      resources :banners, only: %i(index show create update) do
        collection do
          get :count
        end
      end

      resources :mail_types, only: %i(index show create update)

      resources :stats, only: %i(links_clicks banners_clicks) do
        member do
          get :links_clicks
        end

        collection do
          get :banners_clicks
        end
      end

      resources :rules, only: %i(create update) do
        get :current, on: :collection
      end
    end
  end

  namespace :shark, defaults: { format: :json } do
    # password
    post '/sign_up_with_password' => 'users#sign_up_with_password'
    post '/sign_in_with_password' => 'users#sign_in_with_password'

    # oauth
    post '/sign_up_with_oauth' => 'users#sign_up_with_oauth'
    post '/sign_in_with_oauth' => 'users#sign_in_with_oauth'
    post '/create_social_account_link'  => 'users#create_social_account_link'
    post '/social_account_link_confirm' => 'users#confirm_social_account_link'

    # sessions
    get '/sessions/:token' => 'sessions#show', token: /[0-9a-f]+/
    delete '/sign_out' => 'users#sign_out'
    post '/confirm'    => 'users#confirm'
    post '/sessions'   => 'sessions#create'

    # user profile
    constraints(token: /[0-9a-f]+/) do
      get   '/users/:token'                      => 'users#show'
      get   '/users/:token/profile'              => 'users#profile'
      patch '/users/:token'                      => 'users#update'
      patch '/users/:token/password'             => 'users#change_password'
      patch '/users/:token/mailing'              => 'users#update_mailing'
      patch '/users/:token/accept_promo'         => 'users#accept_promo'
      put   '/users/:token/repeat_email_confirm' => 'users#repeat_email_confirm'
    end

    # password reset
    post '/send_password_reset_link'  => 'users#send_password_reset_link'
    get '/check_password_reset_token' => 'users#check_password_reset_token'
    post '/reset_password'            => 'users#reset_password'

    # feedback
    post '/feedback' => 'feedback#create'

    # subscriber
    get  '/contacts/:id'         => 'contacts#show'
    post '/contacts'             => 'contacts#create'
    post '/contacts/confirm'     => 'contacts#subscribe_confirm'
    post '/contacts/unsubscribe' => 'contacts#unsubscribe_confirm'
    get  '/contacts/unsubscribe/:unsubscribe_token' => 'contacts#unsubscribe_info', unsubscribe_token: /[a-zA-Z\d\-_=]+/

    # comments
    get   '/popular_comments/:widget_id/:xid' => 'comments#show_popular',   widget_id: /\d+/, xid: /\d+/
    patch '/popular_comments/:widget_id/:xid' => 'comments#update_popular', widget_id: /\d+/, xid: /\d+/

    scope :newspaper do
      # newspaper get archive url
      get '/:year/:month/:day/zip' => 'newspapers#zip', year: /\d{4}/, month: /\d{2}/, day: /\d{2}/
      # get '/last/zip'              => 'newspapers#zip'

      # newspaper generate archive
      get '/:year/:month/:day/generate_archive' => 'newspapers#generate_archive', year: /\d{4}/, month: /\d{2}/, day: /\d{2}/
      get '/last/generate_archive'              => 'newspapers#generate_archive'
    end

    namespace :orders do
      post '/card'          => 'card#create'
      post '/card/callback' => 'card#callback'
      get  '/card/check'    => 'card#check'

      post '/yandex'       => 'yandex#create'
      post '/yandex/check' => 'yandex#check'
      post '/yandex/aviso' => 'yandex#aviso'

      post '/apple'                   => 'apple#verify'
      post '/apple/link_subscription' => 'orders#link_subscription' # оставил для совместимости, потом можно будет удалить

      post '/google' => 'google#verify'

      post '/link_subscription' => 'orders#link_subscription'

      get  '/mobile_subscriptions/:target' => 'orders#mobile_subscriptions', target: /apple|google/
    end

    namespace :quotes do
      get :main_page
    end

    namespace :prices do
      get :active
    end

    namespace :redlines do
      get :active
    end

    namespace :mailing do
      get '/mail_types/:slug' => 'mail_types#show_by_slug', slug: /[a-z\d_]+/
    end
  end

  namespace :metynnis, defaults: { format: :json } do
    namespace :payments do
      post '/card' => 'card#create'
      get  '/card/check_transaction_id/:id' => 'card#check_transaction_id', id: /\d+/
      post '/card/callback' => 'card#callback'
      get  '/card/check_result/:id' => 'card#check_result', id: /\d+/

      post '/cash'  => 'cash#create'
      post '/trial' => 'trial#create'

      resource :paymaster, only: :create, controller: :pay_master do
        post :confirmation
        post :notification
      end

      namespace :yandex do
        resource :money,    controller: :yandex_money, only: :create
        resource :kassa,    controller: :yandex_kassa, only: :create
        resource :qiwi,     controller: :qiwi,         only: :create
        resource :web_money,controller: :web_money,    only: :create
        scope controller: :yandex_callback do
          post :check
          post :payment
          post :cancel
        end
      end

      constraints operator: /mts|megafon|beeline|tele2/ do
        post '/:operator'                        => 'mobile#create'
        post '/:operator/confirm'                => 'mobile#confirm'
        get  '/:operator/check/:subscription_id' => 'mobile#check'
        post '/:operator/callback'               => 'mobile#callback'
      end
      resources :subscribe_requests, only: [] do
        collection do
          post :company
          post :gift
        end
      end
    end

    namespace :payment_kits do
      get '/all/:product/:namespace', action: 'index', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/

      get '/suitable/unregistered/:product/:namespace', action: 'suitable_index', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/
      get '/suitable/unregistered/:product/:namespace/:subproduct/:period', action: 'suitable_show', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, subproduct: /[a-z\d_]+/, period: /[a-z\d_]+/


      get '/suitable/:token/:product/:namespace', action: 'suitable_index', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, token: /[0-9a-f]+/
      get '/suitable/:token/:product/:namespace/:subproduct/:period', action: 'suitable_show', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, subproduct: /[a-z\d_]+/, period: /[a-z\d_]+/, token: /[0-9a-f]+/

      get '/combined(/:token)', action: 'combined_index', token: /[0-9a-f]+/
      get '/combined/:product/:namespace(/:token)', action: 'combined_index', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, token: /[0-9a-f]+/
      get '/combined/:product/:namespace/:subproduct/:period(/:token)', action: 'combined_show', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, subproduct: /[a-z\d_]+/, period: /[a-z\d_]+/, token: /[0-9a-f]+/

      get '/:product/:namespace/:subproduct/:period', action: 'show', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, subproduct: /[a-z\d_]+/, period: /[a-z\d_]+/

      get '/subproducts/unregistered/:product/:namespace', action: 'subproducts', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/
      get '/subproducts/:token/:product/:namespace', action: 'subproducts', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/, token: /[0-9a-f]+/
    end

    resources :cities, only: [:index]

    namespace :namespaces do
      get :base
      get '/:product/:namespace', action: 'show_by_slug', product: /[a-z\d_]+/, namespace: /[a-z\d_]+/
    end
  end

  namespace :snipe, defaults: { format: :json } do
    namespace :cache do
      post 'purge'
    end

    # restrictions
    resources :restrictions, only: %i(create) do
      delete :destroy, on: :collection
    end
  end

  namespace :lago, defaults:{ foramt: :json } do
    # mobile users
    resources :mobile_users, only: %i(show), param: :token, token: /[0-9a-f]+/ do
      member do
        post :subscribe
      end
      resource :codes, only: %i(create) do
        collection do
          post :check
        end
      end
    end
  end
end
