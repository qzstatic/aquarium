app_root = File.expand_path('../../..', __FILE__)

worker_processes   2
preload_app        true
timeout            45
listen             9030
user               'app'
working_directory  app_root
stderr_path        'log/unicorn.stderr.log'
stdout_path        'log/unicorn.stdout.log'

before_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end
end

after_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end
end
