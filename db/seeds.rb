# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#

[
  'Строительство и недвижимость',
  'Транспорт',
  'Торговля',
  'Промышленность',
  'Бытовые услуги / сервис',
  'Банки и финансы',
  'Интеллектуальные услуги / консалтинг / юриспруденция',
  'Сфера питания / ресторанный бизнес',
  'Государственная служба',
  'Фармацевтика / медицина',
  'Туризм и гостиничный бизнес',
  'Логистика',
  'Культура',
  'Наука / образование',
  'Страхование',
  'Нефть и газ',
  'Красота / спорт / уход за собой',
  'Телеком',
  'Шоу-бизнес',
  'Другое'
].each { |company_speciality| Option.company_speciality.create(value: company_speciality) }

[
  'Руководитель крупной или средней компании (более 50 сотр.)',
  'Руководитель небольшой компании (50 сотр. или меньше)',
  'Руководитель среднего звена (подразделения, отдела, группы)',
  'Специалист',
  'Служащий',
  'Учащийся'
].each { |appointment| Option.appointment.create(value: appointment) }

[
  'Менее 50 000 руб.',
  '50 000 – 99 999 руб.',
  '100 000 – 199 999 руб.',
  '200 000 руб. и более'
].each { |income| Option.income.create(value: income) }

[
  'Ford Focus',
  'Mitsubishi Lancer',
  'Mazda 6'
].each { |auto| Option.auto.create(value: auto) }

Ibis::DumpOptions.call

# Quote sources
{
  currencies_cb:    'Курсы валют ЦБ РФ',
  indices_world:    'Мировые фондовые индексы',
  metals:           'Драгметаллы ЦБ РФ',
  oil:              'Нефть',
  micex_shares:     'Акции ММВБ',
  micex_indices:    'Индексы ММВБ',
  micex_currencies: 'Валюта ММВБ',
  micex_futures:    'Срочный рынок ММВБ'
}.each do |slug, value|
  QuoteSource.create(slug: slug, title: value)
end

# Sources for news import
[
  { slug: 'finmarket', title: 'Финмаркет', url: 'http://www.finmarket.ru/' },
  { slug: 'prime',     title: 'Прайм',     url: 'http://www.1prime.ru/' },
  { slug: 'akm',       title: 'АК&М',      url: 'http://www.akm.ru/' },
  { slug: 'reuters',   title: 'Reuters',   url: 'http://www.reuters.com/' },
  { slug: 'r_sport',   title: 'Р-спорт',   url: 'http://rsport.ru/' },
  { slug: 'interfax',  title: 'Интерфакс', url: 'http://www.interfax.ru/' },
  { slug: 'itar_tass', title: 'ТАСС',      url: 'http://itar-tass.com/' },
  { slug: 'bloomberg', title: 'Bloomberg', url: 'http://www.bloomberg.com/' }
].each do |data|
  ImportSource.create(data)
end

# renewal prices
online_for_month = Price.create(
    product: :online,
    period:  :for_month,
    value:   769,
    since:   Time.now
)

renewal_prices = {}
[
  [ :profi,  :for_6months, 4900 ],
  [ :profi,  :for_year,    9100 ],
  [ :paper,  :for_6months, 4540 ],
  [ :paper,  :for_year,    6490 ]
].each do |p|
  renewal_prices[ p[0..1].join('_') ] = Price.create(
    product:       p[0],
    period:        p[1],
    value:         p[2],
    since:         Time.now,
    expire:        Time.now
  )
end

# active prices
[
  [ :profi,  :for_6months, 2798, 9080,  nil ],
  [ :profi,  :for_year,    5596, 12980, nil ],
  [ :online, :for_6months, 1399, 4540,  online_for_month ],
  [ :online, :for_year,    2798, 6490,  online_for_month ],
  [ :paper,  :for_6months, 1399, 4540,  nil ],
  [ :paper,  :for_year,    2798, 6490,  nil ]
].each do |p|
  Price.create(
    product:       p[0],
    period:        p[1],
    value:         p[2],
    since:         Time.now,
    expire:        Time.parse('2015-03-11 00:00:00'),
    crossed_price: p[3],
    renewal:       p[4] || renewal_prices["#{p[0]}_#{p[1]}"]
  )
end

# разные типы рассылок Mailing::MailType
[
  { slug: 'newsrelease',       title: 'Свежий номер' },
  { slug: 'test',              title: 'Тестовая рассылка' },
  { slug: 'invisible',         title: 'Невидимая рассылка', is_visible: false, subscribers_only: false },
  { slug: 'subscribers_only',  title: 'Только для платных', subscribers_only: true },
  { slug: 'invisible_subsrcr', title: 'Только для платных, да ещё и скрытая', is_visible: false, subscribers_only: true },
].each do |data|
  Mailing::MailType.create(data)
end
