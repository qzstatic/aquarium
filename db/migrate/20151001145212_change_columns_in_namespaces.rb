class ChangeColumnsInNamespaces < ActiveRecord::Migration
  def up
    change_table :namespaces do |t|
      t.json   :picture, null: false, default: {}
      t.hstore :texts,          null: false, default: {}

      t.remove :error_text
    end
  end

  def down
    change_table :namespaces do |t|
      t.text :error_text

      t.remove :picture
      t.remove :texts
    end
  end
end