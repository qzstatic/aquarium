class AddSeveralTexts < ActiveRecord::Migration
  def change
    add_column :subproducts, :phone_text,  :text
    add_column :paymethods,  :description, :text
    add_column :namespaces,  :error_text,  :text
  end
end
