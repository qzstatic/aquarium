class ChangeCommentInOrder < ActiveRecord::Migration
  def up
    remove_column :orders, :comment
    add_column :orders, :managers_data, :hstore, null: false, default: {}
  end

  def down
    remove_column :orders, :managers_data
    add_column :orders, :comment, :string
  end
end
