class DataFieldToMailing < ActiveRecord::Migration
  def change
    add_column :mailings, :data, :json, null: false, default: {}
  end
end
