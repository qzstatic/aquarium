class AddUserDataToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :user_data, :json, default: {}, null: false
  end
end
