class RenameMobileUserColumns < ActiveRecord::Migration
  def change
    rename_column :mobile_users, :phone, :msisdn
  end
end
