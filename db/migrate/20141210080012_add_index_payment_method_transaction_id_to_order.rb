class AddIndexPaymentMethodTransactionIdToOrder < ActiveRecord::Migration
  def change
    add_index :orders, [:payment_method, :transaction_id], unique: true
  end
end
