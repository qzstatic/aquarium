class ChangeUserIdInUserTokens < ActiveRecord::Migration
  def up
    change_column :user_tokens, :user_id, :integer, null: true
  end
  
  def down
    change_column :user_tokens, :user_id, :integer, null: false
  end
end
