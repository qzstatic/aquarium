class AddAutopayToUsers < ActiveRecord::Migration
  def change
    add_column :users, :autopay, :boolean, null: false, default: false
  end
end
