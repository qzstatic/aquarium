class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.integer    :product,       null: false
      t.integer    :period,        null: false
      t.boolean    :student,       null: false, default: false
      t.decimal    :value,         null: false, precision: 10, scale: 2
      t.datetime   :since,         null: false
      t.datetime   :expire
      t.integer    :upsell_ids,    null: false, array: true, default: []
      t.decimal    :crossed_price,              precision: 10, scale: 2

      t.timestamps
    end
    add_index :prices, [:product, :period, :student, :since, :expire], name: 'index_prices_all_options'
    add_index :prices, [:since, :expire]
  end
end
