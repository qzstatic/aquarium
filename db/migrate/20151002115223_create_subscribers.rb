class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.string  :email,               null: false
      t.integer :user_id,             null: true
      t.integer :subscriber_status,   null: false, default: 0
      t.integer :subscription_status, null: false, default: 0
    
      t.timestamps
      
      t.index :email, unique: true
    end
  end
end
