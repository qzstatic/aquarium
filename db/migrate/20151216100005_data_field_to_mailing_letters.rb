class DataFieldToMailingLetters < ActiveRecord::Migration
  def change
    add_column :mailing_letters, :data, :json, null: false, default: {}
  end
end
