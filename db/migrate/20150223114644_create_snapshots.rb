class CreateSnapshots < ActiveRecord::Migration
  def change
    create_table :snapshots do |t|
      t.references :subject, polymorphic: true, index: true, null: false
      t.references :author,  index: true
      t.string :event,       null: false
      t.json :data,          null: false, default: {}
      
      t.timestamps
    end
  end
end
