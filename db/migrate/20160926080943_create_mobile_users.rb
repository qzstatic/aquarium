class CreateMobileUsers < ActiveRecord::Migration
  def change
    create_table :mobile_users do |t|
      t.integer :status
      t.string  :first_name
      t.string  :last_name
      t.string  :email
      t.integer :phone, limit: 8, null: false
      t.string  :operator, null: false
      t.timestamps

      t.index :email, unique: true
      t.index :phone, unique: true
    end
    add_reference :sessions, :mobile_user, index: true
  end
end
