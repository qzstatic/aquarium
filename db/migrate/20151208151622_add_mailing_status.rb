class AddMailingStatus < ActiveRecord::Migration
  def change
    add_column :mailings, :status, :integer, default: 0, null: false
  end
end
