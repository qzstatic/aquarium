class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :type,  null: false
      t.string :value, null: false
      
      t.timestamps
    end
  end
end
