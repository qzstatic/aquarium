class ChangeUserTokens < ActiveRecord::Migration
  def up
    rename_table :confirmation_tokens, :user_tokens
    add_column :user_tokens, :type, :string
    UserToken.update_all(type: 'confirmation')
    change_column_null :user_tokens, :type, false
  end
  
  def down
    remove_column :user_tokens, :type
    rename_table :user_tokens, :confirmation_tokens
  end
end
