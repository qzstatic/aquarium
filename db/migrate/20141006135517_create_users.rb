class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :first_name
      t.string  :last_name
      t.string  :nickname, null: false
      t.string  :email,    null: false
      t.string  :phone
      t.integer :status,   null: false, default: 0
      t.json    :avatar,   null: false, default: '{}'
      t.json    :address,  null: false, default: '{}'
      t.string  :company
      t.date    :birthday
      
      t.integer :company_speciality_id
      t.integer :appointment_id
      t.integer :income_id
      t.integer :auto_id
      
      t.timestamps
      
      t.index :email, unique: true
    end
  end
end
