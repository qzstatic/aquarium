class AddContactStatusToMailing < ActiveRecord::Migration
  def change
    add_column :mailings, :contacts_status, :integer, null: false, default: 0
  end
end
