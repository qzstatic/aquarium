class CreatePressreleaseBanners < ActiveRecord::Migration
  def change
    create_table :pressrelease_banners do |t|
      t.integer :document_id,  null: false, limit: 8
      t.integer :banner_id,    null: false, limit: 8
      
      t.index   :document_id,  unique: true
      t.index   :banner_id,    unique: true
      
      t.timestamps
    end
  end
end
