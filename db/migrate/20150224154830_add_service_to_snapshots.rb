class AddServiceToSnapshots < ActiveRecord::Migration
  def change
    add_column :snapshots, :service, :string
  end
end
