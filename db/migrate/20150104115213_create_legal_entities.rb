class CreateLegalEntities < ActiveRecord::Migration
  def change
    create_table :legal_entities do |t|
      t.string  :name,                  null: false
      t.string  :access_type,           null: false
      t.inet    :ips,      array: true, null: false, default: []
      t.integer :user_ids, array: true, null: false, default: []
      
      t.timestamps
    end
  end
end
