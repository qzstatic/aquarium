class RemoveAutoconfirmFromPaymentKit < ActiveRecord::Migration
  def up
    remove_column :payment_kits, :autoconfirm
  end

  def down
    add_column :payment_kits, :autoconfirm, :boolean, default: false, null: false
  end
end
