class AddErrorColumnToMailingMails < ActiveRecord::Migration
  def change
    add_column :mailing_mails, :error, :text, default: ''
  end
end
