class CreateMailingRules < ActiveRecord::Migration
  def change
    create_table :mailing_rules do |t|
      t.string  :code
      t.integer :status, null: false, default: 0
      t.timestamps

      t.index :code, unique: true
    end
  end
end
