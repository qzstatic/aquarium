RSpec.describe Ibis::ContactsController, :type => :controller do
  let (:user) { User.create!(email:'test@test.com', first_name: 'Vasya', last_name: 'Pupkin') }
  let (:mailing) { Mailing.create!(type_id: mailing_type.id, template: 'template', subject: 'subject') }
  let (:contact) { Contact.create!(user: user, email: 'test@test.com', mailings: [mailing], type_ids: [mailing_type.id]) }
  let (:mailing_type) { Mailing::MailType.create!(title: 'mail_type_1', slug: :newsrelease ) }
  let (:letter) { Mailing::Letter.create!(contact_id: contact.id, mailing_id: mailing.id, status: 2, mail: 'mail body') }

  let (:filtering_params) { {} }

  before :each do
    allow_any_instance_of(SnipeClient).to receive_message_chain(:get, :body) { {editor_id: 1} }
  end

  it '#index' do
    get :index, mailing_id: contact.mailing_ids.first, type_id: contact.type_ids.first
    expect(assigns(:contacts).count).to eql(1)
    get :index, mailing_id: contact.mailing_ids.first, type_id: 99
    expect(assigns(:contacts).count).to eql(0)
  end
  it '#show' do
    get :show, id: contact.id
    expect(assigns(:contact)).to eql(contact)
  end
  it '#update' do

  end
  it '#create' do

  end

  shared_examples_for :letters_actions do |expected_class, expected_status|
    it 'GET #letters' do
      get :letters, {id: contact_id}.merge(filtering_params)

      expect(response.status).to eql(200)
      expect(assigns(:letters).sample).to be_kind_of(expected_class)
    end

    it 'GET #letters/:letter_id/details' do
      get :letter_details, { id: contact_id, letter_id: letter_id}.merge(filtering_params)

      expect(response.status).to eql(expected_status)
      expect(assigns(:letter)).to be_kind_of(expected_class)
    end
  end

  context 'with valid params' do
    let (:contact_id) { contact.id }
    let (:letter_id) { letter.id }

    include_examples :letters_actions, Mailing::Letter, 200

    context 'and additional filtering params' do
      let (:filtering_params) { { type: mailing_type.id } }

      include_examples :letters_actions, Mailing::Letter, 200
    end
  end

  context 'with invalid params' do
    let (:contact_id) { contact.id + rand(10..100) }
    let (:letter_id) { letter.id + rand(10..100) }

    include_examples :letters_actions, NilClass, 404
  end

end
