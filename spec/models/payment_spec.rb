RSpec.describe Payment, type: :model do
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'online', title: 'Онлайн') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'card', title: 'Банковская карта') }
  let(:payment_kit) { PaymentKit.create!(product: product, namespace: namespace, subproduct: subproduct, period: period) }

  describe '#create' do
    it 'will generate guid' do
      payment = described_class.create!(payment_kit: payment_kit, paymethod: paymethod, price: 100)
      expect(payment.reload.data['guid']).to_not be_nil
    end
  end
end
