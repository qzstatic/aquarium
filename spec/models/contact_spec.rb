RSpec.describe Contact, type: :model do
  let (:email) {'test@test.com'}
  describe '.find_by_unsubscribe_token' do
    context 'with valid token' do
      before(:each) do
        subject.email = email
        subject.save
      end
      
      it 'must returns valid contact' do
        token = subject.unsubscribe_token('issue')
        expect(described_class.find_by_unsubscribe_token(token)).to eq(subject)
      end
    end

    context 'with invalid token' do
      before(:each) do
        subject.email = email
        subject.save
      end
      
      it 'must returns nil' do
        token = Base64.urlsafe_encode64(Oj.dump({
          email:     email,
          type_slug: 'issue_new',
          secret:    subject.secret_token('issue')
        }))
        expect(described_class.find_by_unsubscribe_token(token)).to be_nil
      end
    end
  end
end
