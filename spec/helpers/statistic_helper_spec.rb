RSpec.describe StatisticHelper, :type => :helper do
  describe '#statistic_url' do
    it 'pixel' do
      expect(helper.statistic_url(:pixel, :mail_hit, mailing_id: 123, contact_id: 1234)).to eql('http://rigel.vedomosti.ru/pixel?event=mail_hit&tags=%5B%22mailing_id:123%22%2C%22contact_id:1234%22%5D')
    end
    it 'redirect' do
      expect(helper.statistic_url(:redirect, :mail_click, url: 'http://domain.local', mailing_id: 123, contact_id: '{{data "id"}}')).to eql('http://rigel.vedomosti.ru/redirect?event=mail_click&url=http%3A%2F%2Fdomain.local&tags=%5B%22mailing_id:123%22%2C%22contact_id:{{data "id"}}%22%5D')
    end
  end

  describe '#statistic_redirect_url' do
    it 'returns redirect url' do
      expect(helper.statistic_redirect_url('http://domain.local', :mail_click, mailing_id: 123, contact_id: '{{data "id"}}')).to eql('http://rigel.vedomosti.ru/redirect?event=mail_click&url=http%3A%2F%2Fdomain.local&tags=%5B%22mailing_id:123%22%2C%22contact_id:{{data "id"}}%22%5D')
    end
  end
end
