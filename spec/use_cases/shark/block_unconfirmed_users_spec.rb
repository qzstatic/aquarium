RSpec.describe Shark::BlockUnconfirmedUsers do
  before(:each) do
    @user1 = User.create(email: 'user1@example.com', created_at: 1.day.ago)
    @user2 = User.create(email: 'user2@example.com', status: 'confirmed')
    @user3 = User.create(email: 'user3@example.com')
    
    @session = @user1.sessions.create(
      ip:           '0.0.0.0',
      account_id:   123,
      access_token: Session.generate_token
    )
    @session.dump_to_redis
  end
  
  describe '#call' do
    it 'blocks all unconfirmed users' do
      subject.call
      
      expect(@user1.reload.status).to eq('blocked')
      expect(@user2.reload.status).to eq('confirmed')
      expect(@user3.reload.status).to eq('registered')
    end
    
    it 'logs out all blocked users sessions' do
      subject.call
      @session.reload
      
      expect(@session.user_id).to be_nil
      expect(@session.account_id).to be_nil
    end
    
    it 'creates a snapshot for each affected user' do
      expect {
        subject.call
      }.to change { Snapshot.count }.by(1)
      
      expect(Snapshot.last.subject).to eq(@user1)
      expect(Snapshot.last.event).to eq('shark.block_unconfirmed')
    end
  end
end
