RSpec.describe Shark::UpdateMailing do
  let(:user) { User.create(email: 'test@example.com') }
  let(:mail_type) { Mailing::MailType.create(slug: 'test', title: 'Test') }
  
  subject { described_class.new(user, type_ids: [ mail_type.id ]) }

  describe '#call' do
    context 'when user has linked contact' do
      before(:each) do
        @contact = user.create_contact(email: user.email)
      end

      it 'updates contact' do
        subject.call
        @contact.reload
        expect(@contact.contact_status).to eq('confirmed')
        expect(@contact.subscription_status).to eq('on')
        expect(@contact.type_ids).to eq([mail_type.id])
      end

      it 'creates snapshot' do
        expect {subject.call}.to change {Snapshot.count}.by(1)
      end
    end

    context "when user hasn't contact" do
      it 'creates contact' do
        expect {subject.call}.to change {Contact.count}.by(1)
      end

      it 'changes contact' do
        subject.call
        contact = Contact.last
        expect(contact.email).to eq(user.email)
        expect(contact.user_id).to eq(user.id)
        expect(contact.contact_status).to eq('confirmed')
        expect(contact.subscription_status).to eq('on')
        expect(contact.type_ids).to eq([mail_type.id])
      end

      it 'creates 2 snapshots' do
        expect {subject.call}.to change {Snapshot.count}.by(2)
      end      
    end

    context "when contact is exists but not linked to user" do
      before(:each) do
        @contact = Contact.create!(email: user.email)
      end

      it 'changes contact' do
        subject.call
        @contact.reload
        expect(@contact.user_id).to eq(user.id)
        expect(@contact.contact_status).to eq('confirmed')
        expect(@contact.subscription_status).to eq('on')
        expect(@contact.type_ids).to eq([mail_type.id])
      end

      it 'creates snapshot' do
        expect {subject.call}.to change {Snapshot.count}.by(1)
      end      
    end

  end
end
