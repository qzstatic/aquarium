RSpec.describe Shark::IssueSession do
  subject { described_class.new(ip: '0.0.0.0') }
  
  describe '#call' do
    it 'dumps the session into redis' do
      expect(subject.session).to receive(:dump_to_redis)
      subject.call
    end
    
    context 'with persist: true option' do
      it 'saves the session to postgres' do
        expect(subject.session).to receive(:save)
        subject.call(persist: true)
      end
    end
  end
end
