RSpec.describe Shark::PaymentsOld::Card::Callback do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com') }
  let(:transaction_id) { 'abc123' }
  let(:order) { Order.by_card.find_by_transaction_id(transaction_id) }
  let(:client_ip) { '0.0.0.0' }
  let(:price) do
    Price.create!(
      product: :online,
      period:  :for_month,
      student: false,
      value:   1,
      since:   Time.now,
      expire:  1.day.from_now
    )
  end
  subject { described_class.new(transaction_id, client_ip) }
  
  describe '#call' do
    before do
      ResqueSpec.reset!
    end
    
    before(:each) do
      Shark::PaymentsOld::Card::CreateOrder.new({transaction_id: transaction_id, price_id: price.id}, user, client_ip).call
    end
    
    it 'returns wait response when data is empty' do
      res = subject.call
      
      expect(res[:status]).to eq('ok')
      expect(res).to include(:wait)
    end
    
    it 'adds task to queue' do
      subject.call
      expect(Workers::PaymentJob).to have_queued(order.id, :final)
    end
  end
end
