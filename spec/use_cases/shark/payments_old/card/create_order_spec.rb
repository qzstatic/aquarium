RSpec.describe Shark::PaymentsOld::Card::CreateOrder do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com') }
  let(:price) do
    Price.create!(
      product: :online,
      period:  :for_month,
      student: false,
      value:   1,
      since:   Time.now(),
      expire:  Time.now()+1.day
    )
  end
  let(:params) do
    { price_id: price.id }
  end
  subject { described_class.new(params, user, '0.0.0.0') }
  
  describe '#call' do
    before do
      ResqueSpec.reset!
    end
    
    it 'creates a Order' do
      expect { subject.call }.to change { Order.count }.by(1)
    end
    
    it 'creates a AccessRight' do
      expect { subject.call }.to change { AccessRight.count }.by(1)
    end
    
    it 'returns ok message' do
      res = subject.call
      expect(res[:status]).to eq('ok')
      expect(res).to include(:wait)
    end
    
    it 'adds task to queue' do
      res = subject.call
      expect(Workers::PaymentJob).to have_queued(Order.find_by_uuid(res[:wait][:uuid]).id, :start)
    end

    it 'creates Order with price' do
      subject.call
      expect(Order.last.price.id).to eq(price.id)
    end

    context 'without auto_renewal parameter' do
      it 'creates auto renewing order' do
        subject.call
        expect(Order.last.data['auto_renewing']).to eq(true)
      end
    end

    context 'with false auto_renewal parameter' do
      let (:params) do
        { price_id: price.id, auto_renewing: false }
      end
      it 'creates not auto renewing order' do
        subject.call
        expect(Order.last.data['auto_renewing']).to eq(false)
      end
    end
  end
end
