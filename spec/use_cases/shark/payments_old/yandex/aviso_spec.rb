RSpec.describe Shark::PaymentsOld::Yandex::Aviso do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com') }
  let(:client_ip) { Settings.yandex.ips[0] }
  let(:uuid) { 'uuid' }
  let(:correct_params) do
    p = {
      'requestDatetime'         => '2011-05-04T20:38:00.000+04:00',
      'action'                  => 'checkOrder',
      'shopId'                  => Settings.yandex.online.shopid,
      'shopArticleId'           => '456',
      'invoiceId'               => '1234567',
      'customerNumber'          => uuid,
      'orderCreatedDatetime'    => '2011-05-04T20:38:00.000+04:00',
      'orderSumAmount'          => '1.00',
      'orderSumCurrencyPaycash' => '643',
      'orderSumBankPaycash'     => '1001',
      'shopSumAmount'           => '1.00',
      'shopSumCurrencyPaycash'  => '643',
      'shopSumBankPaycash'      => '1001',
      'paymentPayerCode'        => '42007148320',
      'paymentType'             => 'AC'
    }
    pa = %w(action orderSumAmount orderSumCurrencyPaycash orderSumBankPaycash shopId invoiceId customerNumber).map { |k| p[k] } << Settings.yandex.shoppassword
    p['md5'] = Digest::MD5.hexdigest(pa.join(';')).upcase
    p
  end
  let(:order) { Order.by_yandex.find_by_uuid(uuid) }
  let(:access_right) { AccessRight.find_by(order: order) }
  let(:access_dates) {
    {
      start_date: Time.zone.now,
      end_date:   1.month.from_now
    }
  }
  let(:price) do
    Price.create!(
      product: :online,
      period:  :for_month,
      student: false,
      value:   1,
      since:   Time.now(),
      expire:  Time.now()+1.day
    )
  end
  
  let(:params) { ActionController::Parameters.new(correct_params) }
  
  subject { described_class.new(params, client_ip) }
  
  describe '#call' do
    before(:each) do
      Shark::PaymentsOld::Yandex::CreateOrder.new({ uuid: uuid, price_id: price.id }.merge(access_dates), user, '0.0.0.0').call
      # check всегда с правильными параметрами
      Shark::PaymentsOld::Yandex::Check.new(ActionController::Parameters.new(correct_params), Settings.yandex.ips[0]).call
    end
    
    context 'when correct parameters' do
      it 'returns xml body with code="0"' do
        res = subject.call
        
        expect(res[:status]).to eq('ok')
        expect(res).to include(:body)
        expect(res[:body]).to match(/ code="0"/)
      end
      
      it 'change access dates' do
        expect {
          subject.call
        }.to change {
          access_right.reload.slice(:start_date, :end_date).values.map(&:to_i)
        }.from(
          [0, 0]
        ).to(access_dates.values.map(&:to_i))
      end
    end
    
    context 'when incorrect ip' do
      let (:client_ip) { '0.0.0.0' }
      
      it 'returns forbidden error' do
        res = subject.call
        
        expect(res[:status]).to eq('error')
        expect(res[:errors]).to eq([ 'Forbidden' ])
      end
    end
    
    context 'when incorrect md5' do
      let (:params) do
        p = correct_params
        p['md5'] = '1111111'
        ActionController::Parameters.new(p)
      end
      
      it 'returns xml body with code="1"' do
        res = subject.call
        
        expect(res[:status]).to eq('ok')
        expect(res).to include(:body)
        expect(res[:body]).to match(/ code="1"/)
      end
    end
  end
end
