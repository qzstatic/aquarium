RSpec.describe Common::CreateContact do
  let(:attributes) do
    { email: 'user@example.com' }
  end

  let (:type_slugs) do
    [ 'issue' ]
  end
  
  subject { described_class.new(attributes, type_slugs) }

  context 'new contact with valid type_slug' do
    before (:each) do
      Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )
      ResqueSpec.reset!
    end

    it 'returns true' do
      expect(subject.call).to be_truthy
    end

    it 'create new contact' do
      expect {
        subject.call
      }.to change { Contact.count }.by(1)
    end

    it 'create new snapshot' do
      expect {
        subject.call
      }.to change { Snapshot.count }.by(1)
    end

    it 'create new contact confirmation user token' do
      expect {
        subject.call
      }.to change { UserToken.contact_confirmation.count }.by(1)
    end

    it 'make resque task' do
      subject.call
      expect(Workers::SubscriberMailer::ConfirmationEmailJob).to have_queued(subject.token.id)
    end
  end

  context 'new contact with invalid type_slug' do
    before (:each) do
      ResqueSpec.reset!
    end

    it 'returns false' do
      expect(subject.call).to be_falsey
    end

    it 'returns correct error' do
      subject.call
      expect(subject.errors).to eq(['invalid mailing type(s)'])
    end

    it "don't create new contact" do
      expect {
        subject.call
      }.to_not change { Contact.count }
    end

    it "don't create new snapshot" do
      expect {
        subject.call
      }.to_not change { Snapshot.count }
    end

    it "don't create new contact confirmation user token" do
      expect {
        subject.call
      }.to_not change { UserToken.contact_confirmation.count }
    end

    it "don't make resque task" do
      subject.call
      expect(Workers::SubscriberMailer::ConfirmationEmailJob).to_not have_queued
    end
  end

  context 'new contact with one invalid type_slug and one valid type_slug' do
    let (:type_slugs) do
      [ 'issue', 'invalid' ]
    end

    before (:each) do
      Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )

      ResqueSpec.reset!
    end

    it 'returns false' do
      expect(subject.call).to be_falsey
    end

    it 'returns correct error' do
      subject.call
      expect(subject.errors).to eq(['invalid mailing type(s)'])
    end

    it "don't create new contact" do
      expect {
        subject.call
      }.to_not change { Contact.count }
    end

    it "don't create new snapshot" do
      expect {
        subject.call
      }.to_not change { Snapshot.count }
    end

    it "don't create new contact confirmation user token" do
      expect {
        subject.call
      }.to_not change { UserToken.contact_confirmation.count }
    end

    it "don't make resque task" do
      subject.call
      expect(Workers::SubscriberMailer::ConfirmationEmailJob).to_not have_queued
    end
  end

  context 'exists contact with valid type_slug' do
    before (:each) do
      Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )

      Contact.create!(
        { email: 'user@example.com' }
      )

      ResqueSpec.reset!
    end

    it 'returns true' do
      expect(subject.call).to be_truthy
    end

    it "don't create new contact" do
      expect {
        subject.call
      }.to_not change { Contact.count }
    end

    it "don't create new snapshot" do
      expect {
        subject.call
      }.to_not change { Snapshot.count }
    end

    it "create new contact confirmation user token" do
      expect {
        subject.call
      }.to change { UserToken.contact_confirmation.count }.by(1)
    end

    it 'make resque task' do
      subject.call
      expect(Workers::SubscriberMailer::ConfirmationEmailJob).to have_queued(subject.token.id)
    end
  end

  context 'exists contact already subscribed' do
    before (:each) do
      mail_type = Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )

      Contact.create!(
        { email: 'user@example.com', type_ids: [mail_type.id] }
      )

      ResqueSpec.reset!
    end

    it 'returns false' do
      expect(subject.call).to be_falsey
    end

    it 'returns correct error' do
      subject.call
      expect(subject.errors).to eq(['already subscribed'])
    end
  end
end