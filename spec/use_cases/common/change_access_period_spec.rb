RSpec.describe Common::ChangeAccessPeriod do
  let(:access_right) do
    Common::GrantAccess.new(
      reader:     User.create(email: 'user@example.com'),
      order:      Order.create(payment_method: :by_card),
      start_date: Time.now,
      end_date:   1.month.from_now
    ).tap(&:call).access_right
  end
  
  let(:new_period) do
    {
      start_date: 3.months.from_now,
      end_date:   5.month.from_now
    }
  end
  
  subject { described_class.new(access_right.id, new_period) }
  
  it 'changes access pediod for specified access right' do
    expect {
      subject.call
    }.to change {
      access_right.reload.slice(:start_date, :end_date)
    }.to(new_period)
  end
  
  it 'dumps data to redis' do
    allow(AccessRight).to receive(:find).with(access_right.id).and_return(access_right)
    
    expect(access_right).to receive(:dump_to_redis)
    subject.call
  end
  
  it 'creates a snapshot' do
    subject
    
    expect {
      subject.call
    }.to change { Snapshot.count }.by(1)
    
    snapshot = subject.access_right.snapshots.last
    expect(snapshot.event).to eq('access_period_changed')
  end
  
  it 'confirms users' do
    expect(subject.access_right).to receive(:confirm_users)
    subject.call
  end
end
