RSpec.describe Ibis::UpdateUser do
  let(:editor_id) { 1 }
  
  before(:each) do
    Ibis::CreateUser.call({ email: 'foo@bar.com', nickname: 'foobar' }, editor_id)
  end
  
  subject { described_class.new(User.last.id, attributes, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { nickname: 'baz' }
      end
      
      it 'saves the user' do
        expect {
          subject.call
        }.to change { User.last.nickname }.from('foobar').to('baz')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.user.snapshots.last
        expect(snapshot.event).to eq('ibis.user_updated')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { email: nil }
      end
      
      it "doesn't save the user" do
        expect(subject.call).to be_falsy
      end
    end
  end
end
