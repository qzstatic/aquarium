RSpec.describe Ibis::CreateOption do
  subject { described_class.new(attributes) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { type: 'auto', value: 'Volvo' }
      end
      
      it 'saves the option' do
        expect {
          subject.call
        }.to change { Option.count }.by(1)
      end
      
      it 'pushes the data to redis' do
        expect(Option).to receive(:save_type_to_redis).with('auto')
        subject.call
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { type: 'unknown' }
      end
      
      it "doesn't touch redis" do
        expect(Option).not_to receive(:save_type_to_redis).with('auto')
        subject.call
      end
    end
  end
end
