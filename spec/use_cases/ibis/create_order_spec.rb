RSpec.describe Ibis::CreateOrder do
  let(:editor_id) { 1 }
  let(:user) {
    User.create!(email: 'test@test.com', first_name: 'Vasya', last_name: 'Pupkin')
  }
  let(:price_params) do
    {
      product: 'online',
      period:  'for_month'
    }
  end
  subject { described_class.new(attributes, editor_id) }
  
  before(:each) do
    Price.create!({ since: Time.at(0), value: 1 }.merge(price_params))
  end
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        {
          reader:            { reader_type: 'User', reader_id: user.id },
          start_date:        Time.utc(1999,12,31,21).to_s,
          end_date:          Time.utc(2000,1,30,21).to_s,
          account:           'VIN-007',
          subscribe_base_id: 12345,
          manager_comment:   'test order, arrrgh!'
        }.merge(price_params)
      end
      
      it 'saves the order' do
        expect {
          subject.call
        }.to change { Order.count }.by(1)
        
        expect(Order.last.account).to eq('VIN-007')
        expect(Order.last.subscribe_base_id).to eq(12345)
        expect(Order.last.start_date.to_s).to eq(Time.new(2000,1,1).to_s)
        expect(Order.last.end_date.to_s).to eq(Time.new(2000,1,31).end_of_day.to_s)
      end
      
      it 'creates 2 snapshots' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(2)
        
        snapshot = subject.order.snapshots.last
        expect(snapshot.event).to eq('ibis.order_created')
        expect(snapshot.author_id).to eq(editor_id)
      end
      
      it 'creates an access right' do
        expect {
          subject.call
        }.to change { AccessRight.count }.by(1)
        
        expect(subject.order.access_rights[0].reader).to eq(user)
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { 
          start_date:      Time.now.to_s,
          end_date:        1.month.from_now.to_s,
          account:         'VIN-007',
          manager_comment: 'test order, arrrgh!'
        }
      end
      
      it "doesn't saves the order" do
        expect {
          subject.call
        }.not_to change { Order.count }
      end
    end
  end
end
