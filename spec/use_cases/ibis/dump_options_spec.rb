RSpec.describe Ibis::DumpOptions do
  before(:each) do
    Ibis::CreateOption.call(type: 'auto', value: 'Volvo')
  end
  
  describe '#call' do
    it 'dumps all option types to redis' do
      expect(Option).to receive(:save_type_to_redis).exactly(4).times
      subject.call
    end
  end
end
