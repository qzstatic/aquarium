RSpec.describe Ibis::Reports::All do
  let(:editor_id) { 1 }
  
  subject { described_class.new(1) }
  
  before(:each) do
    Ibis::Reports::Create.call({ start_date: Time.now.to_s, end_date: Time.now.to_s, kind: 'report' }, editor_id)
  end
  
  describe '#call' do
    it 'returns all reports' do
      expect(subject.call.count).to eq(1)
      expect(subject.call.first.kind).to eq('report')
    end
  end
end
