RSpec.describe Ibis::UpdateMailing do
  let(:editor_id) { 1 }
  let!(:mail_type) { Mailing::MailType.create!(slug: 'test', title: 'Test') }

  before (:each) do
    Ibis::CreateMailing.new({ type_id: mail_type.id, subject: 'Test mailing' }, editor_id).call
  end
    
  subject { described_class.new(Mailing.last.id, attributes, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { subject: 'Subject changed', preheader: 'preheader' }
      end
      
      it 'saves the mailing' do
        expect {
          subject.call
        }.to change { Mailing.last.subject }.from('Test mailing').to('Subject changed')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.mailing.snapshots.last
        expect(snapshot.event).to eq('ibis.mailing_updated')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { type_id: nil }
      end
      
      it "doesn't save the mailing" do
        expect(subject.call).to be_falsy
      end
    end
  end
end
