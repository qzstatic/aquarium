RSpec.describe Ibis::Mailing::BuildTemplate do
  let(:editor_id) { 1 }
  let!(:mail_type) { Mailing::MailType.create!(slug: 'test', title: 'Test') }
  let!(:mailing) do
    use_case = Ibis::CreateMailing.new({ type_id: mail_type.id, subject: 'Test mailing' }, editor_id)
    use_case.call
    use_case.mailing
  end

  subject { described_class.new(mailing.id, template_kit, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:template_kit) { [ { type: 'test' } ] }

      it 'changed mailing' do
        expect { subject.call }.to change { mailing.reload.data['template_kit'] }.from(nil).to([ { 'type' => 'test' } ])
      end
    end
  end
end