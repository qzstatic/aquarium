RSpec.describe Payments::CreateTrialPayment do
  let(:params) do
    {
      payment_kit_path: [product.slug, namespace.slug, subproduct.slug, period.slug].join('.'),
    }
  end
  let(:user) { User.create!(email: 'test@test.com') }
  let(:client_ip) { '127.0.0.1' }
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'online', title: 'Онлайн') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'trial', title: 'trial') }

  subject { described_class.new(params, user, client_ip) }

  before(:each) do
    pk = PaymentKit.create!(
      product:         product,
      namespace:       namespace,
      subproduct:      subproduct,
      period:          period,
      price:           0,
      paymethod_ids:   [paymethod.id],
      user_conditions: {online_subscriber_now: false}
    )
    pk.renewal_id = pk.id
    pk.save!
  end

  context 'for suitable user' do
    it 'creates new payment' do
      expect {
        subject.call
      }.to change { Payment.count }.by(1)
    end

    it 'add subscription for user' do
      expect {
        subject.call
      }.to change { user.access_rights.active.count }.from(0).to(1)
    end

    it 'add 1 access right' do
      expect {
        subject.call
      }.to change { AccessRight.count }.by(1)
    end
  end

  context 'for unsuitable user' do
    before (:each) do
      Common::GrantAccess.new(reader: user, start_date: 1.month.ago, end_date: 1.month.from_now).call
    end

    it 'returns error' do
      subject.call
      expect(subject.errors).to eq(['PaymentKit not suitable for user'])
    end
    
    it "doesn't creates new payment" do
      expect {
        subject.call
      }.to_not change { Payment.count }
    end
  end
end