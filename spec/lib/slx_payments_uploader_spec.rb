RSpec.describe SLXPaymentsUploader do
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'paper', title: 'Газета') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'card', title: 'Банковская карта') }
  let(:user) { User.create!(email: 'test@test.com', first_name: 'Vasya', last_name: 'Pupkin') }

  before(:each) do
    pk = PaymentKit.create!(
      product:       product,
      namespace:     namespace,
      subproduct:    subproduct,
      period:        period,
      price:         100,
      syncable:      true,
      paymethod_ids: [paymethod.id]
    )
    pk.renewal_id = pk.id
    pk.save!

    Payments::CreatePayment.new(
      {
        paymethod: paymethod,
        payment_kit_path: pk.slug_path
      },
      user,
      '1.1.1.1'
    ).call
  end

  subject { described_class.new }

  it 'payments_content have right content' do
    p = Payment.last
    expect(subject.payments_content).to eq("offline\t#{p.created_at.strftime('%F %T')}\t#{p.start_date.strftime('%F %T')}\t#{p.end_date.strftime('%F %T')}\t1\t100.0\tN\t#{p.id}\t#{user.id}\tN\tcard\t \ttest@test.com\tN\t0\t0\t0")
  end

  it 'users_content have right content' do
    expect(subject.users_content).to eq("\tPupkin\tVasya\t\ttest@test.com\tN\t#{user.id}\t")
  end
end
