RSpec.describe PaymentProcess::Card do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com', autopay: true) }
  let(:start_date) { 1.month.until }
  let(:end_date) { Time.now }
  let(:access_right) { AccessRight.create!(reader: user, order: order, start_date: start_date, end_date: end_date) }
  let(:data) do
    { auto_renewing: true, biller_client_id: 'test' }
  end
  let(:price) { Price.online.for_month.create!(student: false, value: 1, since: Time.now, expire: 1.month.from_now)}
  let(:order) { Order.by_card.paid.create!(start_date: start_date, end_date: end_date, data: data, price: price) }

  subject { order.payment }

  before(:each) { access_right }

  describe '#order_can_renewal?' do
    context 'with recurrent order' do
      it 'returns true' do
        expect(subject.order_can_renewal?).to be_truthy
      end
    end

    context 'with not paid order' do
      let(:order) { Order.by_card.canceled.create!(start_date: start_date, end_date: end_date, data: data, price: price) }

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end

    context 'with not recurrent order' do
      let(:data) do
        { biller_client_id: 'test' }
      end

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end

    context 'with user who canceled autopayments' do
      before { user.update!(autopay: false) }

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end

    context 'with failed renewal' do
      let(:data) do
        { auto_renewing: true, biller_client_id: 'test', renewal_failed: true }
      end

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end

    context 'with already renewed order' do
      let(:data) do
        { auto_renewing: true, biller_client_id: 'test', next_order_id: 123 }
      end

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end

    context 'order for conference translation' do
      let (:price) { Price.conf_translation.for_ever.create!(student: false, value: 1, since: Time.now, expire: 1.month.from_now) }

      it 'returns false' do
        expect(subject.order_can_renewal?).to be_falsey
      end
    end
  end

  describe '#have_next_subscription?' do
    context 'with normal condition' do
      it 'returns false' do
        expect(subject.have_next_subscription?).to be_falsey
      end
    end

    context 'with user who made another subsciption' do
      before { AccessRight.create!(reader: user, start_date: access_right.end_date, end_date: 1.month.from_now) }

      it 'returns true' do
        expect(subject.have_next_subscription?).to be_truthy
      end
    end

    context 'with paper order' do
      before do
        # paper_price = Price.paper.for_month.create!(student: false, value: 1, since: Time.now, expire: 1.month.from_now)
        paper_order = Order.by_card.paid.create!(start_date: end_date, end_date: 1.month.from_now, price: price)
        AccessRight.create!(reader: user, order: paper_order)
      end

      # let(:access_right) { AccessRight.create!(reader: user, order: order }
      let(:price) { Price.paper.for_month.create!(student: false, value: 1, since: Time.now, expire: 1.month.from_now)}
      # let(:order) { Order.by_card.paid.create!(start_date: start_date, end_date: end_date, data: data, price: price) }


      it 'returns true' do
        expect(subject.have_next_subscription?).to be_truthy
      end
    end
  end
end
