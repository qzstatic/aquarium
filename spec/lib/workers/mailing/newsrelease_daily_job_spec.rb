RSpec.describe Workers::Mailing::NewsreleaseDailyJob do
  let(:mail_type) { Mailing::MailType.create!(title: 'Title', slug: 'newsrelease') }
  let!(:user_without_subscription) { User.create!(email: 'without_subscription@test.com') }
  let(:user_with_subscription) { User.create!(email: 'with_subscription@test.com') }
  let!(:access_right) { AccessRight.create!(reader: user_with_subscription, start_date: 1.month.ago, end_date: 1.month.from_now) }

  let!(:contact_without_user) { Contact.create!(email: 'without_user@test.com', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type.id]) }
  let!(:contact_without_subscription) { Contact.create!(email: 'without_subscription@test.com', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type.id]) }
  let!(:contact_with_subscription) { Contact.create!(email: 'with_subscription@test.com', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type.id]) }

  subject { described_class }

  before (:each) do
    allow(::MailingGenerators::Newsrelease).to receive(:generate).and_return('')
  end

  it 'creates 2 mailings' do
    expect { subject.generate }.to change { Mailing.count }.by(2)
  end

  it 'send 2 mailings' do
    subject.generate
    expect(Mailing.last(2).all? { |m| m.ready? && m.contacts_ready? && m.scheduled_at<=Time.now }).to be_truthy
  end

  it 'mailings has correct contacts lists' do
    subject.generate
    contacts, contacts_free = *Mailing.last(2).map(&:contacts)

    expect(contacts).to include(contact_with_subscription)
    expect(contacts).not_to include(contact_without_user)

    expect(contacts_free).to include(contact_without_subscription)
    expect(contacts_free).to include(contact_without_user)
    expect(contacts_free).not_to include(contact_with_subscription)
  end
end
