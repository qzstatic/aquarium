RSpec.describe Mailings::ContactsSelector do
  let (:marketing_type) {Mailing::MailType.create!(slug: 'marketing', title: 'Marketing')}
  
  subject { described_class.new(marketing_type.slug) }

  # by status
  describe '#by_status' do
    context 'get unregistered' do
      let (:user1) { User.create!(email: 'pupkin@test.ru') }
      let! (:registered_contact) { Contact.create!(email: user1.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:unregistered_contact) { Contact.create!(email: 'petrov@test.ru', contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:nospam) { Contact.create!(email: 'kozlov@test.ru', contact_status: 'confirmed', subscription_status: 'on' ) }

      it 'returns correct id' do
        expect(subject.by_status([:unregistered]).sort).to eq([unregistered_contact.id])
      end
    end

    context 'get subscribed' do
      let (:subscriber) { User.create!(email: 'pupkin@test.ru') }
      let (:unsubscriber) { User.create!(email: 'petrov@test.ru') }
      let! (:contact_subscribed) { Contact.create!(email: subscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_unsubscribed) { Contact.create!(email: unsubscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      
      before(:each) do
        AccessRight.create(
          reader:     subscriber,
          start_date: 1.month.ago,
          end_date:   1.month.from_now
        )
      end

      it 'returns correct id' do
        expect(subject.by_status([:subscribed]).sort).to eq([contact_subscribed.id])
      end
    end

    context 'get unsubscribed' do
      let (:subscriber) { User.create!(email: 'pupkin@test.ru') }
      let (:unsubscriber) { User.create!(email: 'petrov@test.ru') }
      let (:exsubscriber) { User.create!(email: 'kozlov@test.ru') }
      let! (:contact_subscribed) { Contact.create!(email: subscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_unsubscribed) { Contact.create!(email: unsubscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_exsubscribed) { Contact.create!(email: exsubscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }

      before(:each) do
        AccessRight.create(
          reader:     subscriber,
          start_date: 1.month.ago,
          end_date:   1.month.from_now
        )

        AccessRight.create(
          reader:     exsubscriber,
          start_date: 1.year.ago,
          end_date:   1.month.ago
        )
      end

      it 'returns correct ids' do
        expect(subject.by_status([:unsubscribed]).sort).to eq([contact_unsubscribed.id, contact_exsubscribed.id].sort)
      end
    end

    context 'get unsubscribed and unregistered' do
      let (:subscriber) { User.create!(email: 'pupkin@test.ru') }
      let (:unsubscriber) { User.create!(email: 'petrov@test.ru') }
      let (:exsubscriber) { User.create!(email: 'kozlov@test.ru') }
      let! (:contact_subscribed) { Contact.create!(email: subscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_unsubscribed) { Contact.create!(email: unsubscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_exsubscribed) { Contact.create!(email: exsubscriber.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }
      let! (:contact_unregistered) { Contact.create!(email: 'ivanov@test.ru', contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id] ) }

      before(:each) do
        AccessRight.create(
          reader:     subscriber,
          start_date: 1.month.ago,
          end_date:   1.month.from_now
        )

        AccessRight.create(
          reader:     exsubscriber,
          start_date: 1.year.ago,
          end_date:   1.month.ago
        )
      end

      it 'returns correct ids' do
        expect(subject.by_status([:unsubscribed, :unregistered]).sort).to eq([contact_unsubscribed.id, contact_exsubscribed.id, contact_unregistered.id].sort)
      end
    end
  end

  # by_mailing
  describe '#by_mailing' do
    let (:mail_type1) { Mailing::MailType.create!(slug: 'test1', title: 'test1') }
    let (:mail_type2) { Mailing::MailType.create!(slug: 'test2', title: 'test2') }
    let! (:contact1) { Contact.create!(email: 'user1@test.ru', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type1.id, marketing_type.id]) }
    let! (:contact2) { Contact.create!(email: 'user2@test.ru', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type2.id, marketing_type.id] ) }
    let! (:contact12) { Contact.create!(email: 'user3@test.ru', contact_status: 'confirmed', subscription_status: 'on', type_ids: [mail_type1.id, mail_type2.id, marketing_type.id] ) }
    let! (:contact0) { Contact.create!(email: 'user4@test.ru', contact_status: 'confirmed', subscription_status: 'on' ) }

    context 'get from 1 mailing' do
      it 'returns 2 contacts' do
        expect(subject.by_mailing([mail_type1.id]).sort).to eq([contact1.id, contact12.id].sort)
      end
    end

    context 'get from 2 mailings' do
      it 'returns 3 contacts' do
        expect(subject.by_mailing([mail_type1.id, mail_type2.id]).sort).to eq([contact1.id, contact2.id, contact12.id].sort)
      end
    end
  end

  # by_payment_namespace
  describe '#by_payment_namespace' do
    let (:product) { Product.create!(slug: 'product1', title: 'product1') }
    let (:namespace1) { Namespace.create!(slug: 'namespace1', title: 'namespace1', product_id: product.id, start_date: 1.month.ago, end_date: 1.month.from_now) }
    let (:namespace2) { Namespace.create!(slug: 'namespace2', title: 'namespace2', product_id: product.id, start_date: 1.month.ago, end_date: 1.month.from_now) }
    let (:subproduct) { Subproduct.create!(slug: 'online', title: 'online', product_id: product.id) }
    let (:period) { Period.create!(slug: '1month', title: 'period', duration: 1.month) }
    let (:payment_kit1) { PaymentKit.create!(product_id: product.id, namespace_id: namespace1.id, subproduct_id: subproduct.id, period_id: period.id) }
    let (:payment_kit2) { PaymentKit.create!(product_id: product.id, namespace_id: namespace2.id, subproduct_id: subproduct.id, period_id: period.id) }
    let (:paymethod) { Paymethod.create!(slug: 'card', title: 'card') }
    let (:payment1) { Payment.paid.create!(payment_kit_id: payment_kit1.id, paymethod_id: paymethod.id, start_date: 2.month.ago, end_date: 1.month.ago, price: 1) }
    let (:payment2) { Payment.paid.create!(payment_kit_id: payment_kit2.id, paymethod_id: paymethod.id, start_date: 2.month.ago, end_date: 1.month.ago, price: 1) }
    let (:user1) { User.create!(email: 'pupkin@test.ru') }
    let (:user2) { User.create!(email: 'petrov@test.ru') }
    let! (:access_right1) { AccessRight.create!(reader: user1, payment_id: payment1.id, start_date: payment1.start_date, end_date: payment1.end_date) }
    let! (:access_right2) { AccessRight.create!(reader: user2, payment_id: payment2.id, start_date: payment2.start_date, end_date: payment2.end_date) }
    let! (:contact1) { Contact.create!(email: user1.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }
    let! (:contact2) { Contact.create!(email: user2.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }

    context 'get from 1 namespace' do
      it 'returns 1 contact' do
        expect(subject.by_payment_namespace([namespace1.id]).sort).to eq([contact1.id])
      end
    end

    context 'get from 2 namespaces' do
      it 'returns 2 contacts' do
        expect(subject.by_payment_namespace([namespace1.id, namespace2.id]).sort).to eq([contact1.id, contact2.id].sort)
      end
    end
  end

  # by_register_date
  describe '#by_register_date' do
    let (:user_old) { User.create!(email: 'pupkin@test.ru', created_at: 2.years.ago) }
    let! (:contact_old) { Contact.create!(email: user_old.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }
    let (:user_new) { User.create!(email: 'petrov@test.ru', created_at: 1.day.ago) }
    let! (:contact_new) { Contact.create!(email: user_new.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }

    context 'get contacts within month' do
      it 'returns 1 contact' do
        expect(subject.by_register_date(1.month.ago, Time.now).sort).to eq([contact_new.id])
      end
    end
  end

  # by_subscription_end
  describe '#by_subscription_end' do
    let (:user1) { User.create!(email: 'pupkin@test.ru') }
    let (:user2) { User.create!(email: 'petrov@test.ru') }
    let (:user3) { User.create!(email: 'kozlov@test.ru') }
    let! (:access_right1) { AccessRight.create!(reader: user1, start_date: 2.years.ago, end_date: 1.years.ago) }
    let! (:access_right2) { AccessRight.create!(reader: user2, start_date: 1.month.ago, end_date: 1.month.from_now) }
    let! (:access_right31) { AccessRight.create!(reader: user3, start_date: 2.years.ago, end_date: 1.year.ago) }
    let! (:access_right32) { AccessRight.create!(reader: user3, start_date: 1.year.ago, end_date: 1.month.from_now) }
    let! (:contact1) { Contact.create!(email: user1.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }
    let! (:contact2) { Contact.create!(email: user2.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }
    let! (:contact3) { Contact.create!(email: user3.email, contact_status: 'confirmed', subscription_status: 'on', type_ids: [marketing_type.id]) }

    context 'get contacts with old subscription' do
      it 'returns 1 contact' do
        expect(subject.by_subscription_end(13.months.ago, 11.months.ago).sort).to eq([contact1.id])
      end
    end
  end
end
