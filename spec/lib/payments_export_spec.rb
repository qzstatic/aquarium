RSpec.describe PaymentsExport do
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'paper', title: 'Газета') }
  let(:subproduct_profi) { Subproduct.create!(product_id: product.id, slug: 'profi', title: 'Газета') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'card', title: 'Банковская карта') }
  let(:user) { User.create!(email: 'test@test.com', first_name: 'Vasya', last_name: 'Pupkin').reload }

  before(:each) do
    pk = PaymentKit.create!(
      product:       product,
      namespace:     namespace,
      subproduct:    subproduct,
      period:        period,
      price:         100,
      syncable:      true,
      paymethod_ids: [paymethod.id]
    )
    pk.renewal_id = pk.id
    pk.save!

    @payment1 = Payments::CreatePayment.new(
      {
        paymethod: paymethod,
        payment_kit_path: pk.slug_path
      },
      user,
      '1.1.1.1'
    ).call
    @payment1.paid!
    @payment1.reload

    pk_profi = PaymentKit.create!(
      product:       product,
      namespace:     namespace,
      subproduct:    subproduct_profi,
      period:        period,
      price:         101,
      syncable:      true,
      paymethod_ids: [paymethod.id]
    )
    pk_profi.renewal_id = pk_profi.id
    pk_profi.save!

    @payment2 = Payments::CreatePayment.new(
      {
        paymethod: paymethod,
        payment_kit_path: pk_profi.slug_path
      },
      user,
      '1.1.1.1'
    ).call.reload
    @payment2.paid!
  end

  subject { described_class.new(1.day.ago) }

  it 'payments_content have right content' do
    expect(subject.payments_content).to eq(
      "3\t#{@payment1.data['guid']}\t#{user.data['guid']}\t\t1\t\t5\t11\t#{@payment1.start_date.strftime('%F')}\t#{@payment1.end_date.strftime('%F')}\t\t1\t100.0\t1\t-1\t2\t1\t\t0\t\t1\t\t\t\t\t\t\t\t\t\t#{@payment1.id}\ttest@test.com\t\t\t\t\t-1\t4\n" +
      "3\t#{@payment2.data['guid']}\t#{user.data['guid']}\t\t8\t\t5\t75\t#{@payment2.start_date.strftime('%F')}\t#{@payment2.end_date.strftime('%F')}\t\t1\t50.5\t1\t-1\t3\t1\t\t0\t\t6\t\t\t\t\t\t\t\t\t\t#{@payment2.id}\ttest@test.com\t\t\t\t\t-1\t2\n" +
      "3\t#{@payment2.data['guid']}\t#{user.data['guid']}\t\t1\t\t5\t11\t#{@payment2.start_date.strftime('%F')}\t#{@payment2.end_date.strftime('%F')}\t\t1\t50.5\t1\t-1\t2\t1\t\t0\t\t1\t\t\t\t\t\t\t\t\t\t#{@payment2.id}\ttest@test.com\t\t\t\t\t-1\t5"
    )
  end

  it 'users_content have right content' do
    expect(subject.users_content).to eq("3\t#{user.data['guid']}\t\tPupkin\tVasya\t\t\t\t\t\t\t\t\ttest@test.com\t0\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t")
  end
end
